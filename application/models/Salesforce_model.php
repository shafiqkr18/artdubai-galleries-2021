<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Salesforce_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_configs()
    {

        $response = $this->db->get('salesforce');

        if( $response->num_rows() > 0 ) {
            return $response->row();
        }

        return false;

    }

    public function set_configs( $data )
    {

        $response = $this->db->update('salesforce', $data);

        return $response;

    }

}
