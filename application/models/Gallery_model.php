<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model {

	public function __construct() {

	}

	public function get_gallery_profile( $userID, $id = NULL )
	{

		$this->db->where('user_id', $userID);

		if( !empty( $id ) )
		{
			$this->db->where('id', $id );
		}

		$q =  $this->db->get('gallery_profile');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_gallery_types()
    {

        $q = $this
            ->db
            ->get('gallery_types');

        if( $q->num_rows() > 0 )
        {
            return $q->result_array();
        }

        return false;

    }

	public function get_social_platforms( $userID, $id = NULL )
	{

		$this->db->where('user_id', $userID);

		if( !empty( $id ) )
		{
			$this->db->where('id', $id );
		}

		$q =  $this->db->get('social_platforms');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_locations( $userID, $id = NULL )
	{

		$this->db->where('user_id', $userID);

		if( !empty( $id ) )
		{
			$this->db->where('id', $id );
		}

		$q =  $this->db->get('locations');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_gallery_team( $userID, $id = NULL )
	{

		$this->db->where('user_id', $userID);

		if( !empty( $id ) )
		{
			$this->db->where('id', $id );
		}

		$q =  $this->db->get('gallery_team');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_artists( $userID, $id = NULL )
	{

		$this->db->where('user_id', $userID);

		if( !empty( $id ) )
		{
			$this->db->where('id', $id );
		}

		$q =  $this->db->get('artists');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_artists_exhibiting( $userID, $id = NULL )
	{

		$this->db->where('user_id', $userID);

		if( !empty( $id ) )
		{
			$this->db->where('id', $id );
		}

		$q =  $this->db->get('artists_exhibit');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

    public function get_an_artist( $id = NULL )
    {

        $this->db->select('*');
        $this->db->where('id', $id );
        $q =  $this->db->get('artists');

        if( $q->num_rows() > 0 ) {
            return $q->row();
        }

        return false;

    }

	public function get_participation( $userID, $id = NULL )
	{

		$this->db->where('user_id', $userID);

		if( !empty( $id ) )
		{
			$this->db->where('id', $id );
		}

		$q =  $this->db->get('participation');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	/* CREATE */

    public function add_locations( $id, $data )
    {

        unset( $data['section'] );

        $this->db->where('user_id', $id );
        $this->db->insert('locations', $data);

        return true;

    }

    public function add_gallery_team( $id, $data )
    {

        unset( $data['section'] );

        $this->db->where('user_id', $id );
        $q = $this->db->insert('gallery_team', $data);

        return $q;

    }

	/* UPDATE */

	public function update_gallery_statement($user_id = 0, $statement)
	{
		$data = array('statement' => $statement);    
		$this->db->where('user_id', $user_id);
		$this->db->update('gallery_profile', $data); 	
		return true;
	}

	public function update_gallery_profile( $id, $data )
	{

		unset( $data['section'] );
		unset( $data['id'] );

		$this->db->where('id', $id );
		$this->db->update('gallery_profile', $data);

		return true;

	}

	public function update_social_platforms( $id, $data )
	{

		unset( $data['section'] );
		unset( $data['id'] );

		$this->db->where('id', $id );
		$this->db->update('social_platforms', $data);

		return true;

	}

	public function update_locations( $id, $data )
	{

		unset( $data['section'] );
		unset( $data['id'] );

		$this->db->where('id', $id );
		$this->db->update('locations', $data);

		return true;

	}

	public function update_gallery_team( $id, $data )
	{

		unset( $data['section'] );
		unset( $data['id'] );

		$this->db->where('id', $id );
		$q = $this->db->update('gallery_team', $data);

		return $q;

	}

	public function update_artists( $id, $data )
	{

		unset( $data['section'] );
		unset( $data['id'] );
		unset( $data['statement'] );

		$this->db->where('id', $id );
		$this->db->update('artists', $data);

		return true;

	}

	public function update_participation( $id, $data )
	{

		unset( $data['section'] );
		unset( $data['id'] );

		$this->db->where('id', $id );
		$this->db->update('participation', $data);

		return true;

	}

	/* DELETE */

	public function delete( $id, $section )
	{

		$this->db->where( 'id', $id );
		$this->db->delete( $section );

		return true;

	}

}
