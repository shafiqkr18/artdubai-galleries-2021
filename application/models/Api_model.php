<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API_model extends CI_Model {

	public function __construct() {

	}

	public function record_count() {
			$this->db->where('catalogue', 1);
      return $this->db->count_all_results("library");
  }
  
  public function get_galleries_list($year)
  {
		$data = array();
		
		// Testing Galleries Kabir's and Genna's
        $exclude_libraries = ' AND library.user_id NOT IN (1038, 1039)'; // ''; // 
        

        if( $year < date("Y") ){
            $archive_status     = 1;
            $conditionquery     = ' AND library.exhibit_year ='.$year;
    		$orderby            = ' ORDER BY `users`.`galleryname` ASC';
        }else {
            $archive_status     = 0;
		    $conditionquery     = ' AND users.archive != 1 AND library.exhibit_year ='.$year;
		    $orderby           = ' ORDER BY `gallery_communication` ASC';
        }
        
         

        
        
        
        $query = $this->db->query( 'SELECT DISTINCT 
                                    users.id, users.galleryname, (SELECT gc.name FROM gallery_communication as gc WHERE users.id = gc.user_id  ) AS gallery_communication
                                    from users, library, gallery_communication as gc 
                                    where (library.user_id=users.id  '. $exclude_libraries .' AND library.catalogue = 1 '. $conditionquery .' AND  users.id != gc.user_id )' . $orderby); 
        
        // 'SELECT DISTINCT 
        // users.id, users.galleryname, gc.name as gallery_communication 
        // from users, library, gallery_communication as gc 
        // where library.user_id=users.id  AND gc.user_id = users.id '. $exclude_libraries .' AND library.catalogue = 1 '. $conditionquery .'   ORDER BY `gc`.`name` ASC'
                                    
        #SELECT DISTINCT users.galleryname,users.id from users,library where library.user_id=users.id AND library.catalogue=1 ORDER BY `users`.`galleryname` ASC

		if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;

  }
  
  public function get_gallery_exhibit_years( $user_id ){

    $query = $this->db->query( 'SELECT DISTINCT exhibit_year FROM `library` where user_id='.$user_id.' ORDER BY `library`.`exhibit_year` DESC'); 
                                
    if( $query->num_rows() > 0 ) {
        return $query->result();
    }

    return false;

	
 }


  public function get_documents($limit, $start, $args)
  {

      $data = array();

			if( isset( $args['md'] ) && !empty( $args['md'] ) ) {
					$this->db->select('user_id,file_id,artist,title,year,medium,dimensions,courtesy,inquiry_email,catalogue,files');
					$this->db->from('library_medium_mapping');
			} else {
					$this->db->select('user_id,id,artist,title,year,medium,dimensions,courtesy,inquiry_email,catalogue,files');
					$this->db->from('library');
			}

			if( isset( $args['dt'] ) && !empty( $args['dt'] ) ) {
				if( $args['dt'] == 'pre' )
						$this->db->where('year<2000');
				if( $args['dt'] == 'post' )
						$this->db->where('year>=2000');
			}

			if( isset( $args['at'] ) && !empty( $args['at'] ) ) {
				$this->db->like('artist', str_replace( '+', ' ', $args['at'] ), 'both');
			}

			if( isset( $args['md'] ) && !empty( $args['md'] ) ) {
				$this->db->join('library', 'library_medium_mapping.file_id = library.id', 'inner');
				$count = 1;
				$mediums = explode( ',', $args['md'] );
				foreach( $mediums as $medium ) {
						if( $count == 1 )
							$this->db->where('library_medium_mapping.medium_id='.$medium);
						else
							$this->db->or_where('library_medium_mapping.medium_id='.$medium);
						$count++;
				}
				$this->db->group_by('library.id');
			}

			// if( !isset( $args['or'] ) && empty( $args['or'] ) ) {
			// 		$this->db->order_by('title','RANDOM');
			// }

			//SELECT file_id,artist,title,year,medium,dimensions,courtesy,files
			//FROM (`library_medium_mapping`
			//INNER JOIN library
			//ON library_medium_mapping.file_id = library.id)
			//WHERE library_medium_mapping.medium_id=7
			//OR library_medium_mapping.medium_id=1
			//GROUP BY file_id;

			// This condition will display all catalogue if user selected favourite page to see all favourite listing
			$favourite_g = array();
			if( isset( $args['favg'] ) && $args['favg'] != '') {
				$favourite_g = explode(',', $args['favg']);
				//$this->db->where_in('id', $favourite_g);
			}

			if( isset( $args['fav'] ) && $args['fav'] != '') {
				$favourite = explode(',', $args['fav']);
				if(!empty($favourite_g))
				{
					$all_ids = array_merge($favourite,$favourite_g);
				}
				$this->db->where_in('id', $all_ids);
			}else{
				if(!empty($favourite_g))
				{
					$this->db->where_in('id', $favourite_g);
				}
			    
			    if( isset( $args['year'] ) && $args['year'] != '') {
					$this->db->where('exhibit_year', $args['year']);
			    }
			    
			}

            

      $query = $this
          ->db
          ->where('catalogue', 1)
		  ->where_not_in('user_id', array(1038, 1039))
		  ->order_by('title','RANDOM')
          ->limit($limit, $start)
          ->get();

		if ($query->num_rows() > 0) {

        foreach ($query->result() as $row) {
            $data[] = $row;
        }

    }

    return $data;

  }

	public function get_documents_total($args)
  {

      $data = array();

			if( isset( $args['md'] ) && !empty( $args['md'] ) ) {
					$this->db->select('user_id,file_id,artist,title,year,medium,dimensions,courtesy,inquiry_email,catalogue,files');
					$this->db->from('library_medium_mapping');
			} else {
					$this->db->select('user_id,id,artist,title,year,medium,dimensions,courtesy,inquiry_email,catalogue,files');
					$this->db->from('library');
			}

			if( isset( $args['dt'] ) && !empty( $args['dt'] ) ) {
				if( $args['dt'] == 'pre' )
						$this->db->where('year<2000');
				if( $args['dt'] == 'post' )
						$this->db->where('year>=2000');
			}

			if( isset( $args['at'] ) && !empty( $args['at'] ) ) {
				$this->db->like('artist', str_replace( '+', ' ', $args['at'] ), 'both');
			}

			if( isset( $args['md'] ) && !empty( $args['md'] ) ) {
				$this->db->join('library', 'library_medium_mapping.file_id = library.id', 'inner');
				$count = 1;
				$mediums = explode( ',', $args['md'] );
				foreach( $mediums as $medium ) {
						if( $count == 1 )
							$this->db->where('library_medium_mapping.medium_id='.$medium);
						else
							$this->db->or_where('library_medium_mapping.medium_id='.$medium);
						$count++;
				}
				$this->db->group_by('library.id');
			}

			if( isset( $args['favg'] ) && $args['favg'] != '') {
				$favourite_g = explode(',', $args['favg']);
				$this->db->where_in('id', $favourite_g);
			}

			if( isset( $args['fav'] ) && $args['fav'] != '') {
					$favourite = explode(',', $args['fav']);
					$this->db->where_in('id', $favourite);
			}
			
			if( isset( $args['year'] ) && $args['year'] != '') {
					$this->db->where('exhibit_year', $args['year']);
			}

      $query = $this
          ->db
          ->where('catalogue', 1)
					//->where_in('id', array(532, 1128, 155, 154))
          ->order_by('title','RANDOM')
          ->get();

		if ($query->num_rows() > 0) {

        foreach ($query->result() as $row) {
            $data[] = $row;
        }

    }

    return $data;

  }

	public function get_file_medium( $id )
	{

			$q = $this
					->db
					->select('name')
					->from('library_medium')
					->where('id',$id)
					->get();

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}



	public function get_file_mediums( $id )
	{

			$q = $this
					->db
					->select('medium_id')
					->from('library_medium_mapping')
					->where('file_id',$id)
					->get();

			if( $q->num_rows() > 0 ) {
					$results = $q->result_array();
					foreach ( $results as $result ) $data[] = $result['medium_id'];
					return $data;
			}

			return false;

	}

	public function get_gallery_documents($user_id, $limit = 4, $sortby)
  {

	  if($sortby == 'artist'){
            $this->db->order_by('artist', 'ASC');
        }else{
            $this->db->order_by('rand()');
        }
      

        $this->db->where('user_id', $user_id);
        $this->db->where('catalogue', 1);
        
        $this->db->limit($limit);
        $query =$this->db->get( 'library' );

		if ($query->num_rows() > 0) {

        foreach ($query->result() as $row) {
            $data[] = $row;
        }

        return $data;

    }

    return false;

  }

	public function get_gallery_documents_total($user_id)
  {

	  $query = $this
	      ->db
				->where('user_id', $user_id)
				->where('catalogue', 1)
          ->order_by('rand()')
	      ->get( 'library' );

		if ($query->num_rows() > 0) {

        foreach ($query->result() as $row) {
            $data[] = $row;
        }

        return $data;

    }

    return false;

  }

	public function get_user_documents_by_gallery_type( $user_id, $gallery_type, $year ){

		// $gallery = $this->api_model->get_gallery_name( $user_id);
		//
		// // $booth        = (!empty( $gallery[0]->booth1 ) ) ? explode('|', $gallery[0]->booth1) : 0 ;
		// // $get_gallery  = (!empty( $booth[1] ) ) ? $booth[1] : 0 ;
		// //
		// // // if($gallery_type != 0){
		// // // 	$gallery_type =  $gallery_type;
		// // // }else if( $get_gallery != 0){
		// // // 	$gallery_type =  $get_gallery;
		// // // }else{
		// // // 	$gallery_type = 0;
		// // // }

		$type = (!empty( $gallery_type ) && $gallery_type != 0 ) ? (int)$gallery_type : 0 ;

		$query = $this
	      ->db
				->select('id')
				->where('user_id', $user_id)
				->where('catalogue', 1)
				->where('exhibit_year', $year)
				->where('gallery_type', $gallery_type)
        ->order_by('rand()')
	      ->get( 'library' );

		if ($query->num_rows() > 0) {

        return $query->result();

    }

    return false;

	}

    public function get_document_by_id( $id )
    {

        $q = $this
            ->db
            ->where( 'id', $id )
            ->where( 'catalogue', 1)
            ->limit(1)
            ->get( 'library' );

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function get_gallery_name( $user_id )
    {

        $q = $this
            ->db
						//->select('*')
						//->select( 'name,email,booth1,booth2,booth1_img_link,booth2_img_link,booth1_allocation,booth2_allocation,medium' )
            ->where( 'user_id', $user_id )
            ->get( 'gallery_profile' );

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }
    
    public function get_catalogue_years(  )
    {

        $query = $this->db->query( 'SELECT DISTINCT exhibit_year FROM `library` where exhibit_year != 0 ORDER BY `library`.`exhibit_year` DESC'); 
                                    
        if( $query->num_rows() > 0 ) {
            return $query->result();
        }

        return false;

		
	}
	
	public function get_exhibition_statement($user_id)
	{
		 $query = $this
	      ->db->where('user_id', $user_id)
				->get("gallery_profile")->row();
				return $query->statement;
	}

}
