<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passes_model extends CI_Model {

	public function __construct() {

	}

	public function get_artists( $userID )
	{

		$response = $this
			->db
			->where( 'user_id', $userID )
			->get( 'artists' );

		if( $response->num_rows() > 0 ) {
			return $response->result();
		}

		return false;

	}

    public function get_a_artist( $artistID )
    {

        $response = $this
            ->db
            ->where( 'id', $artistID )
            ->get( 'artists' );

        if( $response->num_rows() > 0 ) {
            return $response->result();
        }

        return false;

    }

    public function get_staff( $userID )
    {

        $response = $this
            ->db
            ->where( 'user_id', $userID )
            ->get( 'gallery_team' );

        if( $response->num_rows() > 0 ) {
            return $response->result();
        }

        return false;

    }

    public function get_a_staff( $staffID )
    {

        $response = $this
            ->db
            ->where( 'id', $staffID )
            ->get( 'gallery_team' );

        if( $response->num_rows() > 0 ) {
            return $response->result();
        }

        return false;

    }

	public function get_passes( $userID, $type )
	{

		$response = $this
			->db
			->where( 'user_id', $userID )
			->where( 'type', $type )
			->get( 'passes' );

		if( $response->num_rows() > 0 ) {
			return $response->result();
		}

		return false;

	}

	public function get_image( $id )
	{

		$q = $this
			->db
			->where( 'id', $id )
			->limit(1)
			->get( 'passes' );

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function set_passes( $userID, $data )
	{

		$data['user_id'] = $userID;

		$response = $this->db->insert( 'passes', $data );

		return $response;

	}

	public function delete_passes( $id )
	{
		$this->db->delete( 'passes', array( 'id' => $id ) );
	}

}