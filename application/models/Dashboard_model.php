<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct() {

	}

	public function get_notifications( $userID )
	{

		$this->db->select('*,UNIX_TIMESTAMP(date_created) AS date');
		$this->db->where('receiver_id', $userID);
		$q = $this->db->get('notifications');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_unread_notifications( $userID )
	{

		$this->db->select('*,UNIX_TIMESTAMP(date_created) AS date');
		$this->db->where('receiver_id', $userID);
		$this->db->where('read', 0);
		$q = $this->db->get('notifications');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function notification_mark_read( $id, $userID )
	{

		$this->db->set('read', 1);
		$this->db->where('id', $id);
		$this->db->where('receiver_id', $userID);
		$q = $this->db->update('notifications');

		return $q;

	}

	public function get_volunteer_request( $userID )
	{

		$q = $this
			->db
			->where('user_id', $userID)
			->where('status', 1)
			->get('volunteer_request');

		if( $q->num_rows() > 0 ) {
			return true;
		}

		return false;

	}

	function get_search( $match ) {
		$this->db->like('name', $match );
		$this->db->or_like('content', $match, 'both');
		$query = $this->db->get('pages');
		return $query->result();
	}

	public function set_volunteer_request( $userID )
	{

		$this->db->set('status', 1);
		$this->db->set('user_id', $userID);
		$this->db->insert('volunteer_request');

		return true;

	}

	public function get_accommodation_request( $userID )
	{

		$q = $this
			->db
			->where('user_id', $userID)
			->where('status', 1)
			->get('accommodation_request');

		if( $q->num_rows() > 0 ) {
			return true;
		}

		return false;

	}

	public function set_accommodation_request( $userID )
	{

		$this->db->set('status', 1);
		$this->db->set('user_id', $userID);
		$this->db->insert('accommodation_request');

		return true;

	}

    public function get_gallery_fascia( $userID )
    {

        $q = $this
            ->db
            ->where('user_id', $userID)
            ->get('gallery_fascia');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function set_gallery_fascia( $userID, $data )
    {
        $approved = (!empty( $data['approved'] ) ) ? $data['approved'] : 0 ;
        
        $this->db->set('user_id', $userID);
        $this->db->set('name', $data['name']);
        $this->db->set('address1', $data['address1']);
        $this->db->set('approved', $approved);
        $this->db->insert('gallery_fascia');

        return true;

    }

    public function update_gallery_fascia( $userID, $data )
    {
         $approved = (!empty( $data['approved'] ) ) ? $data['approved'] : 0 ;
        
        $this->db->set('name', $data['name']);
        $this->db->set('address1', $data['address1']);
        $this->db->set('approved', $approved);
        $this->db->where('user_id', $userID);
        $this->db->update('gallery_fascia');

        return true;

    }

    public function get_gallery_communication( $userID )
    {

        $q = $this
            ->db
            ->where('user_id', $userID)
            ->get('gallery_communication');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function set_gallery_communication( $userID, $data )
    {
        $approved = (!empty( $data['approved'] ) ) ? $data['approved'] : 0 ;

        $this->db->set('user_id', $userID);
        $this->db->set('name', $data['name']);
        $this->db->set('address1', $data['address1']);
        $this->db->set('approved', $approved);
        $this->db->insert('gallery_communication');

        return true;

    }

    public function update_gallery_communication( $userID, $data )
    {
        $approved = (!empty( $data['approved'] ) ) ? $data['approved'] : 0 ;

        $this->db->set('name', $data['name']);
        $this->db->set('address1', $data['address1']);
        $this->db->set('approved', $approved);
        $this->db->where('user_id', $userID);
        $this->db->update('gallery_communication');

        return true;

    }

}
