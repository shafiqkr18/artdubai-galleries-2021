<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends CI_Model {

	public function __construct() {

	}

	public function get_page( $name )
	{

        $this->db->select('content');
	    $this->db->where('name', $name);
		$q = $this->db->get('pages');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

    public function get_all_documents()
    {

        $q = $this
            ->db
            ->select('*')
            ->from('documents')
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

}