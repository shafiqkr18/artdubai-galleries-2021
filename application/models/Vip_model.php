<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VIP_model extends CI_Model {

	public function __construct() {

	}

	public function create( $userID, $data )
	{

		$data['user_id'] = $userID;

		unset( $data['gallery_name'] );

		$response = $this->db->insert( 'vip', $data );

		return $response;

	}

	public function read( $userID, $type )
	{

		$response = $this
			->db
			->where( 'user_id', $userID )
			->where( 'invite_type', $type )
			->get( 'vip' );

		if( $response->num_rows() > 0 ) {
			return $response->result();
		}

		return false;

	}

	public function update_vip($userID,$dataNew)
	{
		$response = $this
			->db
			->where( 'user_id', $userID )
			->where( 'email', $dataNew['email'] )
			->get( 'vip' );

		if( $response->num_rows() > 0 ) {
			//return $response->result();
			return true;
		}else{
			$dataNew['user_id'] = $userID;

		unset( $dataNew['gallery_name'] );

		$response = $this->db->insert( 'vip', $dataNew );

		//return $response;
		return true;
		}
	}

	public function read_id( $userID, $id )
	{

		$response = $this
			->db
			->where( 'user_id', $userID )
			->where( 'id', $id )
			->get( 'vip' );

		if( $response->num_rows() > 0 ) {
			return $response->result();
		}

		return false;

	}

	public function update( $userID, $data )
	{

		$q = $this
			->db
			->where( 'user_id', $userID )
			->where( 'id', $data['id'] )
			->update( 'vip', $data );

		return $q;

	}

	public function delete( $userID, $id )
	{
		$this->db->delete( 'vip', array( 'user_id' => $userID, 'id' => $id ) );
	}

}
