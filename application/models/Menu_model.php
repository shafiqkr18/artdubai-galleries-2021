<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct() {

	}


	public function get_menu( $menuId )
	{

		$q = $this
			->db
			->where('id', (int)$menuId)
			//->limit(0)
			->get('important_action_menu');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

}