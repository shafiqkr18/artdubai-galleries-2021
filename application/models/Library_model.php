<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library_model extends CI_Model {

	public function __construct() {

	}

	public function get_documents( $userID )
	{

		$q = $this
			->db
			->where( 'user_id', $userID )
			->get( 'library' );

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}
	
	public function get_document_exhibit_years( $user_id )
    {

        $query = $this->db->query( 'SELECT DISTINCT exhibit_year FROM `library` where user_id='.$user_id.' ORDER BY `library`.`exhibit_year` DESC'); 
                                    
        if( $query->num_rows() > 0 ) {
            return $query->result();
        }

        return false;

		
    }

	public function set_document( $userID, $data )
	{
		$catalogue = ( isset( $data['catalogue'] ) && !empty( $data['catalogue'] ) ) ? $data['catalogue'] : '0';
		$is_reserved = ( isset( $data['is_reserved'] ) && !empty( $data['is_reserved'] ) ) ? $data['is_reserved'] : '0';
		$medium_map_id = $data['medium'];
		$data = array(
			'user_id'       => $userID,
			'artist'        => $data['artist'],
			'title'         => $data['title'],
			'year'          => $data['year'],
			'medium'        => $data['medium_text'],
			'dimensions'    => $data['dimensions'],
			'courtesy'      => $data['courtesy'],
			'inquiry_email' => $data['inquiry_email'],
			'exhibit_year'  => $data['exhibit_year'],
			'files'         => $data['file'],
			'catalogue'     => $catalogue,
			'is_reserved'	=> $is_reserved,
			'price'         => $data['price'],
			'statement'     => $data['statement']

		);



		$q = $this->db->insert( 'library', $data );

		$file_id = $this->db->insert_id();

			if(!empty($file_id) && !empty($medium_map_id)){

	        $this->library_model->set_file_medium( $file_id, $medium_map_id );
			}



		return $q;

	}

	public function get_document_by_id( $userID, $id )
	{

		$q = $this
			->db
			->where( 'user_id', $userID )
			->where( 'id', $id )
			->limit(1)
			->get( 'library' );

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function update_document( $userID, $data )
	{
		if($data['medium'] ){
        $this->library_model->clear_file_medium( $data['id'] );
				$this->library_model->update_file_medium( $data['id'], $data['medium'] );
		}

        $catalogue = ( isset( $data['catalogue'] ) && !empty( $data['catalogue'] ) ) ? $data['catalogue'] : '0';

		$data_to_post = array(
			'artist'        => $data['artist'],
			'title'         => $data['title'],
			'year'          => $data['year'],
			'medium'        => $data['medium_text'],
			'dimensions'    => $data['dimensions'],
			'courtesy'      => $data['courtesy'],
			'inquiry_email'=> $data['inquiry_email'],
            'catalogue'     => $catalogue
		);

		$q = $this
			->db
			->where( 'user_id', $userID )
			->where( 'id', $data['id'] )
			->update( 'library', $data_to_post );

		return $q;

	}

	public function delete_document( $userID, $id )
	{
		$this->db->delete( 'library', array( 'user_id' => $userID, 'id' => $id ) );
	}

    public function get_all_mediums()
    {

        $q = $this
            ->db
            ->select('id,name')
            ->from('library_medium')
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result_array();
        }

        return false;

    }

    public function get_file_medium($file_id)
    {

        $q = $this
            ->db
            ->select('medium_id')
            ->where('file_id',$file_id)
            ->from('library_medium_mapping')
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result_array();
        }

        return false;

    }

    public function get_a_medium($medium_id)
    {

        $q = $this
            ->db
            ->select('name')
            ->where('id',$medium_id)
            ->from('library_medium')
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function update_file_medium( $id, $medium )
    {

        $this->db->set( array( 'file_id' => $id, 'medium_id' => $medium ) );
        $this->db->insert('library_medium_mapping');

        return true;

    }

		public function set_file_medium( $id, $medium )
    {

			$data = array(
				'file_id'       => $id,
				'medium_id'        => $medium
			);

			$q = $this
				->db
				->insert( 'library_medium_mapping', $data );


        return true;

    }

    public function clear_file_medium( $id )
    {


        $this->db->where('file_id', $id);
        $this->db->delete('library_medium_mapping');

        return true;

    }

}
