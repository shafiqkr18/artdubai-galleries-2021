<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogue_model extends CI_Model {

	public function __construct() {

	}

	public function get_documents( $userID )
	{

		$q = $this
			->db
			->where( 'user_id', $userID )
			->get( 'library' );

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function set_document( $userID, $data )
	{

		$data = array(
			'user_id'       => $userID,
			'artist'        => $data['artist'],
			'title'         => $data['title'],
			'year'          => $data['year'],
			'medium'        => $data['medium'],
			'dimensions'    => $data['dimensions'],
			'courtesy'      => $data['courtesy'],
			'files'         => $data['file']
		);

		$q = $this
			->db
			->insert( 'library', $data );

		return $q;

	}

	public function get_document_by_id( $userID, $id )
	{

		$q = $this
			->db
			->where( 'user_id', $userID )
			->where( 'id', $id )
			->limit(1)
			->get( 'library' );

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function update_document( $userID, $data )
	{

		$data_to_post = array(
			'artist'        => $data['artist'],
			'title'         => $data['title'],
			'year'          => $data['year'],
			'medium'        => $data['medium'],
			'dimensions'    => $data['dimensions'],
			'courtesy'      => $data['courtesy']
		);

		$q = $this
			->db
			->where( 'user_id', $userID )
			->where( 'id', $data['id'] )
			->update( 'library', $data_to_post );

		return $q;

	}

	public function delete_document( $userID, $id )
	{
		$this->db->delete( 'library', array( 'user_id' => $userID, 'id' => $id ) );
	}

}