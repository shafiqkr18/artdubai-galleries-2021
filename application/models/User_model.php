<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct() {

	}

	public function verify_user($email, $password)
	{
		$q = $this
			->db
			->where('email',$email)
			->where('password',sha1($password))
            ->where('role!="admin"')
            ->where('archive =', 0)
			->limit(1)
			->get('users');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function verify_email($email)
	{
		$q = $this
			->db
			->where('email',$email)
            ->where('archive =', 0)
			->limit(1)
			->get('users');

		if( $q->num_rows() > 0 ) {
			return true;
		}

		return false;

	}

	public function update_key($email,$key)
	{
		$q = $this
			->db
			->set('password_reset_key',$key)
			->where('email',$email)
            ->where('archive =', 0)
			->update('users');

		return $q;

	}

	public function verify_key($email,$key)
	{
		$q = $this
			->db
			->where('email',$email)
			->where('password_reset_key',$key)
            ->where('archive =', 0)
			->limit(1)
			->get('users');

		if( $q->num_rows() > 0 ) {
			return true;
		}

		return false;

	}

	public function reset_password($email,$password)
	{

		$q = $this
				->db
				->set('password',sha1($password))
				->set('password_reset_key','')
				->where('email',$email)
                ->where('archive =', 0)
				->update('users');

		return $q;

	}

}