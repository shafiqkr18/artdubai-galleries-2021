<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']            = 'user';
$route['404_override']                  = '';
$route['translate_uri_dashes']          = FALSE;

$route['login']                         = 'user/login';
$route['logout']                        = 'user/logout';
$route['reset-password']                = 'user/reset_password';
$route['forget-password']               = 'user/forget_password';

$route['dashboard']                     = 'dashboard';
$route['submit-volunteer-request']      = 'dashboard/volunteer_request';
$route['submit-accommodation-request']  = 'dashboard/accommodation_request';
$route['submit-gallery-fascia']         = 'dashboard/gallery_fascia';
$route['submit-gallery-communication']  = 'dashboard/gallery_communication';

$route['gallery-profile']               = 'gallery';
$route['gallery-profile/(:any)/:num']   = 'gallery/edit';
$route['gallery-profile/update']        = 'gallery/update';
$route['gallery-profile/delete']        = 'gallery/delete';
$route['gallery-profile/add/(:any)']    = 'gallery/add';
$route['gallery-profile/create']        = 'gallery/create';

$route['image-library']                 = 'library';
$route['image-library/:num']            = 'library';

$route['vip-designations']              = 'vip';
$route['vip-designations/:num']         = 'vip';

$route['passes/exhibitor']              = 'passes';
$route['passes/artist']                 = 'passes';
$route['passes/create']                 = 'passes/create';
$route['passes/delete']                 = 'passes/delete';

$route['shipping']                      = 'page/shipping';
$route['booth-design']                  = 'page/booth_design';
$route['contact']                       = 'page/contact';
$route['travel']                        = 'page/travel';
$route['communications']                = 'page/communications';
$route['volunteer-request']             = 'page/volunteer';
$route['list-of-works']                 = 'page/list_of_works';
$route['my-exhibition']                 = 'page/my_exhibition';
$route['important-documents']           = 'page/important_documents';

$route['catalogue-entry']               = 'catalogue';

$route['api']                           = 'api';
$route['api/get-all-images']            = 'api/get_all_images';
$route['api/get-all-images/:num']       = 'api/get_all_images';
$route['api/get-gallery-images']        = 'api/get_gallery_images';
$route['api/get-image']                 = 'api/get_image';
$route['api/get-images-by-gallery-type']= 'api/get_images_by_gallery_type';

$route['api/api_update_vip']= 'api/api_update_vip';
