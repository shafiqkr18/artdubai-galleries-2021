<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salesforce
{

    private $ci;

    private $access_token;

    private $refresh_token;

    public function __construct()
    {

        $this->ci = & get_instance();
        $this->ci->load->model('salesforce_model');

        $configs = $this->ci->salesforce_model->get_configs();

        $this->access_token = $configs->access_token;
        $this->refresh_token = $configs->access_token;

    }

    public function check_contact_LNM( $lastName, $mobile )
    {
        $lastName = urlencode($lastName);
        $mobile = urlencode($mobile);
        $q = "query/?q=SELECT+ID,NAME,Email,Art_Dubai_Invitation_Type__c,ID__C,Updated_Details__c+FROM+CONTACT+WHERE+MOBILEPHONE+=+%27$mobile%27+AND+LastName+=+%27$lastName%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;

    }


    public function check_contact_FNM( $firstName, $mobile )
    {
        $firstName = urlencode($firstName);
        $mobile = urlencode($mobile);
        $q = "query/?q=SELECT+ID,NAME,Email,Art_Dubai_Invitation_Type__c,ID__C,Updated_Details__c+FROM+CONTACT+WHERE+MOBILEPHONE+=+%27$mobile%27+AND+FirstName+=+%27$firstName%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;

    }

    public function check_contact_LNE( $lastName, $email )
    {
        $lastName = urlencode($lastName);
        $q = "query/?q=SELECT+ID,NAME,Email,Art_Dubai_Invitation_Type__c,ID__C,Updated_Details__c+FROM+CONTACT+WHERE+Email+=+%27$email%27+AND+LastName+=+%27$lastName%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;

    }

    public function check_contact_FNE( $firstName, $email )
    {
        $firstName = urlencode($firstName);
        $q = "query/?q=SELECT+ID,NAME,Email,Art_Dubai_Invitation_Type__c,ID__C,Updated_Details__c+FROM+CONTACT+WHERE+Email+=+%27$email%27+AND+FirstName+=+%27$firstName%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;

    }

    public function check_contact_a( $email )
    {

        $q = "query/?q=SELECT+ID,NAME,Email,Art_Dubai_Invitation_Type__c,ID__C,Updated_Details__c+FROM+CONTACT+WHERE+Email+=+%27$email%27+OR+Business_Email__c+=+%27$email%27+OR+Personal_Email__c+=+%27$email%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;

    }

    public function check_contact_b( $firstName, $lastName )
    {
        $firstName = urlencode($firstName);
        $lastName = urlencode($lastName);
        $q = "query/?q=SELECT+ID,NAME,Email,Art_Dubai_Invitation_Type__c,ID__C,Updated_Details__c+FROM+CONTACT+WHERE+FirstName+=+%27$firstName%27+AND+LastName+=+%27$lastName%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;

    }

    public function check_lead_by_name($firstName, $lastName)
    {
        $firstName = urlencode($firstName);
        $lastName = urlencode($lastName);
        $q = "query/?q=SELECT+ID,NAME,Email,ID__C,Art_Dubai_Invitation_Types__c+FROM+LEAD+WHERE+FIRSTNAME+=+%27$firstName%27+AND+LASTNAME+=+%27$lastName%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;
    }

    public function check_lead( $email )
    {

        $q = "query/?q=SELECT+ID,NAME,Email,ID__C,Art_Dubai_Invitation_Types__c+FROM+LEAD+WHERE+Email+=+%27$email%27";

        $response = $this->run_curl( 'GET', $q );

        return $response;

    }

    public function update_contact_fields($ID, $gallery , $invite_type )
    {
        # code...
         $jsonData = json_encode(array("Contact_Source_Gallery2__c" => $gallery,"Art_Dubai_Invitation_Type__c" => $invite_type));

        $response = $this->run_curl( 'PATCH', 'sobjects/CONTACT/'.$ID, $jsonData );

        return $response;
    }

    public function update_contact_field($ID, $gallery )
    {
        # code...

        $jsonData = json_encode(array("Contact_Source_Gallery2__c" => $gallery));

        $response = $this->run_curl( 'PATCH', 'sobjects/CONTACT/'.$ID, $jsonData );

        return $response;
    }

    public function update_lead_fields($ID, $gallery , $invite_type )
    {
        # code...


        $jsonData = json_encode(array("Lead_Source_Gallery_Name2__c" => $gallery,"Art_Dubai_Invitation_Type__c" => $invite_type));

        $response = $this->run_curl( 'PATCH', 'sobjects/LEAD/'.$ID, $jsonData );

        return $response;
    }

    public function update_lead_field($ID, $gallery )
    {
        # code...
        $jsonData = json_encode(array("Lead_Source_Gallery_Name2__c" => $gallery));

        $response = $this->run_curl( 'PATCH', 'sobjects/LEAD/'.$ID, $jsonData );

        return $response;
    }

    public function generate_SIT($SFData, $postedData)
    {
        $data = array();

        if($postedData['invite_type'] == 'patrons')
        {
            $inviteType = 'Patrons Circle - AD';
        }elseif($postedData['invite_type'] == 'collectors')
        {
            $inviteType = 'Collectors Circle - AD';
        }else{
            $inviteType = 'VIP 1 - AD';
        }

        $data['Salutation__c']                     = $postedData['salutation'];
        $data['First_Name__c']                      = $postedData['first_name'];
        $data['Last_Name__c']                       = $postedData['sur_name'];
        $data['Job_Title__c']                          = $postedData['job_title'];
        $data['Company__c']                        = $postedData['company'];
        $data['Email__c']                          = $postedData['email'];
        //$data['Telephone__c']                    = $postedData['mobile_code'] . $postedData['mobile'];
        $data['Mobile__c']                       = $postedData['mobile_code'] . $postedData['mobile'];
        $data['Country__c']                        = $postedData['country'];
       // $data['House_Office_Number__c']         = $postedData['apt_flat'];
       // $data['Street__c']                         = $postedData['street_block'];
        $data['City__c']                           = $postedData['city'];
        $data['State__c']                          = ( isset( $postedData['state'] ) && !empty( $postedData['state'] ) ) ? $postedData['state'] : '';
       // $data['Zip_Postal_Code__c']                     = $postedData['postal_zip_code'];
        //$data['Mailing_P_O_Box__c']                     = $postedData['po_box'];
       // $data['Art_Dubai_2020_Invitation_Method__c']  = ( isset( $SFData->Art_Dubai_Invitation_Types__c ) && !empty( $SFData->Art_Dubai_Invitation_Types__c ) ) ? $SFData->Art_Dubai_Invitation_Types__c : $inviteType;
        $data['Status__c']                         = 'Open';
       // $data['Employment_Status__c']           = 'Self-Employed';
        $data['Contact__c']           = $SFData->Id;

        $jsonData = json_encode( $data );

        $response = $this->run_curl( 'POST', 'sobjects/Stay_In_Touch__c', $jsonData );

        return $response;


    }

    public function generate_lead( $SFData, $postedData )
    {

        $data = array();

        $inviteType = ( $postedData['invite_type'] == 'patrons' ) ? 'Patrons Circle - AD' : 'Collectors Circle - AD';

        $data['Contact_ID__c']                  = ( isset( $SFData->ID__C ) && !empty( $SFData->ID__C ) ) ? $SFData->ID__C : '';
        $data['Lead_Source__c']                 = 'AD Exhibitor Portal';
        $data['Lead_Source_Gallery_Name__c']    = $postedData['gallery_name'];
        $data['Salutation']                     = $postedData['salutation'];
        $data['FirstName']                      = $postedData['first_name'];
        $data['LastName']                       = $postedData['sur_name'];
        $data['Title']                          = $postedData['job_title'];
        $data['Company']                        = $postedData['company'];
        $data['Email']                          = $postedData['email'];
        $data['MobilePhone']                    = $postedData['mobile_code'] . $postedData['mobile'];
        $data['Country']                        = $postedData['country'];
        //$data['House_Office_Number__c']         = $postedData['apt_flat'];
        //$data['Street']                         = $postedData['street_block'];
        $data['City']                           = $postedData['city'];
        $data['State']                          = ( isset( $postedData['state'] ) && !empty( $postedData['state'] ) ) ? $postedData['state'] : '';
       // $data['PostalCode']                     = $postedData['postal_zip_code'];
        //$data['P_O_Box__c']                     = $postedData['po_box'];
        $data['Art_Dubai_Invitation_Types__c']  = ( isset( $SFData->Art_Dubai_Invitation_Types__c ) && !empty( $SFData->Art_Dubai_Invitation_Types__c ) ) ? $SFData->Art_Dubai_Invitation_Types__c : $inviteType;
        $data['Status']                         = 'Open';
        $data['Employment_Status__c']           = 'Self-Employed';

        $jsonData = json_encode( $data );

        $response = $this->run_curl( 'POST', 'sobjects/Lead', $jsonData );

        return $response;

    }

    public function verify_token()
    {

        $inContact = $this->check_contact_a( 'gary@wewantzoom.com' );

        $contactJson = json_decode( $inContact );

        
        if ( !isset( $contactJson->totalSize ) ) {
            
            $this->refresh_token();

        }

    }

    private function refresh_token()
    {
        

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://login.salesforce.com/services/oauth2/token?grant_type=refresh_token&refresh_token=5Aep861JmND5bFIsadPhsOBhsJ.YpiUMNVkHptGf8DHDGrewAyjGQIdMTzkTVr4jqLgLQVe6flNf4G9xr4aEcjH&client_id=3MVG9WtWSKUDG.x57rDG7_XgX12t0PMlaYmu_FtecBIcr29wEFV8PxKBuVSJh.f3QCCXAHoL8DSUwrK4H1NL5&client_secret=75DB9B15A0923FA844AE007F44009B41AAD31CC2A955ED1182FFC7B3874F2599",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic 6Cel800D20000000o1M78883X000001Altqg4Pq8NZjr1LrfXgSPeLEUsMACS80ZKiH6adbDKLVBZxsQaVzkwmhlm77WXuJltZBZnzJ8NF8",
                "Content-Type: application/x-www-form-urlencoded",
                "Postman-Token: 596a417d-472f-4b3c-bff7-edb898f73606",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        // echo " here1"; exit();
        if (curl_errno($curl)) {
            $error_msg = curl_error($curl);
            echo $error_msg;
        }
       // print_r($response);

        curl_close($curl);

       

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode( $response );
            $this->ci->salesforce_model->set_configs( array( 'access_token' => $response->access_token ) );
        }

    }

    private function run_curl( $type, $endpoint, $payload = NULL )
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://artdubai.my.salesforce.com/services/data/v44.0/" . $endpoint,
            //CURLOPT_URL => "https://artdubai--chirag.my.salesforce.com/services/data/v44.0/" . $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER =>false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $payload,
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: Bearer " . $this->access_token,
                "Content-Type: application/json",
                "Postman-Token: d75ceeca-8619-4866-9ae3-95fa932025eb",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }

    }

}
