
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Contractor Passes</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="exhibitor-passes">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="info text-center">
                            <p>Only Art Dubai’s official contractors will be permitted on site. Exhibitors planning to use an outside contractor during set-up or take-down must advise the organisers by completing the Contractor Passes form.</p>
                            <p>All third-party contractors will receive <strong>Contractor Passes</strong> which must be worn throughout the fair.<br> Access is granted from March 17 – March 18 for set-up and March 23 for take-down.</p>
                            <div class="icon"><img src="assets/images/icon16.png" alt=""></div>
                        </div>

                        <div class="info filled text-center">
                            <h3 class="section-heading">Before the Fair</h3>
                            <p>In order to receive Contractor Passes, please fill out the Compliance and Responsibility forms available below by February 13, 2019. These must be submitted signed and stamped by the third-party contractor you will be hiring. The exhibitor is responsible for communicating authorisation requirements to the contractor in accordance with the UAE Labour Law, the Venue and the Fair’s Health and Safety regulations; and is responsible for all clearance checks.</p>
                            <a href="#" class="btn smaller alt">Terms & Conditions</a>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                    <div class="white-box">
                                        <div class="icon"><img src="assets/images/icon17.png" alt=""></div>
                                        <h5>Upload Signed<br> <strong>Responsibility Claimed</strong> Form</h5>
                                        <a href="#" class="btn smaller alt"><i class="fa fa-upload"></i> Upload Form</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="white-box">
                                        <div class="icon"><img src="assets/images/icon17.png" alt=""></div>
                                        <h5>Upload Signed<br> <strong>Compliance</strong> Form</h5>
                                        <a href="#" class="btn smaller alt"><i class="fa fa-upload"></i> Upload Form</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content-box">
                            <h3 class="section-heading">Submit Contractor Details</h3>
                            <p>We require an ID image for each Contractor’s Pass. Images should be clear, facing the camera and in color. (SPACE)</p>
                            <p>Image Size: 300 dpi, H: 472px by W: 354px, file size 150 to 300 kb, format: JPEG.</p>
                            <form action="#">
                                <fieldset>
                                    <input type="text" placeholder="Full Name *">
                                </fieldset>
                                <fieldset class="col2">
                                    <input type="text" placeholder="Job Title *">
                                    <input type="text" placeholder="Company *">
                                </fieldset>
                                <fieldset>
                                    <input type="file">
                                </fieldset>
                                <fieldset>
                                    <input type="submit" value="Save" class="btn smaller filled">
                                </fieldset>
                            </form>
                        </div>

                        <div class="list-points text-center">
                            <h3 class="section-heading">During the Fair</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul>
                                        <li>Each contractor should give Art Dubai an approximate time of arrival beforehand.</li>
                                        <li>Contractors must contact Shahane, exhibitor relations manager, once they arrive at the fair.</li>
                                        <li>Art Dubai will give the contractors their Art Dubai Contractor Passes and work permits.</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                        <li>Once they receive their work permits contractors must go to the security department to submit these along with all the ID’s of those who will be working onsite.</li>
                                        <li>In return they will receive a Jumeirah work pass.</li>
                                        <li>Once the contractors have both the Jumeirah work pass and the Art Dubai Contractor Pass they may start working.</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="#" class="btn smaller alt">Download venue Map for Security office location</a>
                        </div>

                    </div>
                </div>
            </div>

        </div>

	</div>
