<div class="ad-page-wrap">

	<div class="ad-inner-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Artist Passes</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="exhibitor-passes">

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<div class="info text-center">
						<p>Please select the artists who will require an artist pass for the fair by <br><strong>January 26, 2021</strong></p>
						<div class="icon"><a class="image-popup" href="<?php echo upload_dir( 'passes/Passes_Dummy.jpg' ); ?>"><img src="<?php echo base_url( 'assets/images/icon2.png' ); ?>" alt="">
							<p><strong>Check Sample Image</strong></a></p>
						</div>
						<div class="critical-info"> <p><strong>Images should be clear, facing the camera and in colour.</strong></p></div>
						<form id="passes-form" action="<?php echo base_url( 'passes/create' ); ?>" method="post">
							<figure>
								<div class="select-custom">
									<select name="aid" id="staff-name" required>
										<option value="">Select Staff</option>
										<?php
										if( !empty( $artists ) )
										{
											foreach ( $artists as $artist )
											{
												$full_name = $artist->first_name . ' ' . $artist->last_name;
												echo '<option value="' . $artist->id . '">' . $full_name . '</option>';
											}
										}
										?>
									</select>
								</div>
								<div class="file-trigger">
									<input type="file" name="file" accept="image/jpg,image/jpeg" required>
									<button class="btn filled"><i class="fa fa-upload"></i> Upload Image</button>
								</div>
								<input type="hidden" name="section" value="<?php echo $section; ?>">
								<input id="passes-submit" type="submit" class="btn filled" value="Submit">
							</figure>
						</form>
						<p>We require an ID image for each Artist Pass. Please upload each member’s image next to<br> their information in the table below.</p>
						<p><strong>Image Size:</strong> W: 354px by H: 472px, file size up to 150kb, format: JPEG/JPG.</p>
					</div>

					<table class="table data-table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Position</th>
								<th>Image</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if( !empty( $passes ) )
							{
								foreach ( $passes as $pass )
								{
									echo '<tr>
									<td>' . $pass['person'][0]->first_name . ' ' . $pass['person'][0]->last_name . '</td>
									<td>Artist</td>
									<td><img width="50" src="' . base_url( 'uploads/passes/' . $pass['thumb'] ) . '" alt=""></td>
									<td><a href="#" data-id="' . $pass['id'] . '" data-section="' . $section . '" class="del delete-pass"><i class="fas fa-trash-alt"></i></a></td>
									</tr>';
								}
							}
							?>
						</tbody>
					</table>

					<p class="text-center">If you’d like to bring an Artist that is not shown below please get in touch with <a href="mailto:genna@artdubai.ae">genna@artdubai.ae</a></p>

				</div>
			</div>
		</div>

	</div>

</div>
