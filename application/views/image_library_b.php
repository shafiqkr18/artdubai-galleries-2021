
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Artwork Image Library</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="image-library">

            <div class="container">

                <div class="row">
                    <div class="col-md-12">

                        <figure class="image-box">
                            <?php
                            $image = json_decode( $files[0]->files );
                            if( !empty( $image ) )
                            {
	                            echo '<img src="' . upload_dir( $image->file_name ) . '" class="img-responsive" alt="">';
                            }
                            ?>

                        </figure>

                        <form id="upload-edit-form" method="post" action="<?php echo base_url( 'library/update' ); ?>" class="edit-form">
                            <fieldset class="col3">
                                <input type="text" name="artist" value="<?php echo $files[0]->artist; ?>" placeholder="Artist" required>
                                <input type="text" name="title" value="<?php echo $files[0]->title; ?>" placeholder="Artwork Title" required>
                                <input type="text" name="year" value="<?php echo $files[0]->year; ?>" placeholder="Year" required>
                            </fieldset>
                            <fieldset class="col3">
                                <input type="text" name="dimensions" value="<?php echo $files[0]->dimensions; ?>" placeholder="Dimensions" required>
                                <div class="select-custom">
                                    <select name="medium" required>
                                        <option value="">Select Tag(s) *</option>
                                        <?php
                                        foreach ( $mediums as $medium ) {
                                            if( in_array( $medium['id'], $mediums_selected ) )
                                            {
                                                $selected = 'selected';
                                            }
                                            else
                                            {
                                                $selected = '';
                                            }
                                            echo '<option value="'.$medium['id'].'" '.$selected.'>'.$medium['name'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <input type="text" name="courtesy" value="<?php echo $files[0]->courtesy; ?>" placeholder="Courtesy of" required>
                            </fieldset>
							<fieldset class="col3">
    							<input type="text" name="medium_text" value="<?php echo $files[0]->medium; ?>" placeholder="Medium" required>
							</fieldset>
							
							<fieldset class="row">
                                <div class="col-md-6">
        							<label>CATALOGUE INQUIRIES WILL BE SENT DIRECTLY TO:</label>
        							<input type="email" name="inquiry_email" value="<?php echo (!empty($files[0]->inquiry_email) ) ? $files[0]->inquiry_email : $gp[0]->email; ?>" placeholder="Inquiry Email" required>
    							</div>
							</fieldset>
							
                            <fieldset class="row">
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label for="catalogue">
                                            <input type="checkbox" id="catalogue" name="catalogue" <?php echo ( $files[0]->catalogue === '1' ) ? 'checked' : ''; ?> value="1" > Show in Catalogue
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <input type="hidden" name="id" value="<?php echo $files[0]->id; ?>">
                                    <input type="submit" class="btn filled small" value="Update">
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div>

            </div>

        </div>

	</div>
