<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="ad-page-wrap">

    <div class="ad-welcome-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner-data">
                        <div class="user-info">
                            <span><?php echo $user->galleryname; ?> <?php //echo ( !empty( $gp[0]->booth1 ) ) ? $gp[0]->booth1 : ''; ?> <?php //echo ( !empty( $gp[0]->booth2 ) ) ? ' / ' . $gp[0]->booth2 : ''; ?></span>
                        </div>
                        <div class="actions">
                            <a href="#" class="notification show-notification <?php echo ( $notifications !== false ) ? '' : 'empty'; ?>"><i class="far fa-bell"></i><span><?php echo ( $notifications !== false ) ? count( $notifications ) : '0'; ?></span></a>
                            <a href="<?php echo base_url( 'logout' ); ?>" class="logout-link">Logout</a>
                        </div>
                        <div id="notifications-box" class="notification-wrap" <?php echo ( $notifications !== false ) ? 'style="display: block;"' : ''; ?>>
                            <div class="notification-list">
                                <h4>Notifications <span>(<?php echo ( $notifications !== false ) ? count( $notifications ) : '0'; ?>)</span></h4>
                                <?php
                                if( !empty( $notifications ) )
                                {
	                                foreach ( $notifications as $notification ) {
                                    echo '<div class="item">
                                            <i class="fa fa-bell"></i>
                                            <p><a href="' . base_url( 'dashboard/notifications' ) . '">' . $notification->title . '</a></p>
                                            <p><span>' . timespan( $notification->date, time(), 1 ) . ' ago</span></p>
                                        </div>';
                                    }
                                    echo '<a href="' . base_url( 'dashboard/notifications' ) . '" class="view-all">View All</a>';
                                }
                                else
                                {
                                    echo '<div class="item no" style="padding-left: 0;background: #fff;text-align: center;"><p>No new notification</p></div>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ad-welcome-data">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <p>Welcome to the Galleries Portal, a platform where you can find all relevant<br> information pertaining to your participation at Art Dubai 2021.</p>

                    <a href="<?php echo base_url('uploads/pdf/AD2020_Galleries_Portal_Guide.pdf'); ?>" title= "Art Dubai 2020 Galleries Portal Guide" target="_blank">



                    </a>

                    <p style="margin: 0 auto;">Download the Art Dubai 2021 Galleries Portal Guide here for an in-depth guide on<br> how to navigate the Art Dubai Galleries Portal and the most important steps that need to be reviewed and submitted.</p>

                    <a  style="margin: 35px auto;" href="<?php echo base_url('uploads/pdf/AD2021_Galleries_Portal_Guide.pdf'); ?>" target="_blank" class="btn simple btn-yellow">
                        <img width="48" src="<?php echo base_url('assets/images/icon-guide.png'); ?>" />
                        Download Portal Guide
                    </a>

                    <h3 class="section-heading">Key Deadlines</h3>

                    <table class="table deadlines">

                        <?php echo $data->box2; ?>

<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'image-library' ); ?><!--">Press Images</a></td>-->
<!--                            <td>November 18, 2019</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td>Second Payment Installment</td>-->
<!--                            <td>December 15, 2019</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'vip-designations' ); ?><!--">VIP Designations</a></td>-->
<!--                            <td>December 18, 2019</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'booth-design' ); ?><!--">Booth Design</a></td>-->
<!--                            <td>January 20, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'image-library' ); ?><!--">E-Catalogue</a></td>-->
<!--                            <td>February 3, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'list-of-works' ); ?><!--">List of Works</a></td>-->
<!--                            <td>February 10, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'passes/artist' ); ?><!--">Access Passes</a></td>-->
<!--                            <td>February 12, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'volunteer-request' ); ?><!--">Volunteer Request</a></td>-->
<!--                            <td>February 13, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'shipping' ); ?><!--">Shipping Documents</a></td>-->
<!--                            <td>February 14, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td>Final Payment Installment</td>-->
<!--                            <td>February 15, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'travel' ); ?><!--">Hotel Bookings</a></td>-->
<!--                            <td>February 15, 2020</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td><a href="--><?php //echo base_url( 'gallery-profile' ); ?><!--">On-Site Contact Person</a></td>-->
<!--                            <td>February 28, 2020</td>-->
<!--                        </tr>-->

                    </table>

                    <a href="<?php echo upload_dir( 'pdf/AD2021_Key_Deadlines.pdf' ); ?>" target="_blank" class="btn simple">Download & Print Deadlines</a>

                    <hr>

                    <h3 class="section-heading">Fair Schedule</h3>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="schedule-wrap">

                                <?php echo $data->box1; ?>

<!--                                <h3><span class="date">Sunday March 17</span> Hanging Day 1 (badge access only)</h3>-->
<!--                                <table class="table">-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>Deliveries by local galleries</td>-->
<!--                                        <td>8am-10am</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Galleries Welcome Desk open on Mina A'Salam entrance</td>-->
<!--                                        <td>8am-9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Exhibitor Help Desk open</td>-->
<!--                                        <td>8am-10pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Gallery Halls open to Exhibitors for set-up</td>-->
<!--                                        <td>10am-10pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Lunch for Exhibitors (Water Terrace)</td>-->
<!--                                        <td>1pm - 2pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Dinner for Exhibitors (Water Terrace)</td>-->
<!--                                        <td>6pm - 7pm</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                                <h3><span class="date">Monday March 18</span> Hanging Day 2 (badge access only)</h3>-->
<!--                                <table class="table">-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>Galleries Welcome Desk open on Mina A'Salam entrance</td>-->
<!--                                        <td>8am-6.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Gallery Halls open to Exhibitors for set-up</td>-->
<!--                                        <td>8am-8pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Exhibitor Help Desk open</td>-->
<!--                                        <td>8am-8pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Lunch for Exhibitors (Water Terrace)</td>-->
<!--                                        <td>1pm-2pm</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                                <h3><span class="date">Tuesday March 19</span> Press, Patrons and Collectors Previews</h3>-->
<!--                                <table class="table">-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>Welcome Desks open</td>-->
<!--                                        <td>9.30am-5pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Art Dubai Press Conference</td>-->
<!--                                        <td>10.30am</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Art Dubai opens for selected guests, Exhibitors to be at their booths</td>-->
<!--                                        <td>2pm-9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Patrons Circle Preview</td>-->
<!--                                        <td>3pm-9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Collectors Circle Preview</td>-->
<!--                                        <td>5pm - 9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Jumeirah Patrons Preview</td>-->
<!--                                        <td>7pm - 9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                                <h3><span class="date"><br>Wednesday March 20</span> Ladies Preview* & VIP Opening </h3>-->
<!--                                <table class="table">-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>Welcome Desks open</td>-->
<!--                                        <td>11am - 9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Art Dubai Ladies Preview*</td>-->
<!--                                        <td>1pm - 4pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>VIP Opening</td>-->
<!--                                        <td>4pm - 9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                                <h3><span class="date">Thursday March 21</span> Public Day 1</h3>-->
<!--                                <table class="table">-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>Welcome Desks open</td>-->
<!--                                        <td>10am - 10pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Public Day 1</td>-->
<!--                                        <td>2pm - 9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                                <h3><span class="date">Thursday March 22</span> Public Day 2</h3>-->
<!--                                <table class="table">-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>Welcome Desks open</td>-->
<!--                                        <td>11am - 9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Public Day 2</td>-->
<!--                                        <td>12pm - 9.30pm</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                                <h3><span class="date">Thursday March 23</span> Public Day 3</h3>-->
<!--                                <table class="table">-->
<!--                                    <tbody>-->
<!--                                    <tr>-->
<!--                                        <td>Welcome Desks open</td>-->
<!--                                        <td>11am - 6.30pm</td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td>Public Day 3</td>-->
<!--                                        <td>12pm - 6.30pm</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
                            <p style=" margin-bottom: 10px; text-align: left; "><small>*Art Dubai Ladies Preview is hosted by Her Highness Shikha Manal bint Mohammed bin Rashid Al Maktoum and is a laides-only event, which includes tours of the fair with trained Consultants and a lunch. If an Exhibitor has female representatives, it is advised that they managed the booth. If only male representatives are available, they are also welcome. </small></p>
                            <p style=" margin: 0; "><strong style=" text-align: left; ">Please note that this timetable is subject to change at the discretion of the Organisers.</strong></p>
                            </div>
                        </div>
                    </div>

                    <a href="<?php echo upload_dir( 'pdf/AD2021_Galleries_Fair_Schedule.pdf' ); ?>" target="_blank" class="btn simple">Download Full Schedule Here</a>

                </div>
            </div>
        </div>
    </div>

</div>
