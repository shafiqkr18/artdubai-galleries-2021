
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Gallery Information Edit</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="vip-designations">

            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-box">

                            <?php if( $section == 'info' ) : ?>

                            <form action="<?php echo base_url( 'gallery-profile/update' ); ?>" id="gp-update-form" class="gp-update-form" method="post">
                                <figure class="col2">
                                    <input type="text" placeholder="Name" name="name" value="<?php echo $gp[0]->name; ?>" required>
                                    <input type="text" placeholder="Established" name="established" value="<?php echo $gp[0]->established; ?>" required>
                                </figure>
                                <figure class="col2">
                                    <input type="email" placeholder="Email" name="email" value="<?php echo $gp[0]->email; ?>" required>
                                    <input type="text" placeholder="Website" name="website" value="<?php echo $gp[0]->website; ?>" required>
                                </figure>
                                <figure>
                                    <textarea name="bio" rows="10" placeholder="Biography" required><?php echo $gp[0]->bio; ?></textarea>
                                </figure>
                                <figure class="btn-wrap">
                                    <input type="hidden" name="id" value="<?php echo $gp[0]->id; ?>">
                                    <input type="hidden" name="section" value="<?php echo $section; ?>">
                                    <input type="submit" class="btn filled small" value="Update">
                                </figure>
                            </form>

	                        <?php elseif( $section == 'location' ) : ?>

                                <form action="<?php echo base_url( 'gallery-profile/update' ); ?>" id="gp-update-form" class="gp-update-form" method="post">
                                    <figure class="col2">
                                        <input type="text" placeholder="Address 1" name="address1" value="<?php echo $lc[0]->address1; ?>" required>
                                        <input type="text" placeholder="Address 2" name="address2" value="<?php echo $lc[0]->address2; ?>">
                                    </figure>
                                    <figure class="col2">
                                        <input type="text" placeholder="City" name="city" value="<?php echo $lc[0]->city; ?>" required>
                                        <div class="select-custom">
                                            <select name="country" id="country" required>
                                                <option value="">Country *</option>
                                                <?php
                                                $countries = config_item('country_list');
                                                foreach ( $countries as $country )
                                                {
                                                    $selected = ( ( trim( $lc[0]->country ) === strtolower( $country ) ) || ( trim( $lc[0]->country ) === $country ) ) ? 'selected=selected' : '';
                                                    echo '<option value="' . strtolower( $country ) . '" ' . $selected . '>' . $country . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </figure>
                                    <figure class="col3">
                                        <input type="text" placeholder="PO Box" name="pobox" value="<?php echo $lc[0]->pobox; ?>">
                                        <input type="text" placeholder="Zip Code" name="zip" value="<?php echo $lc[0]->zip; ?>">
                                        <div class="select-custom">
                                            <select name="type" required>
                                                <option value="">Address type</option>
                                                <option value="1" <?php echo ( $lc[0]->type == 1 ) ? 'selected' : ''; ?>>Primary</option>
                                                <option value="2" <?php echo ( $lc[0]->type == 2 ) ? 'selected' : ''; ?>>Billing</option>
                                                <option value="3" <?php echo ( $lc[0]->type == 3 ) ? 'selected' : ''; ?>>Gallery</option>
                                            </select>
                                        </div>
                                    </figure>
                                    <figure class="btn-wrap">
                                        <input type="hidden" name="id" value="<?php echo $lc[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn filled small" value="Update">
                                    </figure>
                                </form>

                            <?php elseif( $section == 'staff' ) : ?>

                                <form action="<?php echo base_url( 'gallery-profile/update' ); ?>" id="gp-update-form" class="gp-update-form" method="post">
                                    <figure class="col3">
                                        <input type="text" placeholder="Salutation" name="salutation" value="<?php echo $gt[0]->salutation; ?>" required>
                                        <input type="text" placeholder="First Name" name="first_name" value="<?php echo $gt[0]->first_name; ?>" required>
                                        <input type="text" placeholder="Last Name" name="last_name" value="<?php echo $gt[0]->last_name; ?>" required>
                                    </figure>
                                    <figure class="col2">
                                        <input type="text" id="phone" class="only-numbers" placeholder="Phone" name="phone" value="<?php echo $gt[0]->phone_country; ?><?php echo $gt[0]->phone; ?>" required>
                                        <input type="text" id="mobile" class="only-numbers" placeholder="Mobile" name="mobile" value="<?php echo $gt[0]->mobile_country; ?><?php echo $gt[0]->mobile; ?>" required>
                                    </figure>
                                    <figure class="col4">
                                        <input type="email" placeholder="Email" name="email" value="<?php echo $gt[0]->email; ?>">
                                        <input type="text" placeholder="City" name="city" value="<?php echo $gt[0]->city; ?>">
                                        <input type="text" placeholder="Position" name="position" value="<?php echo $gt[0]->position; ?>">
                                        <div class="select-custom">
                                            <select name="type" required>
                                                <option value="">Contact type</option>
                                                <option value="1" <?php echo ( $gt[0]->type == 1 ) ? 'selected' : ''; ?>>Main contact</option>
                                                <option value="2" <?php echo ( $gt[0]->type == 2 ) ? 'selected' : ''; ?>>CC in emails only</option>
                                                <option value="3" <?php echo ( $gt[0]->type == 3 ) ? 'selected' : ''; ?>>Others</option>
                                            </select>
                                        </div>
                                    </figure>
                                    <figure class="btn-wrap">
                                        <input type="hidden" name="id" value="<?php echo $gt[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="hidden" id="phone_code" name="phone_country" value="<?php echo $gt[0]->phone_country; ?>">
                                        <input type="hidden" id="mobile_code" name="mobile_country" value="<?php echo $gt[0]->mobile_country; ?>">
                                        <input type="submit" class="btn filled small" value="Update">
                                    </figure>
                                </form>

                            <?php elseif( $section == 'artist' ) : ?>

                                <form action="<?php echo base_url( 'gallery-profile/update' ); ?>" id="gp-update-form" class="gp-update-form" method="post">
                                    <figure class="col2">
                                        <input type="text" placeholder="First Name" name="first_name" value="<?php echo $ar[0]->first_name; ?>" required>
                                        <input type="text" placeholder="Last Name" name="last_name" value="<?php echo $ar[0]->last_name; ?>" required>
                                    </figure>
                                    <figure class="btn-wrap">
                                        <input type="hidden" name="id" value="<?php echo $ar[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn filled small" value="Update">
                                    </figure>
                                </form>

                            <?php elseif( $section == 'exhibit' ) : ?>

                                <form action="<?php echo base_url( 'gallery-profile/update' ); ?>" id="me-update-artist" class="gp-update-form" method="post">
                                    <p>We will use the below artist information in all fair collateral. Please ensure the artist's name is spelt correctly and tick APPROVE NAME when done. We will also require the artist's nationality and date of birth. Please add this for artist(s) you are exhibiting and click submit.</p>
                                    <figure class="col2">
                                        <input type="text" placeholder="First Name" name="first_name" value="<?php echo $ar[0]->first_name; ?>" required>
                                        <input type="text" placeholder="Last Name" name="last_name" value="<?php echo $ar[0]->last_name; ?>" required>
                                    </figure>
                                    <figure class="col2">
                                        <div class="select-custom">
                                            <select name="nationality" required>
                                                <option value="">Select Country *</option>
                                                <?php
                                                $countries = config_item('country_list');
                                                foreach ( $countries as $key => $value )
                                                {
                                                    $selected = ( $key === $ar[0]->nationality ) ? 'selected=selected' : '';
                                                    echo '<option value="' . $key . '" '.$selected.'>' . $value . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <input type="text" name="dob" placeholder="Date of Birth" value="<?php echo (!empty( $ar[0]->dob )) ? $ar[0]->dob : ''; ?>" required>
                                    </figure>
                                    <figure class="col2">
                                        <div class="select-custom">
                                            <select name="gender" required>
                                                <option value="">Select Gender *</option>
                                                <?php
                                                    $m_selected = (!empty( $ar[0]->gender ) && $ar[0]->gender == 'm' ) ? 'selected' : '';
                                                    $f_selected = (!empty( $ar[0]->gender ) && $ar[0]->gender == 'f' ) ? 'selected' : '';
                                                    echo '<option value="m" '. $m_selected  .'>Male</option>';
                                                    echo '<option value="f" '. $f_selected  .'>Female</option>';

                                                ?>
                                            </select>
                                        </div>
                                    </figure>
                                    <fieldset class="row">
										<div class="col-md-12">
                                         <label>Catalogue Exhibition Statement</label>
                                        <textarea rows="4" class="form-control" placeholder="Catalogue Exhibition Statement" rows="5" id="statement" name="statement"><?php echo (!empty($gp[0]->statement) ) ? $gp[0]->statement : ''; ?></textarea>
										   
        								</div>
    								</fieldset>
                                    <figure>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="approveName" name="approved" value="1" <?php echo (!empty( $ar[0]->approved )) ? 'checked=""' : ''; ?>>
                                            <label class="form-check-label" for="approveName">I approve this name to use in all fair collateral.</label>
                                        </div>
                                    </figure>
                                    <figure class="btn-wrap">
                                        <input type="hidden" name="id" value="<?php echo $ar[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn filled small" value="Submit">
                                    </figure>
                                </form>

                            <?php elseif( $section == 'participation' ) : ?>

                                <form action="<?php echo base_url( 'gallery-profile/update' ); ?>" id="gp-update-form" class="gp-update-form" method="post">
                                    <figure class="col3">
                                        <input type="text" placeholder="Fair Name" name="name" value="<?php echo $pr[0]->name; ?>" required>
                                        <input type="text" placeholder="Year" name="year" value="<?php echo $pr[0]->year; ?>" required>
                                        <input type="text" placeholder="Section" name="section" value="<?php echo $pr[0]->section; ?>">
                                    </figure>
                                    <figure class="btn-wrap">
                                        <input type="hidden" name="id" value="<?php echo $pr[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn filled small" value="Update">
                                    </figure>
                                </form>

                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>

	</div>
