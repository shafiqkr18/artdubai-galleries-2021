
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Vip Designations</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="vip-designations">

            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-box">
                            <form action="<?php echo base_url( 'vip/update' ); ?>" id="vip-entry-form" class="vip-submission-form" method="post">
                                <figure class="col3">
                                    <input type="text" placeholder="Salutation" name="salutation" value="<?php echo $entry[0]->salutation; ?>" required>
                                    <input type="text" placeholder="First name" name="first_name" value="<?php echo $entry[0]->first_name; ?>" required>
                                    <input type="text" placeholder="Surname" name="sur_name" value="<?php echo $entry[0]->sur_name; ?>" required>
                                </figure>
                                <figure class="col2">
                                    <input type="text" placeholder="Company *" name="company" value="<?php echo $entry[0]->company; ?>" required>
                                    <input type="text" placeholder="Job title" name="job_title" value="<?php echo $entry[0]->job_title; ?>">
                                </figure>
                                <figure class="col2">
                                    <input type="text" id="mobile" placeholder="Mobile" name="mobile" value="<?php echo $entry[0]->mobile_code; ?><?php echo $entry[0]->mobile; ?>" required>
                                    <input type="email" placeholder="Email" name="email" value="<?php echo $entry[0]->email; ?>" required>
                                </figure>
                                <figure>
                                    <input type="text" placeholder="Apt/Flat/Building name" name="apt_flat" value="<?php echo $entry[0]->apt_flat; ?>">
                                </figure>
                                <figure>
                                    <input type="text" placeholder="Street/Block *" name="street_block" value="<?php echo $entry[0]->street_block; ?>" required>
                                </figure>
                                <figure class="col2">
                                    <input type="text" placeholder="City" name="city" value="<?php echo $entry[0]->city; ?>" required>
                                    <input type="text" placeholder="State / Province" name="state" value="<?php echo $entry[0]->state; ?>">
                                </figure>
                                <figure class="col2">
                                    <input type="text" placeholder="Postal Code / Zip Code" name="postal_zip_code" value="<?php echo $entry[0]->postal_zip_code; ?>">
                                    <input type="text" placeholder="PO Box" name="po_box" value="<?php echo $entry[0]->po_box; ?>">
                                </figure>
                                <figure class="select-custom">
                                    <select name="country" id="country" required>
                                        <option value="">Country</option>
                                        <?php
                                        $countries = config_item('country_list');
                                        foreach ( $countries as $country )
                                        {
                                            $selected = '';
                                            if( $entry[0]->country == lcfirst( $country ) )
                                            {
                                                echo '<option value=" ' . lcfirst( $country ) . ' " selected>' . $country . '</option>';
                                            }
                                            else
                                            {
                                                echo '<option value=" ' . lcfirst( $country ) . ' ">' . $country . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </figure>
                                <figure class="select-custom">
                                    <select name="invite_type" id="invitation-type" required>
                                        <option value="">Invitation Type</option>
                                        <option value="patrons" <?php echo ( $entry[0]->invite_type == 'patrons' ) ? 'selected' : ''; ?>>Patrons Circle</option>
                                        <option value="collectors" <?php echo ( $entry[0]->invite_type == 'collectors' ) ? 'selected' : ''; ?>>Collectors Circle</option>
																				<option value="guest_of_gallery_invites" <?php echo ( $entry[0]->invite_type == 'guest_of_gallery_invites' ) ? 'selected' : ''; ?>>VIP</option>
                                    </select>
                                </figure>
                                <figure class="btn-wrap">
                                    <input type="hidden" id="mobile_code" name="mobile_code" value="">
                                    <input type="hidden" name="id" value="<?php echo $entry[0]->id; ?>">
                                    <input type="submit" class="btn filled small" value="Update">
                                </figure>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

	</div>
