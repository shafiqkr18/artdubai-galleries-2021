<?php
if(!empty( $_SESSION['userData'])):
    $popup = show_popup_footer();
    if( !empty($popup['display']) && $popup['display'] == 'yes' ):
        ?>

        <div class="modal fade" id="confirmation-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="confirmation-popup-form" action="<?php echo base_url( 'gallery-profile/update' ); ?>" method="POST">
                        <div class="modal-header text-center">
                            <h3>Gallery Name Confirmation</h3>
                            <div class="preview">
                                <?php $lc_selected = json_decode( $popup['info']['gp'][0]->cities ); ?>
                                <h2><?php echo $popup['info']['gp'][0]->name; ?><small>, <?php echo array_to_string( $lc_selected, ' / ' ); ?></small></h2>
                            </div>
                        </div>
                        <div class="modal-body text-left">
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <label for="gname">Gallery Name</label>
                                        <input type="text" class="form-control" name="name" id="gname" value="<?php echo $popup['info']['gp'][0]->name; ?>" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset>
                                        <label for="glocation">Location</label>
                                        <?php /* <select name="cities[]" id="glocation" class="form-control" multiple="multiple" required> */ ?>
                                            <?php
                                            $lcs = $popup['info']['gl'];
                                            $lc_selected = json_decode( $popup['info']['gp'][0]->cities );
                                            $locations = array();
                                            foreach ( $lcs as $lc ) {
                                                /*if($lc_selected):
                                                    if( in_array( $lc->city, $lc_selected ) ) {
                                                        echo '<option value="'.$lc->city.'" selected>'.$lc->city.'</option>';
                                                    } else {
                                                        echo '<option value="'.$lc->city.'">'.$lc->city.'</option>';
                                                    }
                                                else:
                                                    echo '<option value="'.$lc->city.'">'.$lc->city.'</option>';
                                                endif;*/

                                                $locations[] = $lc->city;
                                            }
                                            ?>

                                        <?php /* </select> */ ?>
                                        <?php 
                                        //$lcs = $popup['info']['gl'];
                                        //$lc_selected = json_decode( $popup['info']['gp'][0]->cities );
                                        $cities_bar = (!empty( $lc_selected ) ) ? implode(' / ', $lc_selected) : implode(' / ', $locations);  ?>

                                        <input type="text" class="form-control" name="cities" id="glocation" value="<?php echo $cities_bar; ?>" required>
                                        
                                    </fieldset>
                                </div>
                            </div>
                            <div style="margin: 20px 0 0;"></div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="nameConfirmation" name="confirmation" value="1" required>
                                <label class="custom-control-label" for="nameConfirmation">I confirm that the gallery name is written and formatted exactly how I would like it to appear in all Art Dubai advertising, marketing communications and social media content.</label>
                            </div>
                            <div style="margin: 20px 0 0;"></div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="cityConfirmation" name="confirmation" value="1" required>
                                <label class="custom-control-label" for="cityConfirmation">I confirm that the cities where the gallery(s) are based are written and formatted exactly how I would like them appear in all Art Dubai advertising marketing, communications and social media content (including the order/sequence that the cities are in).</label>
                            </div>
                        </div>
                        <div class="modal-footer text-center">
                            <figure class="btn-wrap">
                                <input type="hidden" name="id" value="<?php echo $popup['info']['gp'][0]->id; ?>">
                                <input type="hidden" name="userID" value="<?php echo $popup['info']['gp'][0]->user_id; ?>">
                                <input type="hidden" name="section" value="popup">
                                <input type="submit" class="btn filled small" value="Confirm">
                                <p><small>You can amend your gallery name or cities at any time by visiting the gallery profile page. Once updated you will need to re-confirm the gallery name and cities confirmation for this to be processed and updated on our system.</small></p>
                            </figure>
                        </div>
                    </form>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>

    <?php endif;

endif; ?>