<!doctype html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Art Dubai</title>

    <script>
        var baseURL = '<?php echo base_url() ?>';
    </script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/live-web/css/master.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dropzone.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">

</head>
<body>

    <header id="header" class="sm-rgt-mn">
        <div class="container  fadeIn">
            <div class="row">
                <div class="col-xs-12 col-sm-8 logo-wrap">
                    <div class="row">
                        <div class="logo col-xs-7 col-xs-offset-1 col-sm-5 col-md-4 col-sm-offset-0">
                            <a href="<?php echo base_url(); ?>"></a>
                            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url( 'assets/images/logo-2-1.png' ); ?>" width="250" id="img-logo-w1" alt="logo" class="img-logo-w1"></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 invis-xs">
                    <div class="login">
	                    <?php
	                    if( isset( $_SESSION['userData'] ) )
	                    {
		                    echo '<a class="log-in-out" href="' . base_url( 'logout' ) . '">Log out</a>';
	                    }
	                    ?>
                        <a href="<?php echo base_url( 'dashboard/search' ); ?>" class="search"><i class="fa fa-search"></i></a>
                    </div>
                    <section class="top-bar">
                        <div class="container">
                            <div class="top-links lftflot"></div>
                            <div class="socialfollow rgtflot">
                                <a href="http://facebook.com/artdubai.artfair" class="facebook" target="blank"><i class="fab fa-facebook"></i></a>
                                <a href="https://twitter.com/artdubai" class="twitter" target="blank"><i class="fab fa-twitter"></i></a>
                                <a href="https://www.youtube.com/user/artdubai" class="youtube" target="blank"><i class="fab fa-youtube"></i></a>
                                <a href="http://instagram.com/artdubai" class="instagram" target="blank"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </header>
<div style="display: none; ">
    <?php 



    function check_menu_important($menuId){
        $CI=& get_instance();
        $CI->load->model('menu_model');
        $menu   = $CI->menu_model->get_menu($menuId);

        $result = (!empty( $menu ) && $menu->status == 1) ? 'has-icon-important' : '';

        return $result;  
    }
?>
</div>

    <div id="menu" class="header_menu">
        <div class="container">
            <div class="row">
                <button type="button" class="navbar-toggle offcanvas-toggle js-offcanvas-has-events" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
                    <span class="sr-only">Toggle navigation</span>
                    <span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <div id="nav-wrap" class="nav-wrap1 navbar navbar-default">
                    <div class="navbar-offcanvas navbar-offcanvas-touch navbar-offcanvas-fade navbar-offcanvas-touch offcanvas-transform js-offcanvas-done" id="js-bootstrap-offcanvas">
                        <section class="top-bar visible-xs top-bar-mobile">
                            <div class="container">
                                <div class="top-links lftflot"></div>
                                <div class="socialfollow rgtflot">
                                    <a href="http://facebook.com/artdubai.artfair" class="facebook"><i class="fab fa-facebook"></i></a>
                                    <a href="https://twitter.com/artdubai" class="twitter"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.youtube.com/user/artdubai" class="youtube"><i class="fab fa-youtube"></i></a>
                                    <a href="http://instagram.com/artdubai" class="instagram"><i class="fab fa-instagram"></i></a>
                                    <a href="<?php echo base_url( 'dashboard/search' ); ?>" class="search"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </section>
                        <nav id="nav" class="nav overline">
                            <ul>
                                <li class="nav__item <?php echo ( $this->uri->segment(1) == 'dashboard' ) ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li class="nav__item <?php echo ( $this->uri->segment(1) == 'gallery-profile' ) ? 'active' : ''; ?>">
                                    <a class="<?php  echo check_menu_important(1); ?>" href="<?php echo base_url( 'gallery-profile' ); ?>">Gallery Profile</a>
                                </li>
                                <li class="nav__item has_sub_menu">
                                    <a class="<?php  echo check_menu_important(2); ?>" href="<?php echo base_url( 'my-exhibition' ); ?>">My Exhibition</a>
                                    <?php /*
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo base_url( 'volunteer-request' ); ?>">Volunteer Request</a></li>
                                    </ul> */?>
                                </li>
                                <li class="nav__item <?php echo ( $this->uri->segment(1) == 'important-documents' ) ? 'active' : ''; ?>">
                                    <a class="<?php  echo check_menu_important(3); ?>" href="<?php echo base_url( 'important-documents' ); ?>">Important Documents</a>
                                </li>
                                <li class="nav__item <?php echo ( $this->uri->segment(1) == 'communications' ) ? 'active' : ''; ?>">
                                    <a class="<?php  echo check_menu_important(4); ?>" href="<?php echo base_url( 'communications' ); ?>">Communications</a>
                                </li>
                                <li class="nav__item <?php echo ( $this->uri->segment(1) == 'shipping' ) ? 'active' : ''; ?>">
                                    <a href="<?php echo base_url( 'shipping' ); ?>">Shipping</a>
                                </li>
                                <li class="nav__item <?php echo ( $this->uri->segment(1) == 'booth-design' ) ? 'active' : ''; ?>">
                                    <a class="<?php  echo check_menu_important(5); ?>" href="<?php echo base_url( 'booth-design' ); ?>">Booth Design</a>
                                </li>
                                <li class="nav__item has_sub_menu">
                                    <a class="<?php  echo check_menu_important(6); ?>" href="#">Access Passes</a>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo base_url( 'passes/exhibitor' ); ?>">Exhibitor Passes</a></li>
                                        <li><a href="<?php echo base_url( 'passes/artist' ); ?>">Artist Passes</a></li>
                                    </ul>
                                </li>
                                <li class="nav__item has_sub_menu">
                                    <a class="<?php  echo check_menu_important(7); ?>" href="<?php echo base_url( 'vip-designations' ); ?>">VIP</a>
                                    <?php /*
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo base_url( 'vip-designations' ); ?>">VIP Designations</a></li>
                                    </ul> */?>
                                </li>
                                <li class="nav__item <?php echo ( $this->uri->segment(1) == 'image-library' ) ? 'active' : ''; ?>">
                                    <a class="<?php  echo check_menu_important(8); ?>" href="<?php echo base_url( 'image-library' ); ?>">Image Library</a>
                                </li>
                                <li class="nav__item has_sub_menu">
                                    <a href="#">Fair Info</a>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo base_url( 'travel' ); ?>">Travel</a></li>
                                        <li><a href="<?php echo base_url( 'contact' ); ?>">Contact</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>