<div class="footer_menu">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav id="nav-wrap">
                    <div class="container">
                        <ul id="nav2">
                            <li id="menu-item-35441" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35441">
                                <a href="https://www.artdubai.ae/about-adg/">ART DUBAI GROUP</a>
                            </li>
                            <li id="menu-item-35442" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35442">
                                <a href="https://www.artdubai.ae/careers/">CAREERS</a>
                            </li>
                            <li id="menu-item-35443" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35443">
                                <a href="https://www.artdubai.ae/contact-us/">CONTACT</a>
                            </li>
                            <li id="menu-item-35444" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35444">
                                <a href="https://www.artdubai.ae/art-salon/">ART SALON</a>
                            </li>
                            <li id="menu-item-35445" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35445">
                                <a href="https://www.artdubai.ae/press/">PRESS</a>
                            </li>
                            <li id="menu-item-35446" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35446">
                                <a href="https://www.artdubai.ae/legal/">LEGAL</a>
                            </li>
                        </ul>
                        <?php /*
                        <ul id="nav2">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24097">
                                <a href="http://www.artdubai.ae/about/">ABOUT</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6764">
                                <a href="http://www.artdubai.ae/careers/">Careers</a></li>
                            <li id="menu-item-6766" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6766">
                                <a href="http://www.artdubai.ae/legal/">LEGAL</a></li>
                            <li id="menu-item-6765" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6765">
                                <a href="http://www.artdubai.ae/contact-us/">Contact</a></li>
                            <li id="menu-item-15115" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15115">
                                <a href="http://www.artdubai.ae/press/">PRESS</a></li>
                        </ul> */ ?>
                    </div>
                </nav>
            </div>
            <?php /*
            <div class="col-md-6 col-md-offset-1">
                <div class="newsletter">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <span>Subscribe to our newsletter</span></div>
                        <div class="col-md-7 col-sm-8">
                            <form id="subscription_form" class="mail-signup medium-centered" action="#" method="post">
                                <div class="input-group">
                                    <input class="email" name="EMAIL" placeholder="EMAIL" type="text">
                                    <a id="subscription_button" type="submit" class="fas fa-play input-group-addon submit" aria-hidden="true">
                                        <input class="hidden_submit" type="submit"> <span class="sr-only">submit</span> </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> */ ?>
        </div>
    </div>
</div>
<footer id="footer" class="fadeIn wow" style="height: auto; visibility: visible; animation-name: fadeIn;">
    <section class="container footer-in" id="hide-for-vip">
        <div class="row">
            <div class="col-sm-3 col-lg-3 social"><!-- widget_container -->
                <p>CONNECT</p>
                <div class="socialfollow">
                    <a href="http://facebook.com/artdubai.artfair" class="facebook" target="blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="https://twitter.com/artdubai" class="twitter" target="blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="https://www.youtube.com/user/artdubai" class="youtube" target="blank">
                        <i class="fab fa-youtube"></i>
                    </a>
                    <a href="http://instagram.com/artdubai" class="instagram" target="blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a href="https://www.linkedin.com/company/art-dubai" class="instagram" target="blank">
                        <i class="fab fa-linkedin"></i>
                    </a>
                </div>
                <?php /*
                <div class="widget">
                    <h5 class="subtitle">Art Dubai</h5>
                    <div class="textwidget">
                        <p>Building 7,
                            <br> Dubai Design District (d3)
                            <br> PO Box 72645, Dubai, UAE
                            <br> T +971 4 563 1400
                            <br> <a href="mailto:info@artdubai.ae"><strong>Info@artdubai.ae</strong><br> </a></p>
                    </div>
                </div>
                */ ?>
            </div>
            <div class="col-sm-9 col-lg-9 advertising">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 partner">
                        <div>
                            <p>PARTNERS</p>
                            <div class="footer-partners">
                                <a href="http://www.piaget.ae/" alt="Piaget" class="vertical-align"> <img src="https://www.artdubai.ae/wp-content/uploads/2017/09/piaget.png" alt="Piaget" class="vertical-align"></a>
                                <a href="https://www.juliusbaer.com/global/en/home/" alt="julius baer" class="vertical-align"> <img src="https://www.artdubai.ae/wp-content/uploads/2017/09/juliusBar.png" alt="julius baer" class="vertical-align"></a>
                            </div>
                            <p style="font-style: italic;    margin-bottom: 0;padding-top: 7px;font-size:12px">Madinat Jumeirah is the home of Art Dubai</p>
                        </div>
                    </div>
                    <div class="sponsors col-xs-12 col-sm-4">
                        <p> STRATEGIC PARTNER</p> <img src="https://www.artdubai.ae/wp-content/uploads/2017/11/CMYK_Dubai_Culture_grey2-1.png" onclick="window.open('http://www.dubaiculture.gov.ae/en/Pages/default.aspx');return false" style="cursor: pointer;"></div>
                </div>
            </div>
        </div>
    </section>
</footer>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/validate.additional.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/intlTelInput.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dropzone.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/hoverSlippery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/authentication.js"></script>
<script src="<?php echo base_url(); ?>assets/js/functions.js"></script>
<?php $this->load->view('templates/popup'); ?>
    </body>

    <script>
    $(document).ready(function () {
        $('#catalogue').change(function () {
        $('#show_price').fadeToggle();
        });

        $("#invitation-type").change(function () {
            var invitation_type = this.value;
            console.log('changed to = '+invitation_type);

            if(invitation_type == "patrons")
            {
                if($('#cnt_pc').val() == 10)
                {
                    $('#vip-entry-form').find('.btn').prop('disabled',true);
                    
                    $.notify({
                            message: "You can not add more PC!"
                        },{
                            type: 'danger'
                        });
                    return false;
                }else{
                    $('#vip-entry-form').find('.btn').prop('disabled',false);
                }

            }else if(invitation_type == "collectors")
            {
                if($('#cnt_cc').val() == 15)
                {
                    $('#vip-entry-form').find('.btn').prop('disabled',true);
                    
                    $.notify({
                            message: "You can not add more CC!"
                        },{
                            type: 'danger'
                        });
                    return false;
                }else{
                    $('#vip-entry-form').find('.btn').prop('disabled',false);
                }

            }else if(invitation_type == "guest_of_gallery_invites")
            {
                if($('#cnt_vip').val() == 20)
                {
                    $('#vip-entry-form').find('.btn').prop('disabled',true);
                    
                    $.notify({
                            message: "You can not add more VIP!"
                        },{
                            type: 'danger'
                        });
                    return false;
                }else{
                    $('#vip-entry-form').find('.btn').prop('disabled',false);
                }
                
            }else{
                $('#vip-entry-form').find('.btn').prop('disabled',false);
            }
        });
    });
    </script>


    </html>