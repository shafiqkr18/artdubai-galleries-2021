
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Artwork Image Library</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="image-library">

            <div class="container">

                <div class="row">
                    <div class="col-md-12">

                        <div class="text-center">
                            <p>Welcome to the exhibitor Image Library where all of the images uploaded in your application and exhibitor portal are stored. Your image library can be added to and edited at any time.</p>
                            <p>The e-catalogue will be made live in February so please ensure that all your images are tagged as 'show in catalogue' before the 28th of January 2021. You may continue to add more images to the e-catalogue throughout the lead up to the fair.</p>
                            <p>Our VIPs and the public will have direct access to these images, with the newly introduced ability to 'favourite' artworks and create portfolios of works they would like to see during the fair.<br/><br/><br/></p>
                        </div>

                        <form id="upload-form" method="post" action="<?php echo base_url( 'library/upload' ); ?>" class="upload-form">
                            <div class="row">
                                <div class="col-md-5">
                                    <h4>Upload Image</h4>
                                    <div class="dropzone-wrap">
                                        <div class="droparea" id="upload-zone"></div>
                                    </div>
                                    <br>
                                    <fieldset class="row">
										<div class="col-md-12">
                                         <label>Catalogue Exhibition Statement</label>
                                        <textarea rows="4" class="form-control" placeholder="Catalogue Exhibition Statement" id="statement" name="statement"></textarea>
										   
        								</div>
    								</fieldset>
                                   
                                </div>
                                
                                <div class="col-md-7">
                                    <p>Required Image<br> <strong>Size: Minimum 1MB to Maxixmum 20MB, Format: JPG/JPEG, Width: Minimum 2000px</strong></p>
                                    <fieldset class="col2">
                                        <input type="text" placeholder="Artist" name="artist" required>
                                        <input type="text" placeholder="Artwork Title" name="title" required>
                                    </fieldset>
                                    <fieldset class="col2">
                                        <input type="text" placeholder="Year" name="year" required>
                                        <div class="select-custom">
                                            <select required name="medium">
                                                <option value="">Select Tag(s) *</option>
                                                <?php
                                                foreach ( $mediums as $medium ) {
                                                    echo '<option value="'.$medium['id'].'">'.$medium['name'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </fieldset>
									<fieldset class="row">
										<div class="col-md-12">
        									<input type="text" name="medium_text" value="" placeholder="Medium" required>
        								</div>
    								</fieldset>
                                    <fieldset class="col2">
                                        <input type="text" placeholder="Dimensions" name="dimensions" required>
                                        <input type="text" placeholder="Courtesy of" name="courtesy" required>
                                    </fieldset>

                                    <fieldset class="row">
										<div class="col-md-12">
										    <label>CATALOGUE INQUIRIES WILL BE SENT DIRECTLY TO:</label>
        									<input type="email" name="inquiry_email" value="<?php echo (!empty($gp[0]->email) ) ? $gp[0]->email : ''; ?>" placeholder="Inquiry Email" required>
        								</div>
    								</fieldset>
                                    

                                    <fieldset class="row">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label for="catalogue">
                                                    <input type="checkbox" id="catalogue" name="catalogue" value="1"> Show in Catalogue
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="row" id="show_price" style="display:none;">
                                        <div class="col-md-6">
                                        <input type="text" placeholder="Price (USD)" name="price" >
                                        </div>
                                        <div class="col-md-6">
                                        <!-- <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="is_reserved" name="is_reserved" value="1">
                                        <label class="custom-control-label" for="is_reserved">Reserved</label>
                                        </div> -->
                                        <div class="radio" style="margin-top:0px;">
                                        <label><input type="radio" name="is_reserved" value="1" checked>Reserved</label>
                                        </div>
                                        <div class="radio">
                                        <label><input type="radio" name="is_reserved" value="0">Sold</label>
                                        </div>
                                        </div>

                                    </div>
                                    <fieldset class="row">
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6 text-right">
                                            <input type="hidden" name="id">
                                            <input type="submit" class="btn filled small" value="Upload">
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </form>

                        <p class="alert alert-info">To select images for the E-Catalogue please hover over the chosen image, click edit and tick the 'Show in Catalogue' box and click UPDATE.</p>

                        <div class="info">
                            <p><strong>Please note that Art Dubai holds the right to use all images uploaded to the Image Library for marketing purposes. If there are any works you would not like to be used for marketing purposes please remove them from your Image Library.</strong></p>
                        </div>

                        <div class="image-gallery">
                            <h4 class="section-heading text-center">Your Image Gallery</h4>
                            <div class="row">
                                <?php if( !empty( $files ) ) :
                                    foreach ( $files as $file ) :
                                        $obj = json_decode( $file->files );
                                        if($exhibit_years[0]->exhibit_year == $file->exhibit_year || $file->exhibit_year == 0 ):

                                        ?>
                                        <div class="col-md-4">
                                            <figure>
                                                <img src="<?php echo upload_dir( $obj->raw_name . $obj->file_ext ); ?>" alt="<?php echo $file->title; ?>">
                                                <div class="overlay">
                                                    <p>Size: <?php echo number_format((float)($obj->file_size/1024), 2, '.', ''); ?>MB<br><a href="<?php echo base_url( 'image-library/' . $file->id ); ?>"><i class="fa fa-pencil-alt"></i> Edit</a>
                                                        <a href="#" class="delete-image" data-id="<?php echo $file->id; ?>"><i class="fa fa-trash"></i> Delete</a></p>
                                                </div>
                                                <?php echo ( $file->catalogue == 1 ) ? '<span class="label label-success">Show on Catalogue</span>' : ''; ?>
                                            </figure>
                                        </div>
                                        <?php
                                        endif;
                                    endforeach;
                                else: ?>
                                    <div class="col-md-12">
                                        <p class="text-center">Nothing uploaded yet.</p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

	</div>
   
