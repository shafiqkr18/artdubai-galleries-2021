
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Add New Gallery <?php if( $section == 'location' ) { echo 'Location'; } elseif( $section == 'staff' ) { echo 'Staff'; } ?></h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="vip-designations">

            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-box">

                            <?php if( $section == 'location' ) : ?>

                                <form action="<?php echo base_url( 'gallery-profile/create' ); ?>" id="gp-create-form" class="gp-update-form" method="post">
                                    <figure class="col2">
                                        <input type="text" placeholder="Address 1 *" name="address1" value="" required>
                                        <input type="text" placeholder="Address 2" name="address2" value="">
                                    </figure>
                                    <figure class="col2">
                                        <input type="text" placeholder="City *" name="city" value="" required>
                                        <div class="select-custom">
                                            <select name="country" required>
                                                <option value="">Country *</option>
                                                <?php
                                                $countries = config_item('country_list');
                                                foreach ( $countries as $country )
                                                {
                                                    echo '<option value="' . strtolower( $country ) . '">' . $country . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </figure>
                                    <figure class="col3">
                                        <input type="text" placeholder="PO Box *" name="pobox" value="" required>
                                        <input type="text" placeholder="Zip Code *" name="zip" value="" required>
                                        <div class="select-custom">
                                            <select name="type" required>
                                                <option value="">Address type *</option>
                                                <option value="1">Primary</option>
                                                <option value="2">Billing</option>
                                                <option value="3">Gallery</option>
                                            </select>
                                        </div>
                                    </figure>
                                    <figure class="btn-wrap">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn filled small" value="Add">
                                    </figure>
                                </form>

                            <?php elseif( $section == 'staff' ) : ?>

                                <form action="<?php echo base_url( 'gallery-profile/create' ); ?>" id="gp-create-form" class="gp-update-form" method="post">
                                    <figure class="col3">
                                        <input type="text" placeholder="Salutation" name="salutation" value="" required>
                                        <input type="text" placeholder="First Name" name="first_name" value="" required>
                                        <input type="text" placeholder="Last Name" name="last_name" value="" required>
                                    </figure>
                                    <figure class="col2">
                                        <input type="text" id="phone" class="only-numbers" placeholder="Phone" name="phone" value="" required>
                                        <input type="text" id="mobile" class="only-numbers" placeholder="Mobile" name="mobile" value="" required>
                                    </figure>
                                    <figure class="col4">
                                        <input type="email" placeholder="Email" name="email" value="" required>
                                        <input type="text" placeholder="City" name="city" value="" required>
                                        <input type="text" placeholder="Position" name="position" value="" required>
                                        <div class="select-custom">
                                            <select name="type" required>
                                                <option value="">Contact type</option>
                                                <option value="1">Main contact</option>
                                                <option value="2">CC in emails only</option>
                                                <option value="3">Others</option>
                                            </select>
                                        </div>
                                    </figure>
                                    <figure class="btn-wrap">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="hidden" id="phone_code" name="phone_country" value="">
                                        <input type="hidden" id="mobile_code" name="mobile_country" value="">
                                        <input type="submit" class="btn filled small" value="Add">
                                    </figure>
                                </form>

                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>

	</div>
