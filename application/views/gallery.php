<div class="ad-page-wrap">

	<div class="ad-inner-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Gallery Profile</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="gallery-profile-page">

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<div class="ad-sec-header">
						<h3>General Information</h3>
                        <a href="<?php echo base_url( 'gallery-profile/info/' . $gp[0]->id ); ?>" class="edit btn smaller alt"><i class="fas fa-pencil-alt"></i> Edit</a>
					</div>

					<div class="add-section">
						<div class="row">
							<div class="col-md-6">
								<h6>Gallery Name</h6>
								<p><?php echo $gp[0]->name; ?></p>
							</div>
							<div class="col-md-6">
								<h6>Established</h6>
								<p><?php echo $gp[0]->established; ?></p>
							</div>
							<div class="col-md-6">
								<h6>Email</h6>
								<p><?php echo $gp[0]->email; ?></p>
							</div>
							<div class="col-md-6">
								<h6>Website</h6>
								<p><?php echo $gp[0]->website; ?></p>
							</div>
                            <div class="<?php echo ( !empty( $gp[0]->booth2 ) ) ? 'col-md-4' : 'col-md-6'; ?>">
                                <h6>Confirmed Cities</h6>
                                <p>
                                    <?php $lc_selected = json_decode( $gp[0]->cities ); ?>
                                    <?php echo array_to_string( $lc_selected, ' / ' ); ?>
                                </p>
                            </div>
							<div class="col-md-12">
								<h6>Gallery Bio</h6>
								<?php echo autop( $gp[0]->bio ); ?>
							</div>
						</div>
					</div>

					<div class="ad-sec-header">
						<h3>Social Platforms</h3>
					</div>

					<div class="add-section">
						<div class="row">
							<?php
							$links = json_decode( $sp[0]->links );
							foreach ( $links as $link )
							if( is_array( $links ) && !empty( $links ) )
							{
								{
									if( strpos( $link, 'facebook' ) !== false ) {
										$title = 'Facebook link';
									} elseif( strpos( $link, 'twitter' ) !== false ) {
										$title = 'Twitter link';
									} elseif( strpos( $link, 'instagram' ) !== false ) {
										$title = 'Instagram link';
									} else {
										$title = 'Other social link';
									}
									?>
									<div class="col-md-12">
										<h6><?php echo $title; ?></h6>
										<p><?php echo $link; ?></p>
									</div>
									<?php
								}
							}
							else
							{
								?>
								<div class="col-md-12">
									<p>No social media link is added.</p>
								</div>
								<?php
							}
							?>
						</div>
					</div>

					<div class="ad-sec-header">
						<h3>Locations</h3>
                        <a href="<?php echo base_url( 'gallery-profile/add/location' ); ?>" class="edit btn smaller alt"><i class="fas fa-plus"></i> Add New</a>
					</div>

					<div class="add-section no-padding">
						<table class="data-table alt">
							<thead>
							<tr>
								<th>Address 1</th>
								<th>Address 2</th>
								<th>City</th>
								<th>Country</th>
								<th>P O Box</th>
								<th>Zip / Postal Code</th>
								<th>Type</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<?php
							if( !empty( $lc ) )
							{
								foreach ( $lc as $location )
								{
									?>
									<tr>
										<td><?php echo $location->address1; ?></td>
										<td><?php echo $location->address2; ?></td>
										<td><?php echo $location->city; ?></td>
										<td><?php echo $location->country; ?></td>
										<td><?php echo $location->pobox; ?></td>
										<td><?php echo $location->zip; ?></td>
										<td>
											<?php
											if( $location->type == 1 ) {
												echo 'Primary';
											} elseif( $location->type == 2 ) {
												echo 'Billing';
											} elseif( $location->type == 3 ) {
												echo 'Gallery';
											} else {
												echo 'Others';
											}
											?>
										</td>
										<td><a href="<?php echo base_url( 'gallery-profile/location/' . $location->id ); ?>" class="edit"><i class="fas fa-pencil-alt"></i></a><a href="#" class="del delete-gp" data-id="<?php echo $location->id; ?>" data-section="locations"><i class="fas fa-trash"></i></a></td>
									</tr>
									<?php
								}
							}
							?>
							</tbody>
						</table>
					</div>

					<div class="ad-sec-header">
						<h3>Gallery Team</h3>
                        <p><strong>For all future communications please ensure that the correct Main Contact and CCs are reflected below.</strong></p>
					</div>

					<div class="add-section no-padding">
						<table class="data-table alt colored">
							<thead>
							<tr>
								<th>Team Member</th>
								<th>Position</th>
								<th>Type</th>
								<th><a href="<?php echo base_url( 'gallery-profile/add/staff' ); ?>" class="edit btn smaller alt"><i class="fas fa-plus"></i> Add New</a></th>
							</tr>
							</thead>
							<tbody>
							<?php
							if( !empty( $gt ) )
							{
								foreach ( $gt as $member )
								{
									?>
									<tr>
										<td><?php echo ucfirst( $member->salutation ); ?> <?php echo $member->first_name; ?> <?php echo $member->last_name; ?><br>
											<?php echo $member->city; ?><br>
											<?php echo $member->phone_country; ?> <?php echo $member->phone; ?><br>
											<?php echo $member->mobile_country; ?> <?php echo $member->mobile; ?><br>
											<?php echo $member->email; ?></td>
										<td><?php echo $member->position; ?></td>
										<td>
											<?php
											if( $member->type == 1 ) {
												echo 'Main point of contact';
											} elseif( $member->type == 2 ) {
												echo 'Only in CC';
											} else {
												echo 'Others';
											}
											?>
										</td>
										<td><a href="<?php echo base_url( 'gallery-profile/staff/' . $member->id ); ?>" class="edit"><i class="fas fa-pencil-alt"></i></a><a href="#" class="del delete-gp" data-id="<?php echo $member->id; ?>" data-section="gallery_team"><i class="fas fa-trash"></i></a></td>
									</tr>
									<?php
								}
							}
							?>
							</tbody>
						</table>
					</div>

					<div class="row">
						<div class="col-md-12">

							<div class="ad-sec-header">
								<h3>Represented Artists</h3>
							</div>

							<div class="add-section no-padding">
								<table class="data-table alt "><!-- no-actions -->
									<thead>
									<tr>
										<th>First Name</th>
										<th>Surname</th>
                                        <th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if($ar){
										foreach ( $ar as $artist )
										{
											?>
											<tr>
												<td><?php echo $artist->first_name; ?></td>
												<td><?php echo $artist->last_name; ?></td>
												<td class="text-right">
													<a href="<?php echo base_url( 'gallery-profile/artist/' . $artist->id ); ?>" class="edit"><i class="fas fa-pencil-alt"></i></a>
													<a href="#" class="del delete-gp" data-id="<?php echo $artist->id; ?>" data-section="artists"><i class="fas fa-trash"></i></a>
												</td>
											</tr>
											<?php
										}

									}else{
										echo '<tr>
															<td colspan="4">No artist is added.</td>
													</tr>';
									}

									?>
									</tbody>
								</table>
							</div>

						</div>
						<div class="col-md-12">

							<div class="ad-sec-header">
								<h3>Fair Participation</h3>
							</div>

							<div class="add-section no-padding">
								<table class="data-table alt "><!-- no-actions -->
									<thead>
									<tr>
										<th>Art Fair</th>
										<th>Year</th>
										<th>Section</th>
                                        <th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									if( isset( $pr ) && !empty( $pr ) )
                                    {
                                        foreach ( $pr as $event )
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $event->name; ?></td>
                                                <td><?php echo $event->year; ?></td>
                                                <td><?php echo $event->section; ?></td>
                                                <td class="text-right"><a href="<?php echo base_url( 'gallery-profile/participation/' . $event->id ); ?>" class="edit"><i class="fas fa-pencil-alt"></i></a><a href="#" class="del delete-gp" data-id="<?php echo $event->id; ?>" data-section="participation"><i class="fas fa-trash"></i></a></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                        <tr>
                                            <td colspan="4">No fair participation detail is added.</td>
                                        </tr>
                                        <?php
                                    }
									?>
									</tbody>
								</table>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>
