
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Vip Designations</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="vip-designations">

            <div class="intro">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Welcome to your VIP designations page. Here you will find information on complimentary access offered to guests of Art Dubai participating galleries.<br> The guests you upload to this section will be receiving digital invitations and access to the Art Dubai VIP Portal and Art Dubai App.</p>

														<p style="color: red">
                                                        <!-- The deadline to submit your VIP Designations is 10th December, 2020 -->
                                                        <!-- You can upload your VIP Designations from <strong>December 17<sup>th</sup>, 2021</strong> -->
                                                        <!-- We apologise for the delay in launching the VIP Page, there has been a number of technical errors that our web developers are working quickly to resolve. We will share an email with you as soon as the VIP Page is live as well as the extended deadline date for your VIP Designations. -->
                                                        </p>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="types-wrap">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="icon"><img src="assets/images/icon27.png" alt=""></div>
                            <p>All invitation requests for all tiers (<strong>Patrons Circle and Collectors Circle & VIP</strong>) require you to submit guest details in the form, to ensure they receive personalised invitations set from Art Dubai on your behalf.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="guestnames">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h3 class="section-heading text-center">Submit Guest Names</h3>
                        <p class="text-center" style="margin: -10px 0 30px;">Galleries can invite 10 Patrons Circle, 15 Collectors Circle and 20 VIP guests.</p>
                        <p class="text-center" style="margin: 10px 30px 30px;">
                            Patrons Circle is Art Dubai’s highest invitation tier – guests have first access to the fair from Tuesday, March 16, Collectors Circle have access later in the day on <strong>Tuesday, March 16</strong>, while VIP guests have access to the fair on <strong>Wednesday, March 17</strong>.
                        </p>
                        <p class="text-center" style="margin: 0px 30px 30px;"> Please include their details below and click submit.</p>

                        <div class="form-box">
                            <form action="<?php echo base_url( 'vip/create' ); ?>" id="vip-entry-form" class="vip-submission-form" method="post">
                                <legend>Personal Information</legend>
                                <figure class="col3">
                                    <input type="text" placeholder="Salutation *" name="salutation" required>
                                    <input type="text" placeholder="First name *" name="first_name" required>
                                    <input type="text" placeholder="Surname *" name="sur_name" required>
                                </figure>
                                <figure class="col2">
                                    <input type="text" placeholder="Company *" name="company" required>
                                    <input type="text" placeholder="Job title" name="job_title">
                                </figure>
                                <figure class="col2">
                                    <input type="text" id="mobile" placeholder="Mobile *" name="mobile" required>
                                    <input type="email" placeholder="Email *" name="email" required>
                                </figure>
                                <legend>Address</legend>
                                <!-- <figure>
                                    <input type="text" placeholder="Apt/Flat/Building name" name="apt_flat" value="">
                                </figure> -->
                                <!-- <figure>
                                    <input type="text" placeholder="Street/Block *" name="street_block" value="" required>
                                </figure> -->
                                <figure >
                                    <input type="text" placeholder="City *" name="city" required>
                                    <!-- <input type="text" placeholder="Postal Code / Zip Code" name="postal_zip_code" value="">
                                    <input type="text" placeholder="PO Box" name="po_box" value=""> -->
                                </figure>
                                <figure class="col2">
                                    <div class="select-custom">
                                        <select name="country" id="country" required>
                                            <option value="">Select Country *</option>
                                            <?php
                                            $countries = config_item('country_list');
                                            foreach ( $countries as $country )
                                            {
                                                echo '<option value="' . strtolower( $country ) . '">' . $country . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="select-custom">
                                        <select name="state" id="state" disabled required>
                                            <option value="">State / Province *</option>
                                        </select>
                                    </div>
                                </figure>
                                <figure>
                                    <div class="select-custom">
                                        <select name="invite_type" id="invitation-type" required>
                                            <option value="">Invitation Type *</option>
                                            <option value="patrons">Patrons Circle</option>
                                            <option value="collectors">Collectors Circle</option>
											<option value="guest_of_gallery_invites">VIP</option>
                                        </select>
                                    </div>
                                </figure>
                                <figure class="btn-wrap">
                                    <input type="hidden" name="gallery_name" value="<?php echo $user->galleryname; ?>">
                                    <input type="hidden" id="mobile_code" name="mobile_code" value="">
                                    <input type="submit" class="btn filled small" value="Submit">
                                </figure>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="submission-table">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <input type="hidden" id="cnt_pc" name="cnt_pc" value="<?php echo count(array($patrons));?>">
                        <h3 class="section-heading text-center">Patrons Circle Submissions</h3>
                        <table class="table data-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Designations</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( !empty( $patrons ) )
                            {
                                $count = 1;
                                foreach ( $patrons as $entry )
                                {
                                    if( $entry->invite_type == 'patrons' )
                                    {
	                                    echo '<tr>
                                            <td>' . ucfirst( $entry->salutation ) . ' ' . ucfirst( $entry->first_name ) . ' ' . ucfirst( $entry->sur_name ) . '</td>
                                            <td>' . ucfirst( $entry->invite_type ) . '</td>
                                            <td>' . $count . '/10</td>
                                            <td><a href="' . base_url( 'vip-designations/' . $entry->id ) . '" class="edit update-vip" data-id="' . $entry->id . '"><i class="fas fa-pencil-alt"></i></a><a href="" class="del delete-vip" data-id="' . $entry->id . '"><i class="fas fa-trash-alt"></i></a></td>
                                        </tr>';
	                                    $count++;
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                        <h3 class="section-heading text-center">Collectors Circle Submissions</h3>
                        <input type="hidden" id="cnt_cc" name="cnt_cc" value="<?php echo count(array($collectors));?>">
                        <table class="table data-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Designations</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( !empty( $collectors ) )
                            {
	                            $count = 1;
	                            foreach ( $collectors as $entry )
	                            {
		                            if( $entry->invite_type == 'collectors' )
		                            {
			                            echo '<tr>
                                            <td>' . ucfirst( $entry->salutation ) . ' ' . ucfirst( $entry->first_name ) . ' ' . ucfirst( $entry->sur_name ) . '</td>
                                            <td>' . ucfirst( $entry->invite_type ) . '</td>
                                            <td>' . $count . '/15</td>
                                            <td><a href="' . base_url( 'vip-designations/' . $entry->id ) . '" class="edit update-vip" data-id="' . $entry->id . '"><i class="fas fa-pencil-alt"></i></a><a href="" class="del delete-vip" data-id="' . $entry->id . '"><i class="fas fa-trash-alt"></i></a></td>
                                        </tr>';
			                            $count++;
		                            }
	                            }
                            }
                            ?>
                            </tbody>
                        </table>

						<h3 class="section-heading text-center">VIP Submissions</h3>
                        <input type="hidden" id="cnt_vip" name="cnt_vip" value="<?php echo count(array($guest_of_gallery_invites));?>">
                        <table class="table data-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Designations</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( !empty( $guest_of_gallery_invites ) )
                            {
                                $count = 1;
                                foreach ( $guest_of_gallery_invites as $entry )
                                {
                                    if( $entry->invite_type == 'guest_of_gallery_invites' )
                                    {
	                                    echo '<tr>
                                            <td>' . ucfirst( $entry->salutation ) . ' ' . ucfirst( $entry->first_name ) . ' ' . ucfirst( $entry->sur_name ) . '</td>
                                            <td>VIP</td>
                                            <td>' . $count . '/20</td>
                                            <td><a href="' . base_url( 'vip-designations/' . $entry->id ) . '" class="edit update-vip" data-id="' . $entry->id . '"><i class="fas fa-pencil-alt"></i></a><a href="" class="del delete-vip" data-id="' . $entry->id . '"><i class="fas fa-trash-alt"></i></a></td>
                                        </tr>';
	                                    $count++;
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>


            <div class="intro">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Guests will not be given access to the Gallery Halls outside fair opening hours.</p>
                        </div>
                    </div>
                </div>
            </div>

<!--            <div id="vip-submission-popup" class="vip-submission-popup white-popup mfp-hide alt">-->
<!--                <p>This guest has already been invited to Art Dubai. You may cancel this action and reallocate the slot to another guest or continue with your submission.</p>-->
<!--                <a href="" class="btn filled small alt mfp-close">Cancel</a>-->
<!--                <a href="" class="btn filled small">Submit</a>-->
<!--            </div>-->

        </div>

	</div>
   
