<div class="ad-page-wrap">

	<div class="ad-inner-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Booth Design</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="booth-design">

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<div class="info text-center">
						<?php echo $data->box1; ?>
					</div>

					<div class="box">
						<h6>Contact Person</h6>
						<div class="row row-eq-height">
							<div class="col-md-9">
								<!-- <span class="avatar"><img width="78" src="<?php //echo upload_dir_admin( $data->image->file_name ); ?>" alt=""></span> -->
								<div class="data">
									<?php echo $data->box2; ?>
								</div>
							</div>
							<div class="col-md-3 text-right">
								<span><img src="assets/images/logo.png" alt=""></span>
							</div>
						</div>
					</div>

					<div class="booth-design">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
							    <h6 class="section-heading text-center">Booth Design</h6>
							    <p class="text-center">This is your Booth Design Folder – where your Booth Design(s) will be stored + the Booth Design Guide which provides further information on how to layout your sketch.
							    </p>
							    <p class="text-center"><strong>Your Booth Design should be submitted by January 26, 2021</strong></p>
							    <p class="text-center">
							        <img width="150" src="assets/images/img-booth-design.png" alt="" style="display: inline-block; ">
							    </p>
							    <p class="text-center">
							        <a title="Individual Booth Plan" class="btn btn-yellow" target="_blank" href="<?php //echo ( !empty( $indiviual_booth_plan ) ) ? $indiviual_booth_plan : ''; ?>" style=" text-transform: none; pointer-events: none;">
							            <!-- To view your <strong>Individual Booth Plan</strong> please click
							            <strong> here</strong> -->
													You will be able to download your individual booth plan from here shortly.
							         </a>
							        </span>
							    </p><br>
							    <p>To design your booth:</p><br>
							    <p>- Download the PDF in the above link and draw directly on the plan</p>
							    <p><strong>- Draw all required specifications: including extra walls (+additional paint colours), lighting and socket locations</strong></p>
							    <p>- Scan your designed booth</p>
							    <p>- Share your Booth Design with artdubaigalleries@electradubai.ae making sure to keep genna@artdubai.ae in cc</p>
							    <p>- The Subject of the email should say: [Gallery Name] Booth Design</p><br><br>
							</div>
						</div>

					</div>
					<?php /*
					<div class="info text-center">
						<?php echo $data->box3; ?>
					</div> */?>


					<div class="box-filled">
						<div class="row">
							<div class="col-md-4  col-md-offset-2">
								<span class="icon"><img src="assets/images/icon3.png" alt=""></span>
								<h4>Booth Manual</h4>
								<p>Submit your booth design by January 26, 2021</p>
								<a href="<?php echo upload_dir( 'pdf/AD2021_Electra_Galleries_Manual.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
							</div>
							<?php /*
							<div class="col-md-4">
								<span class="icon"><img src="assets/images/icon4.png" alt=""></span>
								<h4>Booth Design Template</h4>
								<p>Submit to contractors by January 20, 2020</p>
								<a href="<?php echo upload_dir( 'pdf/AD2020_Booth_Design_Layout.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
							</div> */?>
							<div class="col-md-4">
								<span class="icon"><img src="assets/images/icon5.png" alt=""></span>
								<h4>Extra Order Forms</h4>
								<p>Submit to contractors by February 8, 2021</p>
								<a href="<?php echo upload_dir( 'pdf/AD2021_Additional_Order_Form.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
							</div>
						</div>
					</div>

                    <div class="info text-center">
                        <h4 class="section-heading">Gallery Name Board</h4>
                        <?php echo $data->box4; ?>
                    </div>

                    <div class="row">
                        <?php if(!empty( $gallery_fascia ) && $gallery_fascia[0]->approved == 1 || !empty($gallery_profile)):?>
                            <div class="col-md-8 col-md-offset-2">
                                <div class="box booth-info">


                                        <?php if( empty( $gallery_booth ) ) { ?>
                                            <h2><small class="label label-danger" style="font-size: 12px;">Booth name will be added by administrator</small></h2>
                                        <?php }else{ ?>
                                        	<h2><?php echo ( !empty( $gallery_booth ) ) ? $gallery_booth : '-'; ?></h2>

                                        <?php  } ?>

                                    <h3><strong class="name"><?php echo ( !empty( $gallery_fascia ) ) ? $gallery_fascia[0]->name : $gallery_profile[0]->name;; ?></strong></h3>
                                    <h3  class="city"><?php echo ( !empty( $gallery_fascia ) ) ? $gallery_fascia[0]->address1 : ''; ?></h3>
                                </div>
                                <p class="text-center"><small class="label label-warning">The above image is just for representation purposes – the design may be subject to change</small></p>
                            </div>

                        <?php endif; ?>

                        <div class="col-md-10 col-md-offset-1">
                            <div class="box">
                                <form id="gallery-fascia-form" action="<?php echo base_url( 'submit-gallery-fascia' ); ?>" method="post">
                                    <fieldset class="col2">
                                        <input  id="gallery-booth-name"  type="text" name="name" maxlength="100" placeholder="Gallery Name" value="<?php echo ( !empty( $gallery_fascia ) ) ? $gallery_fascia[0]->name : $gallery_profile[0]->name; ?>" required>
                                        <input  id="gallery-booth-city"  type="text" name="address1" maxlength="100" placeholder="City" value="<?php echo ( !empty( $gallery_fascia ) ) ? $gallery_fascia[0]->address1 : ''; ?>" required>
                                    </fieldset>
                                    <fieldset class="form-check">
                                        <input type="checkbox" class="form-check-input" id="approveGallery" name="approved" value="1"
                                        <?php echo  (!empty( $gallery_fascia[0]->approved ) && $gallery_fascia[0]->approved == 1 ) ? 'checked' : '' ; ?>>
                                        <label class="form-check-label" for="approveGallery">I approve the spelling and formatting of my gallery name and my gallery's locations</label><br/>
                                    </fieldset>
                                    <fieldset class="btn-wrap">
                                        <?php $userData = $_SESSION['userData']; ?>
                                        <input type="hidden" name="userID" value="<?php echo $userData->id; ?>">
                                        <input type="submit" class="btn filled small" value="Approve">
                                    </fieldset>
                                </form>
                            </div>
                        </div>

                    </div>

				</div>
			</div>
		</div>

	</div>

</div>
