<div class="ad-page-wrap">

    <div class="ad-inner-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>List of Works</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="travel-page">

        <div class="container">

            <div class="content-box">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-heading text-center">LIST OF WORKS</h3>
                    </div>
                    <div class="col-md-8 col-md-offset-2">
                        <article class="text-center">
                            <p>Kindly ensure that the List of Works that you will be bringing and exhibiting at the fair is sent to <a href="mailto:genna@artdubai.ae">genna@artdubai.ae</a> by the <strong>15th of February</strong>. Please download the List of Works Template below and complete the necessary sections ensuring all images are submitted with full captions.</p>
                        </article>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <a href="<?php echo upload_dir( 'pdf/AD_Lists_Works.docx' ); ?>" download="AD_Lists_Works" target="_blank" class="btn">Download Template</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-box">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-heading text-center">PRICE LISTS</h3>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <article class="text-center">
                            <p>The VIP team at Art Dubai is constantly reaching out to collectors, museums groups and collectors in an effort to draw the right audiences to the fair in March and make sure we meet the expectations of both galleries and our VIP, Collectors and Museums representatives in terms of sales.</p>
                            <p>If you would like to share your indicative price ranges or a specific price list please email them directly to <a href="mailto:genna@artdubai.ae">genna@artdubai.ae</a>. We can then do our best to approach the right collectors for you and match visitors relevant to your gallery. Needless to say that the information will be kept confidential and will only serve to inform our ongoing discussions with collectors and visiting groups.</p>
                            Please download the Price List template below and ensure that you send your Price Lists by the <strong>15th of February</strong>.
                        </article>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <a href="<?php echo upload_dir( 'pdf/AD_Prices_Lists.docx' ); ?>" download="AD_Prices_Lists" target="_blank" class="btn">Download Template</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>