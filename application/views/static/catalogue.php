<div class="ad-page-wrap">

    <div class="ad-inner-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Catalogue Entry</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="catalogue-entry-page">

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="info text-center">
                        <p>Please review the below information and upload an image for our online catalogue.<br> The information below may be edited and must be confirmed by January 4, 2019.<br> A confirmation is registered once you click the button at the end of the page.</p>
                        <div class="icon"><img src="assets/images/icon9.png" alt=""></div>
                    </div>

                    <div class="gallery-info">
                        <div class="row">
                            <div class="col-md-6">
                                <h6>Gallery Name</h6>
                                <p><?php echo ( !empty( $gp[0]->name ) ) ? $gp[0]->name : ''; ?></p>
                            </div>
                            <div class="col-md-6">
                                <h6>City, Country</h6>
                                <p>Dubai, UAE</p>
                            </div>
                            <div class="col-md-12">
                                <h6>Gallery Bio</h6>
                                <p><?php echo ( !empty( $gp[0]->bio ) ) ? $gp[0]->bio : ''; ?></p>
                            </div>
                            <div class="col-md-4">
                                <h6>Exhibition Artist</h6>
                                <p>John Doe</p>
                            </div>
                            <div class="col-md-4">
                                <h6>Represnted Artist</h6>
                                <p>John Doe</p>
                            </div>
                            <div class="col-md-4">
                                <h6>Gallert Director</h6>
                                <p>John Doe</p>
                            </div>
                            <div class="col-md-12">
                                <h6>Gallert Address</h6>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <div class="col-md-3">
                                <h6>Country Code</h6>
                                <p>+971</p>
                            </div>
                            <div class="col-md-3">
                                <h6>Phone Number</h6>
                                <p>8765 54322</p>
                            </div>
                            <div class="col-md-3">
                                <h6>Email</h6>
                                <p><?php echo ( !empty( $gp[0]->email ) ) ? $gp[0]->email : ''; ?></p>
                            </div>
                            <div class="col-md-3">
                                <h6>Website</h6>
                                <p><a href="<?php echo ( !empty( $gp[0]->website ) ) ? $gp[0]->website : ''; ?>" target="_blank"><?php echo ( !empty( $gp[0]->website ) ) ? $gp[0]->website : ''; ?></a></p>
                            </div>
                        </div>
                    </div>

                    <form id="upload-form" method="post" action="<?php echo base_url( 'library/upload' ); ?>" class="upload-form">
                        <div class="info">
                            <div class="row">
                                <div class="col-md-5">
                                    <h4>Upload Catalogue Image</h4>
                                    <div class="dropzone-wrap">
                                        <div class="droparea" id="upload-zone"></div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <p>Required Image<br> <strong>Size: Minimum 1MB to Maxixmum 20MB, Format: JPG/JPEG, Width: Minimum 2000px</strong></p>
                                    <fieldset class="col2">
                                        <input type="text" placeholder="Artist" name="artist" required>
                                        <input type="text" placeholder="Artwork Title" name="title" required>
                                    </fieldset>
                                    <fieldset class="col2">
                                        <input type="text" placeholder="Year" name="year" required>
                                        <input type="text" placeholder="Medium" name="medium" required>
                                    </fieldset>
                                    <fieldset class="col2">
                                        <input type="text" placeholder="Dimensions" name="dimensions" required>
                                        <input type="text" placeholder="Courtesy of" name="courtesy" required>
                                    </fieldset>
                                    <fieldset class="text-right">
                                        <input type="submit" class="btn filled smaller" value="Upload">
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="btn-wrap text-center">
                            <input type="submit" class="btn filled smaller" value="Confirm Catalogue Information">
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>

</div>