
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Important Documents</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="exhibitor-passes">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="info">
                            <p>This page is where all important documents & PDFs relating to your participation at Art Dubai 2021 will appear and will be available for download.</p>
                            <p>For any forms that need to be filled out:</p>
                            <ul>
                                <li>Download the document</li>
                                <li>Fill out the relevant information</li>
                                <li>Share directly with Genna <a href="mailto:genna@artdubai.ae">genna@artdubai.ae</a></li>
                            </ul>
                        </div>
                    </div>
                    <?php /*
                    <div class="row travel-page">
                        <div class="col-md-12">
                            <div class="box-filled">
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="icon"><img src="assets/images/icon10.png" alt=""></span>
                                                <h4>Art Dubai Logo</h4>
                                                <a href="<?php echo upload_dir( 'Art_Dubai_2019_Logo.png' ); ?>" target="_blank" class="download-btn">Download</a>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="icon"><img src="assets/images/icon11.png" alt=""></span>
                                                <h4>Fair Schedule</h4>
                                                <a href="<?php echo upload_dir( 'pdf/AD2021_Galleries_Fair_Schedule.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    */ ?>
                    <div class="row">
                        <table class="table data-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>File Name</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( !empty( $documents ) )
                            {
                                $counter = 1;
	                            foreach ( $documents as $document )
	                            {
	                                $file = json_decode( $document->file );
		                            echo '<tr>
                                        <td>' . $counter . '</td>
                                        <td class="icon-pdf-download">' . $document->name . '</td>
                                        <td><a href="'.upload_dir_admin( 'documents/' . $file->file_name ).'" target="_blank" class="edit"><i class="fas fa-download"></i> Download</a></td>
                                    </tr>';
		                            $counter++;
	                            }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>

	</div>
