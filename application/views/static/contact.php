<div class="ad-page-wrap">

	<div class="ad-inner-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Contact</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="contact-page">

		<div class="map">
			<address>
				<h5>Art Dubai</h5>
				<p>Dubai Design District
					Building 7, Floor 4
					Office 403
					PO Box 72645
					Dubai, UAE<br>
					+971 4 563 1400<br>
                    <a href="mailto:info@artdubai.ae">info@artdubai.ae</a></p>
			</address>
		</div>

		<div class="container">
			<div class="row row-no-padding row-eq-height">
				<div class="col-md-5">
					<div class="info alt">
                        <?php echo $data->box1; ?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="info alt"></div>
				</div>
				<div class="col-md-5">
					<div class="info alt">
                        <?php echo $data->box2; ?>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row row-no-padding">
				<div class="col-md-5">
                    <?php echo $data->box3; ?>
				</div>
				<div class="col-md-2">
					<div class="info"></div>
				</div>
				<div class="col-md-5">
                    <?php echo $data->box4; ?>
				</div>
			</div>
		</div>

	</div>

</div>