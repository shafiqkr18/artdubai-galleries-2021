
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Volunteer Request</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="volunteer-request">

            <div class="container">
                <div class="row row-eq-height row-no-padding">
                    <div class="col-md-6">
                        <article>
                            <?php echo $data->box1; ?>
                        </article>
                    </div>
                    <div class="col-md-6">
                        <figure><img src="assets/images/volunteer-request.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <article class="text-center">
                            <form id="volunteer-request" action="<?php echo base_url( 'submit-volunteer-request' ) ?>" method="post">
                                <?php $userData = $_SESSION['userData']; ?>
                                <input type="hidden" name="userID" value="<?php echo $userData->id; ?>">
                                <input type="submit" class="btn filled submit-volunteer-request" value="Submit Request">
                            </form>
                        </article>
                    </div>
                </div>
            </div>

        </div>

        <div id="volunteer-request-popup" class="white-popup mfp-hide">
            <i class="fa fa-check-circle"></i>
            <h4>Thank You</h4>
            <p>Your request for a volunteer has been received. We will be in touch closer to the fair with further details. </p>
        </div>

	</div>
