<div class="ad-page-wrap">

	<div class="ad-inner-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Shipping Information</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="shipping-page">

		<div class="container">

			<div class="row">
				<div class="col-md-12">
					<h3 class="section-heading text-center">Art Dubai Official Shipping Partners</h3>
				</div>
			</div>

			<div class="ad-partner-wrap">
				<div class="row row-eq-height">
					<div class="col-md-6">
						<div class="ad-partner">
							<h4><?php echo $data->title1; ?></h4>
							<div class="clearfix">
								<!-- <div class="avatar"><img src="<?php //echo upload_dir_admin( $data->image1->file_name ); ?>" alt=""></div> -->
								<div class="logo"></div>
							</div>
								<?php echo $data->box1; ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="ad-partner">
							<h4><?php echo $data->title2; ?></h4>
							<div class="clearfix">
								<!-- <div class="avatar"><img src="<?php //echo upload_dir_admin( $data->image2->file_name ); ?>" alt=""></div> -->
								<div class="logo"></div>
							</div>
								<?php echo $data->box2; ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">

					<div class="info text-center">
						<?php echo $data->box3; ?>
					</div>

					<div class="content-box">
						<div class="row row-eq-height row-no-padding">
							<div class="col-md-6">
								<article>
										<?php echo $data->box4; ?>
								</article>
							</div>
							<div class="col-md-6">
								<figure><img src="assets/images/shipping.jpg" alt=""></figure>
							</div>
						</div>
					</div>

					<div class="info text-center">
						<p><?php echo $data->box5; ?></p>
					</div>

					<div class="box-filled">
						<h4>Please read through the below documents before contacting the shippers.</h4>
						<div class="row">
							<div class="col-md-3">
								<span class="icon"><img src="assets/images/icon6.png" alt=""></span>
								<h4>Shipping & Customs<br> Procedures</h4>
								<a href="<?php echo upload_dir( 'pdf/AD2021_Shipping_Customs_Procedures.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
							</div>
							<div class="col-md-3">
								<span class="icon"><img src="assets/images/icon7.png" alt=""></span>
								<h4>Packing List<br> Template</h4>
								<a href="<?php echo upload_dir( 'pdf/AD2020_Packing_Template.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
							</div>
							<div class="col-md-3">
								<span class="icon"><img src="assets/images/icon8.png" alt=""></span>
								<h4>Commercial<br> Template</h4>
								<a href="<?php echo upload_dir( 'pdf/AD2020_Commmercial_Invoice_Template.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
							</div>
                            <div class="col-md-3">
                                <span class="icon"><img src="assets/images/icon29.png" alt=""></span>
                                <h4>Indicative<br> Rates</h4>
                                <a href="<?php echo upload_dir( 'pdf/AD2020_Shipping_Rates.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
                            </div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>
