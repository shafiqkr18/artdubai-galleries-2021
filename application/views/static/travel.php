<div class="ad-page-wrap">

    <div class="ad-inner-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Travel</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="travel-page">

        <div class="container">

            <div class="row">
                <div class="col-md-12 text-center">
                    <?php /*
                    <a style="margin: 35px auto;" href="<?php echo upload_dir( 'pdf/AD_Travel_Guide.pdf' ); ?>" target="_blank" class="btn simple btn-yellow">
                        <img width="48" src="assets/images/icon13.png">
                        Download Travel Guide
                    </a> */ ?>

                    <?php /*
                    <div class="box-filled">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="icon"><img src="assets/images/icon10.png" alt=""></span>
                                        <h4>Art Dubai Logo</h4>
                                        <a href="<?php echo upload_dir( 'Art_Dubai_2019_Logo.png' ); ?>" target="_blank" class="download-btn">Download</a>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="icon"><img src="assets/images/icon11.png" alt=""></span>
                                        <h4>Fair Schedule</h4>
                                        <a href="<?php echo upload_dir( 'pdf/AD2021_Galleries_Fair_Schedule.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
                                    </div>

                                    <div class="col-md-3">
                                        <span class="icon"><img src="assets/images/icon12.png" alt=""></span>
                                        <h4>Fair Etiquette</h4>
                                        <a href="<?php echo upload_dir( 'pdf/AD2020_Fair_Etiquette.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
                                    </div>
                                    <div class="col-md-12">
                                        <span class="icon"><img src="assets/images/icon13.png" alt=""></span>
                                        <h4>Travel Guide</h4>
                                        <a href="<?php echo upload_dir( 'pdf/AD_Travel_Guide.pdf' ); ?>" target="_blank" class="download-btn">Download</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>*/ ?>
                </div>
            </div>

            <div class="content-box">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-heading text-center">HOTELS</h3>
                        <p class="text-center">Please find our preferential rates below.</p>
                    </div>
                    <div class="col-md-3">
                        <figure><img src="assets/images/hotel-logo.png" alt=""></figure>
                    </div>
                    <div class="col-md-9">
                        <article>
                            <?php echo $data->box1; ?>
                        </article>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <form id="accommodation-request" action="<?php echo base_url( 'submit-accommodation-request' ) ?>" method="post">
                                <?php $userData = $_SESSION['userData']; ?>
                                <input type="hidden" name="userID" value="<?php echo $userData->id; ?>">
                                <input type="submit" class="btn filled submit-accommodation-request" value="Submit Request">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="accommodation-request-popup" class="white-popup mfp-hide">
                <i class="fa fa-check-circle"></i>
                <h4>Thank You</h4>
                <p>for your request for accommodation & travel. An email will follow shortly for further details. </p>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <figure class="image-box">
                        <a href="https://www.jumeirah.com/en/hotels-resorts/dubai/madinat-jumeirah/al-qasr/offers/uae-residents-offer-at-jumeirah-al-qasr/?gclid=EAIaIQobChMI6cDRqeDL3gIVxJTVCh0BHAPdEAAYASAAEgJtsPD_BwE&gclsrc=aw.ds" target="_blank"><img src="assets/images/hotel1.jpg" alt=""></a>
                        <h5>Al Qasr</h5>
                        <h6>Madinat Jumeirah</h6>
                    </figure>
                </div>
                <div class="col-md-6">
                    <figure class="image-box">
                        <a href="https://www.jumeirah.com/en/hotels-resorts/dubai/madinat-jumeirah/jumeirah-al-naseem/?currency=AED&gclid=Cj0KEQiAvNrBBRDe3IOwzLn6_O4BEiQAmbK-Dur_1oK5friNniPEXuxmatDiCR6PnzJMXsgiWRZVIPMaAi3k8P8HAQ&gclsrc=aw.ds&dclid=CKf84eCswdACFQ6kFgodUioKJw" target="_blank"><img src="assets/images/hotel2.jpg" alt=""></a>
                        <h5>Al Naseem</h5>
                        <h6>Madinat Jumeirah</h6>
                    </figure>
                </div>
                <div class="col-md-6">
                    <figure class="image-box">
                        <a href="https://www.jumeirah.com/en/hotels-resorts/dubai/madinat-jumeirah/dar-al-masyaf/?cm_mmc_o=PyzEp%20VV2CjCKA_zb%2f43K-4%2f-HK%2fHWCjCPyzEp%20(H0zgf)CjCpzy%20zk%20FzlYzu%20FzpbEzf%20XAFwbyzt&gclid=CKub_MuMnawCFYEZ4QodPWUoAA" target="_blank"><img src="assets/images/hotel3.jpg" alt=""></a>
                        <h5>Dar al Masyaf</h5>
                        <h6>Madinat Jumeirah</h6>
                    </figure>
                </div>
                <div class="col-md-6">
                    <figure class="image-box">
                        <a href="https://www.jumeirah.com/en/hotels-resorts/dubai/madinat-jumeirah/mina-a-salam/" target="_blank"><img src="assets/images/hotel4.jpg" alt=""></a>
                        <h5>Mina A’Salam</h5>
                        <h6>Madinat Jumeirah</h6>
                    </figure>
                </div>
                <?php /*
                <div class="col-md-6 col-md-offset-3">
                    <figure class="image-box">
                        <a href="https://www.zabeelhouse.com/en/thegreens" target="_blank"><img src="assets/images/hotel5.jpg" alt=""></a>
                        <h5>Zabeel House</h5>
                        <h6 style="margin-bottom: 10px;">The Greens</h6>
                        <p style="margin-bottom: 45px;">The hotel is high on design synonymous with other hotels within the Zabeel House by Jumeirah collection; but inspired by New York loft living, with contemporary and yet quirky art and design throughout the hotel, representing the spirit of the Zabeel House by Jumeirah brand perfectly. This 4* upscale hotel is centrally-located near the fair and where most of our hosted guests will be staying.</p>
                    </figure>
                </div> */ ?>

                <div class="col-md-6 col-md-offset-3">
                    <figure class="image-box">
                        <img src="assets/images/AD_20_Hotel-Room-sale.jpg" alt="">
                        <h5>VIDA HILLS</h5>
                        <?php /*<h6 style="margin-bottom: 10px;">The Greens</h6>*/ ?>
                        <p style="margin-bottom: 45px;">As most of the Exhibitors, Curators, Museum Directors & Guest Speakers will be staying at Vida Hills, Art Dubai will be providing special hotel rates for single & double rooms. The hotel is a 12 minute drive away from the fair and rates are set at AED 570 (single occupancy) and AED 615 (double occupancy) inclusive of taxes, free WiFi and breakfast. Rooms are on a first come, first serve basis so please be sure to contact <a href= "mailto:cristina@artdubai.ae">cristina@artdubai.ae</a> with your travel requests as soon as possible.</p>
                    </figure>
                </div>
            </div>

        </div>

        <div class="box-filled text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="icon"><img src="assets/images/icon28.png" alt=""></div>
                        <?php echo $data->box2; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
