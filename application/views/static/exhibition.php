<div class="ad-page-wrap">

    <div class="ad-inner-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>My Exhibition</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="travel-page">

        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <div class="ad-sec-header">
                        <h3>Booth Information</h3>
                    </div>

                    <div class="add-section booth-info">

                        <div class="row">
                          <div class="col-md-12">
                            <p style="border-bottom: 0px;">Your booth information will be updated shortly.</p>
                          </div>
                            <?php /* ?>
                            <div class="col-md-6">
                                <?php
                                // Booth 1 Information
                                $b1         = (!empty( $gp[0]->booth1 ) ) ? explode('|', $gp[0]->booth1) : '' ;
                                $b1_m2      = (!empty( $gp[0]->booth1_m2 ) ) ? $gp[0]->booth1_m2 : '' ;
                                $b1_link    = (!empty( $gp[0]->booth1_img_link ) ) ? $gp[0]->booth1_img_link : '' ;
                                $b1_type    = '';
                                if(!empty($b1)):
                                    foreach ( $tp as $type ) :
                                        if($type['id'] == $b1[1]):
                                           $b1_type = $type['name'];
                                        endif;
                                    endforeach;
                                endif;

                                // Booth 2 Information
                                $b2         = (!empty( $gp[0]->booth2 ) ) ? explode('|', $gp[0]->booth2) : '' ;
                                $b2_m2      = (!empty( $gp[0]->booth2_m2 ) ) ? $gp[0]->booth2_m2 : '' ;
                                $b2_link    = (!empty( $gp[0]->booth2_img_link ) ) ? $gp[0]->booth2_img_link : '' ;
                                $b2_type    = '';
                                if(!empty($b1)):
                                    foreach ( $tp as $type ) :
                                        if($type['id'] == $b2[1]):
                                           $b2_type = $type['name'];
                                        endif;
                                    endforeach;
                                endif;

                                ?>
                                <h5>Booth <?php echo ( !empty( $b2 ) && !empty( $b2_m2 )  ) ? '1' : ''; ?></h5>

                                <div>
                                    <?php if(!empty( $b1[0] )): ?>
                                        <p><strong>Number:</strong> <?php echo (!empty( $b1[0] ) ) ? $b1[0] : ''; ?></p>
                                        <?php /*
                                        <?php $booth1_allocation = json_decode($gp[0]->booth1_allocation); ?>
                                        <p><strong>Allocation:</strong> <?php echo (!empty( $booth1_allocation->file_name) ) ? '<a target="_blank" href="'.base_url('admin/uploads/booth-allocation/'.$booth1_allocation->file_name).'">'.$booth1_allocation->file_name.'</a>' : '' ;?></p>
                                        * ?>
                                        <p><strong>m2:</strong> <?php echo $b1_m2; ?></p>
                                        <p>To view your <?php echo $b1_type; ?> Booth Allocation click <strong><a target="_blank" href="<?php echo $b1_link; ?>" >here</a></strong></p>
                                    <?php else: ?>
                                        <span class="label label-danger">Your Booth Number(s) will be added shortly</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if( !empty( $b1 ) &&  !empty( $b2_m2 )) : ?>
                                <div class="col-md-6">
                                    <h5>Booth 2</h5>
                                    <div>
                                        <p><strong>Number:</strong> <?php echo $b2[0]; ?></p>
                                        <?php /*
                                        <?php $booth2_allocation = json_decode($gp[0]->booth2_allocation); ?>
                                        <p><strong>Allocation:</strong> <?php echo (!empty( $booth2_allocation->file_name) ) ? '<a target="_blank" href="'.base_url('admin/uploads/booth-allocation/'.$booth2_allocation->file_name).'">'.$booth2_allocation->file_name.'</a>' : '' ;?></p>
                                        * ?>
                                        <p><strong>m2:</strong> <?php echo $b2_m2; ?></p>

                                       <p>To view your <?php echo $b2_type; ?> Booth Allocation click <strong><a target="_blank" href="<?php echo $b2_link; ?>" >here</a></strong></p>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php */ ?>
                        </div>
                    </div>

                    <div class="ad-sec-header">
                        <h3>Exhibiting Artists</h3>
                        <p style="line-height: 20px;">We will use the below artist information in all fair collateral. Please ensure the artist's name is spelt correctly, to edit an artist information click on edit button and confirm the information. We will also require the artist's nationality and date of birth.</p>
                    </div>

                    <div class="add-section no-padding">
                        <table class="data-table alt no-actions">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Surname</th>
                                <th>Nationality</th>
                                <th>Date of Birth</th>
                                <th>Gender</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( isset( $ae ) && !empty( $ae ) ) :
                                foreach ( $ae as $artist )
                                {

                                    $gender     = '';
                                    if($artist){
                                      if($artist->gender){
                                        if($artist->gender == 'm'){ $gender = 'Male'; }else if($artist->gender == 'f'){ $gender = 'Female'; }
                                      }

                                      $countries = config_item('country_list');
                                      $country = $artist->nationality;
                                      if (array_key_exists($country, $countries)) {
                                          $selected_country = $countries[$country];
                                      }




                                    ?>


                                    <tr>
                                        <td><?php echo $artist->first_name; ?></td>
                                        <td><?php echo $artist->last_name; ?></td>
                                        <td><?php echo ( !empty( $selected_country ) ) ? $selected_country : $artist->nationality; ?></td>
                                        <td><?php echo ( !empty( $artist->dob ) ) ? $artist->dob : ''; ?></td>
                                        <td><?php echo $gender; ?></td>
                                        <td class="text-right">
                                            <a href="<?php echo base_url( 'gallery-profile/exhibit/' . $artist->id ); ?>" class="edit"><i class="fas fa-pencil-alt"></i> Edit</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                }
                                if( isset( $aae ) && !empty( $aae ) ) :
                                    foreach ( $aae as $artist )
                                    {
                                        $name = explode( ' ', $artist );
                                        ?>
                                        <tr>
                                            <td><?php echo $name[0]; ?></td>
                                            <td><?php echo ( !empty( $name[1] ) ) ? $name[1] : ''; ?> <?php echo ( !empty( $name[2] ) ) ? $name[2] : ''; ?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                    }
                                endif;
                            else:
                                if( isset( $aae ) && !empty( $aae ) ) :
                                    foreach ( $aae as $artist )
                                    {
                                        $name = explode( ' ', $artist );
                                        ?>
                                        <tr>
                                            <td><?php echo $name[0]; ?></td>
                                            <td><?php echo ( !empty( $name[1] ) ) ? $name[1] : ''; ?> <?php echo ( !empty( $name[2] ) ) ? $name[2] : ''; ?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                    }
                                else:
                                    ?>
                                    <tr>
                                        <td colspan="5">No exhibited artists.</td>
                                    </tr>
                                <?php
                                endif;
                            endif;
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>


            <div class="content-box">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-heading text-center">LIST OF WORKS</h3>
                    </div>
                    <div class="col-md-8 col-md-offset-2">
                        <article class="text-center">
                            <p>Kindly ensure that the List of Works that you will be bringing and exhibiting at the fair is sent to <a href="mailto:genna@artdubai.ae">genna@artdubai.ae</a> by the <strong>28th of January</strong>. Please download the List of Works Template below and complete the necessary sections ensuring all images are submitted with full captions.</p>
                        </article>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <a href="<?php echo upload_dir( 'pdf/AD2020_Lists_Works.docx' ); ?>" download="AD2020_Lists_Works" target="_blank" class="btn">Download Template</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-box">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-heading text-center">PRICE LISTS</h3>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <article class="text-center">
                            <p>The VIP team at Art Dubai is constantly reaching out to collectors, museums groups and collectors in an effort to draw the right audiences to the fair in March and make sure we meet the expectations of both galleries and our VIP, Collectors and Museums representatives in terms of sales.</p>
                            <p>If you would like to share your indicative price ranges or a specific price list please email them directly to <a href="mailto:genna@artdubai.ae" >genna@artdubai.ae</a>. We can then do our best to approach the right collectors for you and match visitors relevant to your gallery. Needless to say that the information will be kept confidential and will only serve to inform our ongoing discussions with collectors and visiting groups.</p>
                            Please download the Price List template below and ensure that you send your Price Lists to <strong><a href="mailto:genna@artdubai.ae">genna@artdubai.ae</a></strong> by the <strong>28th of January</strong>.
                        </article>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <a href="<?php echo upload_dir( 'pdf/AD2020_Prices_Lists.docx' ); ?>" download="AD2020_Prices_Lists" target="_blank" class="btn">Download Template</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="volunteer-request">

            <div class="container">
                <div class="ad-inner-header">
                    <h2>Volunteer Request</h2>
                </div>

                <div class="row row-eq-height row-no-padding">
                    <div class="col-md-6">
                        <article>
                            <?php echo $volunteer->box1; ?>
                        </article>
                    </div>
                    <div class="col-md-6">
                        <figure><img src="assets/images/volunteer-request.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <article class="text-center">
                            <form id="volunteer-request" action="<?php echo base_url( 'submit-volunteer-request' ) ?>" method="post">
                                <?php $userData = $_SESSION['userData']; ?>
                                <input type="hidden" name="userID" value="<?php echo $userData->id; ?>">
                                <input type="submit" class="btn filled submit-volunteer-request" value="Submit Request">
                            </form>
                        </article>
                    </div>
                </div>
            </div>

        </div>

        <div id="volunteer-request-popup" class="white-popup mfp-hide">
            <i class="fa fa-check-circle"></i>
            <h4>Thank You</h4>
            <p>Your request for a volunteer has been received. We will be in touch closer to the fair with further details. </p>
        </div>

        </div>

    </div>

</div>
