
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Communications & Marketing</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="communications-page">

            <div class="container">

                <div class="icons-box">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Please ensure that your gallery name and the gallery's locations show exactly as you would like them to appear in all adverts, marketing and social media content. This also includes the order/sequence that the cities are in.</p>
                            <p>Your gallery name will appear exactly as it was submitted in your application.</p>
                            <p>If you need to change any aspect of your Gallery Name and/or Cities, you will need to edit the below text and re-submit your gallery name & cities. Changes include:</p>
                            <ul>
                                <li>If you need to add anything to your name or change any parts of your name, this includes CAPITAL LETTERING</li>
                                <li>If you need to add accents anywhere in the name</li>
                                <li>If you need to change the sequence/order in how the cities appear</li>
                                <li>If you need to delete any parts of the name that appear</li>
                            </ul>
                            <p>Example - before editing: Abc Gallery, London / Dubai</p>
                            <p>Example - after editing: ABC Galeria, Dubai / London</p>
                            <div id="gallery-communication-preview">
                                <strong>PREVIEW:</strong>  
                                <?php $lc_selected = json_decode( $gallery_profile->cities ); ?>
                                <span class="name"><?php echo ( !empty( $gallery_communication ) ) ? $gallery_communication[0]->name : $gallery_profile->name; ?></span><span class="city">, <?php echo ( !empty( $gallery_communication ) ) ? $gallery_communication[0]->address1 : ''; ?></span>
                                
                            </div>
                            <form id="gallery-communication-form" action="<?php echo base_url( 'submit-gallery-communication' ); ?>" method="post">
                                <fieldset class="col2">
                                    <input type="text" name="name" id="gallery-communication-name" maxlength="100" placeholder="Gallery Name" value="<?php echo ( !empty( $gallery_communication ) ) ? $gallery_communication[0]->name : $gallery_profile->name; ?>" required>
                                    <input type="text" name="address1" id="gallery-communication-city" maxlength="100" placeholder="City" value="<?php echo ( !empty( $gallery_communication ) ) ? $gallery_communication[0]->address1 : ''; ?>" required>
                                </fieldset>
                                <fieldset class="form-check">
                                    <input type="checkbox" class="form-check-input" id="approveGallery" name="approved" value="1" <?php echo  (!empty( $gallery_communication[0]->approved ) && $gallery_communication[0]->approved == 1 ) ? 'checked' : '' ; ?>>
                                    <label class="form-check-label" for="approveGallery">I confirm that this is how I would like my gallery name and location to appear on all adverts, marketing and social media content.</label><br/>
                                    <small class="label label-warning"></small>
                                </fieldset>
                                <fieldset class="btn-wrap">
                                    <?php $userData = $_SESSION['userData']; ?>
                                    <input type="hidden" name="userID" value="<?php echo $userData->id; ?>">
                                    <input type="submit" class="btn filled small" value="Approve">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="icons-box">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="section-heading text-center">Media Package</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p><i class="icon icon18"></i> International Hosted Press Trip of 40+ high profile arts writers from across the world</p>
                        </div>
                        <div class="col-md-4">
                            <p><i class="icon icon19"></i> Outdoor and radio advertising in UAE</p>
                        </div>
                        <div class="col-md-4">
                            <p><i class="icon icon20"></i> Promotion with over 50 key regional and international media partners</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p><i class="icon icon21"></i> 500+ journalists attending the fair</p>
                        </div>
                        <div class="col-md-4">
                            <p><i class="icon icon22"></i> Social media campaigns (Facebook 151,600+, Twitter 86,300+, Instagram 139,000+)</p>
                        </div>
                        <div class="col-md-4">
                            <p><i class="icon icon23"></i> Promotion through Dubai Government channels</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p><i class="icon icon24"></i> Newsletter campaigns to Art Dubai database (over 48,000 contacts)</p>
                        </div>
                        <div class="col-md-4">
                            <p><i class="icon icon25"></i> Year round promotional events regionally and internationally</p>
                        </div>
                        <div class="col-md-4">
                            <p><i class="icon icon26"></i> Promotion through attending exhibitors’ and participating sponsors’ database</p>
                        </div>
                    </div>
                </div>

                <div class="content-box">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="section-heading">Public Relations</h4>
                        </div>
                    </div>
                    <div class="row row-eq-height row-no-padding">
                        <div class="col-md-6">
                            <article>
                                <?php echo $data->box1; ?>
                            </article>
                        </div>
                        <div class="col-md-6">
                            <figure><img src="assets/images/communications.jpg" alt=""></figure>
                        </div>
                    </div>
                </div>

            </div>

            <div class="social-media">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 text-center">
                            <h4 class="section-heading">Social Media</h4>
                            <?php echo $data->box2; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h4 class="section-heading">Website</h4>
                            <?php echo $data->box3; ?>
                            <a href="<?php echo base_url('uploads/pdf/AD2021_Galleries_Fair_Schedule.pdf'); ?>" target="_blank" class="btn simple">Download Logo Here</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

	</div>
