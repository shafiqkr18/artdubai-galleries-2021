<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="ad-authentication-wrap">

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="login-registration-box">
                    <div class="row no-gutters row-eq-height">

                        <div class="col-md-3">
                            &nbsp;
                        </div>

                        <div class="col-md-6">

                            <?php if( !empty( $_GET['action'] ) && ( $_GET['action'] == 'forget-password' ) ) : ?>

                                <div class="forms">

                                    <form class="forget-password-form animate" id="forget-password" action="<?php echo base_url('forget-password'); ?>" method="post">
                                        <h3>Forget Password</h3>
                                        <fieldset>
                                            <label>Enter registered email address</label>
                                            <input type="email" class="text-box" name="email" placeholder="Email*" value="" required>
                                        </fieldset>
                                        <input type="submit" class="submit" name="submit" value="Reset Password">
                                    </form>

                                </div>

                                <div class="after-forms">
                                    <p>Have a password already?</p>
                                    <p><a href="<?php echo base_url(); ?>">Sign in here</a></p>
                                </div>

                            <?php elseif( !empty( $_GET['action'] ) && ( $_GET['action'] == 'reset-password' ) && ( !empty( $_GET['key'] ) ) ) : ?>

                                    <div class="forms">

                                        <?php
                                        $key = base64_decode( $_GET['key'] );
                                        $data = explode( '-', $key );
                                        ?>

                                        <form class="forget-password-form animate" id="reset-password" action="<?php echo base_url('reset-password'); ?>" method="post">
                                            <h3>Reset Password</h3>
                                            <fieldset>
                                                <input type="password" class="text-box" id="password" name="password" placeholder="New Password*" value="" required>
                                                <input type="password" class="text-box" id="password_confirmation" name="password_confirmation" placeholder="Confirm New Password*" value="" required>
                                                <input type="hidden" name="password_reset_key" value="<?php echo $data[0]; ?>">
                                                <input type="hidden" name="email" value="<?php echo $data[1]; ?>">
                                            </fieldset>
                                            <input type="submit" class="submit" name="submit" value="Reset Password">
                                        </form>

                                    </div>

                                    <div class="after-forms">
                                        <p>Have a password already?</p>
                                        <p><a href="<?php echo base_url(); ?>">Sign in here</a></p>
                                    </div>

                            <?php else: ?>

                                <div class="forms">

                                    <form class="login-form animate" id="login" action="<?php echo base_url('login'); ?>" method="post">
                                        <h3>Sign In</h3>
                                        <fieldset>
                                            <input type="email" class="text-box" name="email" placeholder="Email*" value="" required>
                                            <input type="password" class="text-box" name="password" placeholder="Password*" value="" required>
                                        </fieldset>
                                        <input type="submit" class="submit" name="submit" value="Sign In">
                                    </form>

                                </div>

                                <div class="after-forms">
                                    <p>Having problems signing in?</p>
                                    <p><a href="<?php echo base_url( '?action=forget-password' ); ?>">Reset your password</a></p>
                                </div>

                            <?php endif; ?>

                        </div>

                        <div class="col-md-3">
                            &nbsp;
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
