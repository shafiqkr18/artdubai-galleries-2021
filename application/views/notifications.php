<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="ad-page-wrap">

    <div class="ad-inner-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Your Notifications</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="notification-list">
	                <?php
	                if( !empty( $notifications ) )
	                {
                        $notifications = array_reverse( $notifications );
		                foreach ( $notifications as $notification ) {
			                echo '<div class="item ' . ( ( $notification->read == 1 ) ? 'read' : '' ) . '">
                                <i class="fa fa-bell"></i>
                                <p>' . $notification->title . '</p>
                                <p><small> ' . $notification->message . ' </small></p>
                                <p><span>' . timespan( $notification->date, time(), 1 ) . ' ago</span></p>
                                <a class="update_notification" data-id=" ' . $notification->id . ' " data-user=" ' . $notification->receiver_id . ' " href="#">Mark as read</a>
                            </div>';
		                }
	                }
	                ?>
                </div>
            </div>
        </div>
    </div>

</div>
