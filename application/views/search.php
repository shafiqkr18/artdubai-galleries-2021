
	<div class="ad-page-wrap">

		<div class="ad-inner-header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>Search Results</h2>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<form action="<?php echo base_url( 'dashboard/search' ); ?>" method="post" class="row">
						<div class="col-md-10">
							<figure>
								<input type="text" name="s" class="input-flat" placeholder="Enter keyword to search" value="<?php echo $keyword; ?>" required>
							</figure>
						</div>
						<div class="col-md-2">
							<figure class="text-right">
								<input type="submit" value="Search" style="height: 44px;" class="btn small filled">
							</figure>
						</div>
					</form>

					<div class="search-page-wrap">

						<div class="row">
							<?php
							if( !empty( $search ) )
							{
								foreach ( $search as $item )
								{
									$content = json_decode( $item->content );
									?>
									<div class="col-md-6">
										<article class="post-item">
											<h5><a href="#"><?php echo ucfirst( $item->name ); ?></a></h5>
											<p><?php echo html_to_text( $item->content ) ?></p>
											<a href="<?php echo base_url( $item->name ); ?>" class="btn">Read more</a>
										</article>
									</div>
									<?php
								}
							}
							else
							{
								?>
								<div class="col-md-12">
									<article class="post-item" style="padding: 0;border: 0;margin: 0;">
										<p>Nothing found! Try searching again with different keywords.</p>
									</article>
								</div>
								<?php
							}
							?>
						</div>

<!--						<div class="load-more">-->
<!--							<a href="#" class="btn">Load More</a>-->
<!--						</div>-->

					</div>
				</div>
			</div>
		</div>

	</div>