<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->library('rat');
	}

	public function index()
	{

		if( isset( $_SESSION['userData'] ) )
		{
			redirect('dashboard');
		}


		$this->load->view('templates/header' );
		$this->load->view('authentication');
		$this->load->view('templates/footer');

	}

	public function login()
	{
		$email      = $this->input->post('email');
		$password   = $this->input->post('password');

		$user       = $this->user_model->verify_user($email,$password);

		if( $user == false )
		{
			echo json_encode( array( 'error' => 'Email or Password is not matching with our record.' ) );
		}
		else
		{
			$_SESSION['userData'] = $user[0];
			$this->rat->add_log( $user[0]->fullname . ' logged in', 501, $user[0]->id);
			echo json_encode( array( 'success' => 'Logged in successfully.' ) );
		}

	}

	public function forget_password()
	{

		$config = array(
			'mailtype'      => 'html',
			'smtp_host'     => 'smtp-relay.gmail.com',
			'smtp_user'     => 'umair@wewantzoom.com',
			'smtp_pass'     => 'Umair@Zoom5858!',
			'smtp_port'     => '587',
			'smtp_crypto'   => 'ssl'
		);

		$this->load->library('email');
		$this->load->helper('url');
		$this->load->helper('string');

		$email = $this->input->post('email');
		$response = $this->user_model->verify_email($email);

		if( $response == true )
		{
 
			$key = random_string('alnum', 16); 
			$key_added = $this->user_model->update_key($email,$key);
			if( $key_added == true )
			{

				$html = 'Click on the link to reset your password: '.anchor( site_url( '?action=reset-password&key=' . base64_encode( $key.'-'.$email ) ), 'Reset Password', array( 'target' => '_blank' ) );

				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from('no-reply@artdubai.ae', 'Art Dubai');
				$this->email->to($email);
                $this->email->cc('umair@wewantzoom.com');
                $this->email->cc('galleriesartdubai@gmail.com');
				$this->email->subject('Password Reset Request');
				$this->email->message( $html );

				$mail_sent = $this->email->send();

				if( $mail_sent == 1 )
				{
					echo json_encode( array( 'success' => 'An email has been sent with a link to reset your password.' ) );
				}
				else
				{
					echo json_encode( array( 'error' => 'Password reset email sent failed.' ) );
				}

			}
			else
			{
				echo json_encode( array( 'error' => 'Key is not updated into database.' ) );
			}

		}
		else
		{
			echo json_encode( array( 'error' => 'This email address is not registered.' ) );
		}

	}

	public function reset_password(){

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$key = $this->input->post('password_reset_key');

		$response = $this->user_model->verify_key($email,$key);

		if( isset( $key ) && !empty( $key ) )
        {
            if( $response == true ) {
                $res2 = $this->user_model->reset_password($email,$password);
                if( $res2 !== false ) {
                    echo json_encode( array( 'success' => 'Your password is changed successfully.' ) );
                }
                else
                {
                    echo json_encode( array( 'error' => 'Sorry! Password cannot be changed.' ) );
                }
            } else {
                echo json_encode( array( 'error' => 'Sorry, Password cannot be changed.' ) );
            }
        }
        else
        {
            show_error( 'Not Allowed.' );
        }

	}

	public function logout()
	{
	    if( isset( $_SESSION['userData'] ) ) {
            $user = $_SESSION['userData'];
            $this->rat->add_log( $user->fullname . ' logged out', 501, $user->id);
            session_destroy();
            redirect('/');
        } else {
            session_destroy();
            redirect('/');
        }
	}

}