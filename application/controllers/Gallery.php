<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('gallery_model');
		$this->load->model('dashboard_model');
	}

	public function index()
	{

		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('user');
		}

		$user = $_SESSION['userData'];

		$ae = $this->gallery_model->get_artists_exhibiting( $user->id );

		$data['gp'] = $this->gallery_model->get_gallery_profile( $user->id );
		$data['sp'] = $this->gallery_model->get_social_platforms( $user->id );
		$data['lc'] = $this->gallery_model->get_locations( $user->id );
		$data['gt'] = $this->gallery_model->get_gallery_team( $user->id );
		$data['ar'] = $this->gallery_model->get_artists( $user->id );
		$data['pr'] = $this->gallery_model->get_participation( $user->id );
        
        if($ae){
            $artists = json_decode( $ae->artists );
        }
		

		if( !empty( $ae->additional_artists ) ) {
            $data['aae'] = explode( ',', $ae->additional_artists );
        }
        
        if(isset($artists)){
        foreach ( $artists as $artist ) {
            $data['ae'][] = $this->gallery_model->get_an_artist( (int) $artist );
        }
        }

		$this->load->view('templates/header' );
		$this->load->view('gallery', $data);
		$this->load->view('templates/footer');

	}

	public function edit()
	{

		$user = $_SESSION['userData'];

		if( !isset( $user ) )
		{
			redirect('user');
		}

		$all_modules = array( 'info', 'location', 'staff', 'artist', 'exhibit', 'participation' );

		$section = $this->uri->segment(2, 0);
		$id = $this->uri->segment(3, 0);

		if( in_array( $section, $all_modules ) )
		{

			$data['section'] = $section;
			$data['gp'] = $this->gallery_model->get_gallery_profile( $user->id, $id );
			$data['sp'] = $this->gallery_model->get_social_platforms( $user->id, $id );
			$data['lc'] = $this->gallery_model->get_locations( $user->id, $id );
			$data['gt'] = $this->gallery_model->get_gallery_team( $user->id, $id );
			$data['ar'] = $this->gallery_model->get_artists( $user->id, $id );
			$data['pr'] = $this->gallery_model->get_participation( $user->id, $id );

			$this->load->view('templates/header' );
			$this->load->view('gallery_b', $data);
			$this->load->view('templates/footer');

		}
		else
		{

			show_error( 'Something went wrong!' );

		}

	}

    public function add()
    {

        $user = $_SESSION['userData'];

        if( !isset( $user ) )
        {
            redirect('user');
        }

        $all_modules = array( 'location', 'staff' );

        $section = $this->uri->segment(3, 0);

        if( in_array( $section, $all_modules ) )
        {

            $data['section'] = $section;

            $this->load->view('templates/header' );
            $this->load->view('gallery_c', $data);
            $this->load->view('templates/footer');

        }
        else
        {

            show_error( 'Something went wrong!' );

        }

    }

    public function create()
    {

        $posted_data = $this->input->post();

        if( !empty( $posted_data ) )
        {

            $user = $_SESSION['userData'];
            $section = $posted_data['section'];
            $posted_data['user_id'] = $user->id;

            if( $section == 'location' ) {
                $res = $this->gallery_model->add_locations( $user->id, $posted_data );
            } elseif( $section == 'staff' ) {
                $res = $this->gallery_model->add_gallery_team( $user->id, $posted_data );
            } else {
                show_error( 'Something went wrong!' );
            }

            // Ajax response message 
			if($res == true){
				$response 	= json_encode( array( 'success' => 'Submitted Successfully.' ) );
			}else{
				show_error( 'Something went wrong!' );
			}

			echo $response;
        }
        else
        {

            show_error( 'Something went wrong!' );

        }

    }

	public function update()
	{

		$to         = 'galleriesartdubai@gmail.com'; // 'kabir@wewantzoom.com'; // 
        $subject    = 'Gallery Updated';
        $cc         = 'umair@wewantzoom.com'; // 'umair@wewantzoom.com'; // 
        $attachment = '';

		$posted_data = $this->input->post();

		if( !empty( $posted_data ) )
		{

            $user = $_SESSION['userData'];

			$section = $posted_data['section'];

			if( $section == 'info' ) {
				$res = $this->gallery_model->update_gallery_profile( $posted_data['id'], $posted_data );

				// Email
				$message    = $user->galleryname . ' have updated their profile info.';
				$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

			} elseif( $section == 'popup' ) {
			    //$posted_data['cities'] = json_encode( $posted_data['cities'] );
			    
			    $posted_data['address1']= $posted_data['cities'];

                //This will also update information in Communication/Marketing page
                $already_added = $this->dashboard_model->get_gallery_communication( $posted_data['userID'] );
                if( $already_added == true ) {
                	unset($posted_data['cities']);
                	$this->dashboard_model->update_gallery_communication( $posted_data['userID'], $posted_data );
                } else {
                	unset($posted_data['cities']);
	                $this->dashboard_model->set_gallery_communication( $posted_data['userID'], $posted_data );
	            }
	            // To update gallery information
	            $posted_data['cities'] 	=  json_encode(explode(' / ', $posted_data['address1']));

	            unset($posted_data['address1']);
	            unset($posted_data['userID']);
	            $res = $this->gallery_model->update_gallery_profile( $posted_data['id'], $posted_data );

				// Email
				$message    = $user->galleryname . ' has updated and confirmed their gallery name and cities info.' ;
				$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

            } elseif( $section == 'social' ) {
				$res = $this->gallery_model->update_social_platforms( $posted_data['id'], $posted_data );

				// Email
				$message    = $user->galleryname . ' has social platforms.' ;
				$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

			} elseif( $section == 'location' ) {
				$res = $this->gallery_model->update_locations( $posted_data['id'], $posted_data );

				// Email
				$message    = $user->galleryname . ' have updated a location detail.' ;
				$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

			} elseif( $section == 'staff' ) {
				$res = $this->gallery_model->update_gallery_team( $posted_data['id'], $posted_data );

				// Email
				$message    = $user->galleryname . ' have updated a staff info.' ;
				$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

			} elseif( $section == 'artist' || $section == 'exhibit' ) {
				$res = $this->gallery_model->update_artists( $posted_data['id'], $posted_data );
				$conditional_text = (!empty( $section ) && $section == 'artist' ) ? 'artists' : 'exhibiting artists';
				
				if(!empty($posted_data['statement']) && $posted_data['statement'] != '')
				{
					$res = $this->gallery_model->update_gallery_statement( $user->id, $posted_data['statement'] );
				}

				// Email
				$message    = $user->galleryname . ' have updated their represented '. $conditional_text .' info.'  ;
				$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

			} elseif( $section == 'participation' ) {
				$res = $this->gallery_model->update_participation( $posted_data['id'], $posted_data );

				// Email
				$message    = $user->galleryname . ' have updated their participation detail.'  ;
				$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

			} else {
				$res = false;
			}

			// Ajax response message 
			if($res == true){
				$response 	= json_encode( array( 'success' => 'Submitted Successfully.' ) );
			}else{
				show_error( 'Something went wrong!' );
			}

			echo $response;

		}
		else
		{

			show_error( 'Something went wrong!' );

		}

	}

	public function delete()
	{

		$posted_data = $this->input->post();

		if( !empty( $posted_data ) )
		{ 
			$section = $posted_data['section'];
			$id = $posted_data['id'];

			$res = $this->gallery_model->delete( $id, $section );

			if( $res == true ){
				echo json_encode( array( 'success' => 'Selected information has been deleted.' ) );
			}
			else{
				echo json_encode( array( 'error' => 'Sorry! Something went wrong.' ) );
			}
		}
		else{
			show_error( 'Something went wrong!' );
		}

	}

}
