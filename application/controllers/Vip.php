<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VIP extends CI_Controller
{

    private $userData;

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('vip_model');
        $this->load->library('salesforce');
        if( isset( $_SESSION['userData'] ) )
        {
            $this->userData = $_SESSION['userData'];
        }
	}

	public function index()
	{
		
		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('user');
		}

        $this->salesforce->verify_token();

		$user = $_SESSION['userData'];

		$entry_id = $this->uri->segment(2, 0);

		if( !empty( $entry_id ) )
		{
			$data['entry'] = $this->vip_model->read_id( $user->id, $entry_id );
			$view = 'vip_designations_b';
		}
		else
		{
      $data['user'] = $user;
			$data['patrons'] = $this->vip_model->read( $user->id, 'patrons' );
			$data['collectors'] = $this->vip_model->read( $user->id, 'collectors' );
      $data['guest_of_gallery_invites'] = $this->vip_model->read( $user->id, 'guest_of_gallery_invites' );
			$view = 'vip_designations';
		}

		$this->load->view('templates/header' );
		$this->load->view($view, $data);
		$this->load->view('templates/footer');

	}

	public function create()
	{

		$posted_data = $this->input->post();
		$userFORAPI =  $_SESSION['userData'];
		
		
		if( !empty( $posted_data ))
		{

			$mobileNoForAPI = $posted_data['mobile_code'] . $posted_data['mobile'];
			

			//1 check FN+Email
			$inContactFNE = $this->salesforce->check_contact_FNE( $posted_data['first_name'],$posted_data['email'] );
			$contactJsonFNE = json_decode( $inContactFNE );
			
			if($contactJsonFNE->totalSize == 1)
			{
				$contactData = $contactJsonFNE->records[0];
			}else{
				//2 check LN+Email
				$inContactLNE = $this->salesforce->check_contact_LNE( $posted_data['sur_name'],$posted_data['email'] );
				$contactJsonLNE = json_decode( $inContactLNE );
				if($contactJsonLNE->totalSize == 1)
				{
					$contactData = $contactJsonLNE->records[0];
				}else{
					//3 check FN+Mobile 
					$inContactFNM = $this->salesforce->check_contact_FNM( $posted_data['first_name'],$mobileNoForAPI );
					$contactJsonFNM = json_decode( $inContactFNM );
					if($contactJsonFNM->totalSize == 1)
					{
						$contactData = $contactJsonFNM->records[0];
					}else{
						//4 check LN+Mobile
						$inContactLNM = $this->salesforce->check_contact_LNM( $posted_data['sur_name'],$mobileNoForAPI );
						$contactJsonLNM = json_decode( $inContactLNM );
						if($contactJsonLNM->totalSize == 1)
						{
							$contactData = $contactJsonLNM->records[0];
						}else{
							//5 check email or personal or business
							$inContactA = $this->salesforce->check_contact_a( $posted_data['email'] );
							$contactJsonA = json_decode( $inContactA );
							if($contactJsonA->totalSize == 1)
							{
								$contactData = $contactJsonA->records[0];
							}else{
								$contactData = '';
							}
						}
					}
				}
			}

			//echo "here";
			//print_r($contactData);
			//exit;
			


			//End for today

			if( isset($contactData) && $contactData != '' ) 
			{

				
				// echo "contact section A";
				// print_r($contactData);
				// echo "<br>contact section B";
				// print_r($contactJsonB);
				// exit;

                // CONTACT - YES
				

				//APPLY CONDITIONS AS PER CHART PROVIDED

				//CONDITION 1, IF PC SELECTED
				if( $posted_data['invite_type'] == 'patrons')
				{
					
					if( $contactData->Art_Dubai_Invitation_Type__c == 'Art Salon' || $contactData->Art_Dubai_Invitation_Type__c == 'Super PC - AD' || $contactData->Art_Dubai_Invitation_Type__c == 'Patrons Circle - AD' || $contactData->Art_Dubai_Invitation_Type__c == 'Invited Curator' || $contactData->Art_Dubai_Invitation_Type__c == "International Collector's Programme - ICP" )
					{
						echo json_encode( array( 'info' => 'Your Guest has already been invited by Art Dubai, you may select another guest.' ) );


					}else{
						
						//TODO
						//1- UPDATE CONTACT Contact_Source_Gallery2__c & Art_Dubai_Invitation_Type__c
						 $invite_type = 'Patrons Circle - AD';
						$res = $this->salesforce->update_contact_fields( $contactData->Id,$posted_data['gallery_name'] , $invite_type );
						
						if($contactData->Updated_Details__c == 'Confirmed AD 2020')
						{
							//TODO

						}elseif($contactData->Updated_Details__c == 'Updated Details AD 2020')
						{
							//TODO
						}else{
							//NOW SAVE SIT RECORD - TODO
						$res_SIT = $this->salesforce->generate_SIT($contactData , $posted_data);
						}

						//update local db
						$this->vip_model->update_vip($userFORAPI->id, $posted_data);
						

						if($res == '') $res = "Record Created!";
						echo json_encode( array( 'success' => 'Updated Successfully!.') );

						
					}



				}elseif( $posted_data['invite_type'] == 'collectors') //CONDITION 2
				{

					if( $contactData->Art_Dubai_Invitation_Type__c == 'Collectors Circle - AD')
					{

						echo json_encode( array( 'info' => 'Your Guest has already been invited by Art Dubai, you may select another guest.' ) );

					}elseif($contactData->Art_Dubai_Invitation_Type__c == 'Art Salon' || $contactData->Art_Dubai_Invitation_Type__c == 'Super PC - AD' || $contactData->Art_Dubai_Invitation_Type__c == 'Patrons Circle - AD' || $contactData->Art_Dubai_Invitation_Type__c == 'Invited Curator' || $contactData->Art_Dubai_Invitation_Type__c == "International Collector's Programme – ICP" )
					{

						//TODO
						//1- UPDATE CONTACT Contact_Source_Gallery2__c 
						$res = $this->salesforce->update_contact_field( $contactData->Id,$posted_data['gallery_name'] );
						
						if($contactData->Updated_Details__c == 'Confirmed AD 2020')
						{
							//TODO

						}elseif($contactData->Updated_Details__c == 'Updated Details AD 2020')
						{
							//TODO
						}else{
							//NOW SAVE SIT RECORD - TODO
						$res_SIT = $this->salesforce->generate_SIT($contactData , $posted_data);
						}

						$this->vip_model->update_vip($userFORAPI->id, $posted_data);
						if($res == '') $res = "Record Created!";
						echo json_encode( array( 'success' => 'Updated Successfully!.') );

					}else{

						//TODO
						//1- UPDATE CONTACT Contact_Source_Gallery2__c & Art_Dubai_Invitation_Type__c
						 $invite_type = 'Collectors Circle - AD';
						$res = $this->salesforce->update_contact_fields($contactData->Id,$posted_data['gallery_name'] , $invite_type );
						
						if($contactData->Updated_Details__c == 'Confirmed AD 2020')
						{
							//TODO

						}elseif($contactData->Updated_Details__c == 'Updated Details AD 2020')
						{
							//TODO
						}else{
							//NOW SAVE SIT RECORD - TODO
						$res_SIT = $this->salesforce->generate_SIT($contactData , $posted_data);
						}

						$this->vip_model->update_vip($userFORAPI->id, $posted_data);
						if($res == '') $res = "Record Created!";
						echo json_encode( array( 'success' => 'Updated Successfully!.') );
						

					}
				}elseif($posted_data['invite_type'] == 'guest_of_gallery_invites')
				{

					if( $contactData->Art_Dubai_Invitation_Type__c == 'VIP 1 - AD')
					{

						echo json_encode( array( 'info' => 'Your Guest has already been invited by Art Dubai, you may select another guest.' ) );

					}elseif($contactData->Art_Dubai_Invitation_Type__c == 'Art Salon' || $contactData->Art_Dubai_Invitation_Type__c == 'Super PC - AD' || $contactData->Art_Dubai_Invitation_Type__c == 'Patrons Circle - AD' || $contactData->Art_Dubai_Invitation_Type__c == 'Invited Curator' || $contactData->Art_Dubai_Invitation_Type__c == "International Collector's Programme – ICP" || $contactData->Art_Dubai_Invitation_Type__c == 'Collectors Circle - AD' )
					{

						//TODO
						//1- UPDATE CONTACT Contact_Source_Gallery2__c 
						$res = $this->salesforce->update_contact_field($contactData->Id,$posted_data['gallery_name'] );
						

						if($contactData->Updated_Details__c == 'Confirmed AD 2020')
						{
							//TODO

						}elseif($contactData->Updated_Details__c == 'Updated Details AD 2020')
						{
							//TODO
						}else{
							//NOW SAVE SIT RECORD - TODO
						$res_SIT = $this->salesforce->generate_SIT($contactData , $posted_data);
						}

						$this->vip_model->update_vip($userFORAPI->id, $posted_data);
						if($res == '') $res = "Record Created!";
						echo json_encode( array( 'success' => 'Updated Successfully!..') );

						

					}else{

						//TODO
						//1- UPDATE CONTACT Contact_Source_Gallery2__c & Art_Dubai_Invitation_Type__c
						$invite_type = 'VIP 1 - AD';
						$res = $this->salesforce->update_contact_fields( $contactData->Id,$posted_data['gallery_name'] , $invite_type );
						
						if($contactData->Updated_Details__c == 'Confirmed AD 2020')
						{
							//TODO

						}elseif($contactData->Updated_Details__c == 'Updated Details AD 2020')
						{
							//TODO
						}else{
							//NOW SAVE SIT RECORD - TODO
						$res_SIT = $this->salesforce->generate_SIT($contactData , $posted_data);
						}

						$this->vip_model->update_vip($userFORAPI->id, $posted_data);
						if($res == '') $res = "Record Created!";
						echo json_encode( array( 'success' => 'Updated Successfully!...') );

						

					}

				}else{
					//ANY OTHER TYPE IN FUTURE
				}
                

                

            }
            else
            {

				// CONTACT - NOT FOUND, NOW CHECK LEAD
				
				
				$inLeadA = $this->salesforce->check_lead( $posted_data['email'] );
			
				$leadJsonA = json_decode( $inLeadA );
				if($leadJsonA->totalSize == 1)
				{
					$leadData = $leadJsonA->records[0];
				}else{
					$inLeadAN = $this->salesforce->check_lead_by_name( $posted_data['first_name'], $posted_data['sur_name'] );
					$leadJsonAN = json_decode( $inLeadAN );

					if($leadJsonAN->totalSize == 1)
					{
						$leadData = $leadJsonAN->records[0];
					}else{
						$leadData = '';
					}
				}


				//end for today

				 //echo "I am in lead section";
				 //print_r($leadData);exit;

				if(isset($leadData) && $leadData != '')
				{
					//$leadData = $leadJsonA->records[0];

					//APPLY CONDITIONS AS PER CHART PROVIDED

					//CONDITION 1, IF PC SELECTED
					if( $posted_data['invite_type'] == 'patrons')
					{


						if( $leadData->Art_Dubai_Invitation_Types__c == 'Art Salon' || $leadData->Art_Dubai_Invitation_Types__c == 'Super PC - AD' || $leadData->Art_Dubai_Invitation_Types__c == 'Patrons Circle - AD' || $leadData->Art_Dubai_Invitation_Types__c == 'Invited Curator' || $leadData->Art_Dubai_Invitation_Types__c == "International Collector's Programme – ICP" )
						{

							echo json_encode( array( 'info' => 'Your Guest has already been invited by Art Dubai, you may select another guest..' ) );


						}else{

							//TODO
							$invite_type = 'Patrons Circle - AD';
							$res = $this->salesforce->update_lead_fields( $leadData->Id,$posted_data['gallery_name'] , $invite_type );
							$this->vip_model->update_vip($userFORAPI->id, $posted_data);
							echo json_encode( array( 'success' => 'Updated Successfully!.' ) );
						}



					}elseif( $posted_data['invite_type'] == 'collectors') //CONDITION 2
					{

						if( $leadData->Art_Dubai_Invitation_Types__c == 'Collectors Circle - AD')
						{

							echo json_encode( array( 'info' => 'Your Guest has already been invited by Art Dubai, you may select another guest..' ) );

						}elseif($leadData->Art_Dubai_Invitation_Types__c == 'Art Salon' || $leadData->Art_Dubai_Invitation_Types__c == 'Super PC - AD' || $leadData->Art_Dubai_Invitation_Types__c == 'Patrons Circle - AD' || $leadData->Art_Dubai_Invitation_Types__c == 'Invited Curator' || $leadData->Art_Dubai_Invitation_Types__c == "International Collector's Programme – ICP" )
						{

							//TODO
							$res = $this->salesforce->update_lead_field( $leadData->Id,$posted_data['gallery_name'] );
							$this->vip_model->update_vip($userFORAPI->id, $posted_data);
							echo json_encode( array( 'success' => 'Updated Successfully!.' ) );

						}else{

							//TODO
							$invite_type = 'Collectors Circle - AD';
							$res = $this->salesforce->update_lead_fields( $leadData->Id,$posted_data['gallery_name'] , $invite_type );
							$this->vip_model->update_vip($userFORAPI->id, $posted_data);
							echo json_encode( array( 'success' => 'Updated Successfully!.' ) );


						}
					}elseif($posted_data['invite_type'] == 'guest_of_gallery_invites')
					{

						if( $leadData->Art_Dubai_Invitation_Types__c == 'VIP 1 - AD')
						{

							echo json_encode( array( 'info' => 'Your Guest has already been invited by Art Dubai, you may select another guest..' ) );

						}elseif($leadData->Art_Dubai_Invitation_Types__c == 'Art Salon' || $leadData->Art_Dubai_Invitation_Types__c == 'Super PC - AD' || $leadData->Art_Dubai_Invitation_Types__c == 'Patrons Circle - AD' || $leadData->Art_Dubai_Invitation_Types__c == 'Invited Curator' || $leadData->Art_Dubai_Invitation_Types__c == "International Collector's Programme – ICP" || $leadData->Art_Dubai_Invitation_Types__c == 'Collectors Circle - AD' )
						{

							//TODO
							$res = $this->salesforce->update_lead_field( $leadData->Id,$posted_data['gallery_name'] );
							$this->vip_model->update_vip($userFORAPI->id, $posted_data);
							echo json_encode( array( 'success' => 'Updated Successfully!.' ) );

						}else{

							//TODO
							$invite_type = 'VIP 1 - AD';
							$res = $this->salesforce->update_lead_fields( $leadData->Id, $posted_data['gallery_name'] , $invite_type );
							$this->vip_model->update_vip($userFORAPI->id, $posted_data);
							echo json_encode( array( 'success' => 'Updated Successfully!.' ) );


						}

					}else{
						//ANY OTHER TYPE IN FUTURE
						echo json_encode( array( 'errorMsg' => 'Any Other details!' ) );
					}

				}else{

					//PROCESS LEAD
					$this->salesforce->generate_lead(null , $posted_data );
					$this->process_lead( $posted_data );
					//echo json_encode( array( 'success' => 'New Lead Created' ) );

				}

			   

            }

		}else
		{

			show_error( 'Not allowed!' );

		}

	}

	public function update()
	{

		$data = $this->input->post();

		if( isset( $data ) && !empty( $data ) )
		{
			$user = $_SESSION['userData'];
			$this->vip_model->update( $user->id, $data );
			echo json_encode( array( 'success' => 'Submitted Successfully.' ) );
		}
		else
		{
			show_error( 'Sorry! Something went wrong.', 405 );
		}

	}

	public function delete()
	{

		$id = $this->input->post('id');

		if( isset( $id ) && !empty( $id ) )
		{
			$user = $_SESSION['userData'];
			$this->vip_model->delete( $user->id, $id );
			echo json_encode( array( 'success' => 'Selected information has been deleted.' ) );
		}
		else
		{
			show_error( 'Sorry! Something went wrong.', 405 );
		}

	}

	public function process_lead( $posted_data )
    {

        $response = $this->vip_model->create( $this->userData->id, $posted_data );

        if( $response === true )
        {

            //$this->send_notification_email( $this->userData );

            echo json_encode( array( "success" => "Submitted Successfully." ) );

        } else {

            echo json_encode( array( 'error' => 'Sorry! Something went wrong.', 'errorMsg' => $response ) );

        }

    }

	public function send_notification_email( $userData )
    {

        $config = array(
            'mailtype'      => 'html',
            'smtp_host'     => 'smtp-relay.gmail.com',
            'smtp_user'     => 'umair@wewantzoom.com',
            'smtp_pass'     => 'Umair@Zoom5858!',
            'smtp_port'     => '587',
            'smtp_crypto'   => 'ssl'
        );

        $this->load->library('email');

        $html = '<p>A new VIP Invitation Requested is submitted.</p>';
        $html .= '<table><thead><tr><th>Full Name</th><th>Gallery Name</th><th>Email</th></tr></thead><tbody><tr><td>' . $userData->fullname . '</td><td>' . $userData->galleryname . '</td><td>' . $userData->email . '</td></tr></tbody></table>';

        $this->email->initialize($config);
        $this->email->from('no-reply@wewantzoom.com', 'Art Dubai');
        $this->email->to('isidora@artdubai.ae ');
        $this->email->cc('galleriesartdubai@gmail.com,umair@wewantzoom.com');
        $this->email->subject('VIP Invitation Request - Art Dubai');
        $this->email->message( $html );

        $response = $this->email->send();

        return $response;

    }

}
