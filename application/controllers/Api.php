<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{

    private $apiKey = "125e8fb3-e22d-41b5-9c82-d325c7213efe";

    function __construct()
    {
        header('Access-Control-Allow-Headers: "Content-Type, api-key, Authorization"');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Origin: *");
        parent::__construct();
        $this->load->model('api_model');
        $this->load->library('pagination');
    }

    public function index()
    {

        echo 'Invalid API Gateway.';

    }
    
    public function catalogue_years(){
        
        $years = $this->api_model->get_catalogue_years();
        
        foreach($years as $year){
            $result[] = $year->exhibit_year;
        }
        if ($result) {
                echo json_encode($result);
            } else {
                return false;
            }
       
    }

    public function list()
    {
        $args = $this->input->get();
        $apiKey = $this->input->get_request_header('api-key', TRUE);
        $result = $this->check_api_key($apiKey);
        //if ($result === true) {

            $exhibit_year = $this->input->get('year');
            
            if ($exhibit_year) {
                $year = $exhibit_year;
            } else {
                $year = date("Y");
            }
    
            $res = $this->api_model->get_galleries_list($year);
            //print("<pre>".print_r(json_encode($res),true)."</pre>");
            //http://localhost/artdubai-galleries/api/list
    
            if ($res) {
                echo json_encode($res);
            } else {
                return false;
            }
        
        // } else {
        //     echo 'Authentication Failed!';
        // }
    }

    public function get_all_images()
    {

        $args = $this->input->get();
        $apiKey = $this->input->get_request_header('api-key', TRUE);
        $result = $this->check_api_key($apiKey);
        if ($result === true) {
            $config = array();
            $config["base_url"] = base_url() . "api/get-all-images";
            $config["total_rows"] = $this->api_model->record_count();
            $config["per_page"] = 12;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $documents = $this->api_model->get_documents($config["per_page"], $page, $args);
            $total = $this->api_model->get_documents_total($args);
           
            $response = array();
            $count = 0;
            if (count($documents) > 0) {
                foreach ($documents as $document) {
                    $id = (isset($document->file_id)) ? $document->file_id : $document->id;
                    $response['total'] = count($total);
                    $response['data'][$count]['id'] = $id;
                    $response['data'][$count]['artist'] = $document->artist;
                    $response['data'][$count]['year'] = $document->year;
                    $response['data'][$count]['title'] = $document->title;
                    $response['data'][$count]['medium'] = json_encode($this->api_model->get_file_mediums($id), true);
                    $gallery = $this->api_model->get_gallery_name($document->user_id);
                    $response['data'][$count]['gallery'] = $gallery[0]->name;
                    $file = json_decode($document->files);
                    $response['data'][$count]['file'] = $this->check_file_name($gallery[0]->name, $file->file_name);
                    $response['data'][$count]['file_name'] = $file->file_name;
                    $count++;
                }
            }
            echo json_encode($response, true);
        } else {
            echo 'Authentication Failed!';
        }

    }

    public function get_gallery_images()
    {

        $apiKey = $this->input->get_request_header('api-key', TRUE);
        $result = $this->check_api_key($apiKey);
        if ($result === true) {
            $user_id = $this->input->get('id');
            $limit = $this->input->get('limit');
            $sortby = $this->input->get('sort');
            $documents = $this->api_model->get_gallery_documents($user_id, $limit, $sortby);
            //$documents = $this->api_model->get_gallery_documents($user_id, $limit);
            $total = $this->api_model->get_gallery_documents_total($user_id);
            $exhibition_statement = $this->api_model->get_exhibition_statement($user_id);
            $response = array();
            $count = 0;
            foreach ($documents as $document) {
                $response['exhibition_statement'] = $exhibition_statement;
                $response['total'] = count($total);
                $response['data'][$count]['id'] = $document->id;
                $response['data'][$count]['artist'] = $document->artist;
                $response['data'][$count]['year'] = $document->year;
                $response['data'][$count]['title'] = $document->title;
                $response['data'][$count]['is_reserved'] = $document->is_reserved;
                $response['data'][$count]['price'] = $document->price;
                $response['data'][$count]['statement'] = $document->statement;
                $response['data'][$count]['exhibit_year'] = $document->exhibit_year;  
                $gallery = $this->api_model->get_gallery_name($document->user_id);
                $response['data'][$count]['gallery'] = $gallery[0]->name;
                $response['data'][$count]['gallery_booth'] = $gallery[0]->booth1;
                $response['data'][$count]['gallery_booth2'] = $gallery[0]->booth2;
                $file = json_decode($document->files);
                $response['data'][$count]['file'] = $file->file_name;
                $count++;
            }
            echo json_encode($response, true);
        } else {
            echo 'Authentication Failed!';
        }

    }

    public function get_images_by_gallery_type()
    {

        $apiKey = $this->input->get_request_header('api-key', TRUE);
        $result = $this->check_api_key($apiKey);
        //if( $result === true ) {
        $user_id = $this->input->get('id');
        $type = (!empty($this->input->get('type')) && $this->input->get('type') != 0) ? $this->input->get('type') : 0;

        //$gallery = $this->api_model->get_gallery_name( $user_id);
        
        $exhibited_years = $this->api_model->get_gallery_exhibit_years($user_id);
        
        // This will get yearly basis catalogue's
        foreach ($exhibited_years as $year){
            $year = $year->exhibit_year;
            
            $documents = $this->api_model->get_user_documents_by_gallery_type($user_id, $type, $year);
            
            $libraries = array();
            if ($documents) {
                foreach ($documents as $document) {
    
    
                    $libraries[] = $document->id;
    
                }
                echo json_encode($libraries, true);
            }
            
        }
       
        // $response = array();
        // $count = 0;
        

        
        // } else {
        //   echo 'Authentication Failed!';
        // }

    }

    public function get_image()
    {
        $this->load->helper('url');
        $siteurl = site_url();

        $apiKey = $this->input->get_request_header('api-key', TRUE);
        $result = $this->check_api_key($apiKey);
        if ($result === true) {
            $document_id = $this->input->get('id');
            $files = $this->api_model->get_document_by_id($document_id);

            $response = array();
            foreach ($files as $document) {
                $medium = $this->api_model->get_file_medium($document->medium);

                $response['id'] = $document->id;
                $response['user_id'] = $document->user_id;
                $response['artist'] = $document->artist;
                $response['year'] = $document->year;
                $response['title'] = $document->title;
                $response['is_reserved'] = $document->is_reserved;
                $response['price'] = $document->price;
                $response['statement'] = $document->statement;
                //$response['medium'] = (!empty( $medium ) ) ? $medium->name : '' ;
                $response['medium'] = (!empty($document->medium)) ? $document->medium : '';
                $response['courtesy'] = $document->courtesy;
                $response['inquiry_email'] = $document->inquiry_email;
                $response['dimensions'] = $document->dimensions;
                $response['gallery_type'] = $document->gallery_type;
                $response['exhibit_year'] = $document->exhibit_year;
                $gallery = $this->api_model->get_gallery_name($document->user_id);
                $response['gallery'] = $gallery[0]->name;
                $response['gallery_email'] = $gallery[0]->email;
                $response['gallery_booth'] = $gallery[0]->booth1;
                $response['gallery_booth2'] = $gallery[0]->booth2;
                $response['booth1_img_link'] = $gallery[0]->booth1_img_link;
                $response['booth2_img_link'] = $gallery[0]->booth2_img_link;

                $booth1_allocation = json_decode($gallery[0]->booth1_allocation);
                if ($booth1_allocation) {
                    $response['booth1_allocation'] = site_url('../uploads/booth-location/' . $booth1_allocation->file_name);
                }

                $booth2_allocation = json_decode($gallery[0]->booth2_allocation);
                if ($booth2_allocation) {
                    $response['booth2_allocation'] = site_url('../uploads/booth-location/' . $booth2_allocation->file_name);
                }


                $file = json_decode($document->files);
                $response['file'] = $file->file_name;
            }
            echo json_encode($response, true);
        } else {
            echo 'Authentication Failed!';
        }

    }

    private function check_api_key($key)
    {

        if ($key === $this->apiKey) {
            return true;
        } else {
            return false;
        }

    }

    private function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        if ($result !== FALSE) {
            return true;
        } else {
            return false;
        }
    }

    private function check_file_name($gallery, $file)
    {

        $residents = array('Revolver Galería', 'Instituto de Visión', 'Barro', 'Casa Triângulo', 'Servando', 'Galeria Pilar', 'Mendes Wood DM', 'Galleria Continua', 'A Gentil Carioca');

        $cdn = 'http://artdubai.lvzvrijbr5hr9afnw.maxcdn-edge.com/';
        // $cdn = 'https://galleries.artdubai.ae/uploads/';

        if (in_array($gallery, $residents)) {
            $src = $cdn . 'residents/' . $file;
            $file_found = $this->checkRemoteFile($src);
            if ($file_found) {
                $src = $cdn . 'residents/' . $file;

            } else {
                $src = $cdn . $file;
            }
        } else {
            $src = $cdn . $file;
        }

        return $src;

    }

    public function api_update_vip()
    {
        $status = false;
        $code = 400;
        $message = "Error";
        $response = array();
        // echo json_encode($response, true);
        $apiKey = $this->input->get_request_header('api-key', TRUE);
        $result = $this->check_api_key($apiKey);
        if ($result === true) {
            
            $email = $this->input->post('email');
            $user_id = $this->input->post('user_id');

            $res = $this
			->db
			->where( 'user_id', $user_id )
			->where( 'email', $email )
            ->get( 'vip' );

            if( $res->num_rows() > 0 ) {
                $dataArr = array();

                $dataArr['salutation'] = $this->input->post('salutation');
                $dataArr['first_name'] = $this->input->post('first_name');
                $dataArr['sur_name'] = $this->input->post('sur_name');
                $dataArr['company'] = $this->input->post('company');
                $dataArr['job_title'] = $this->input->post('job_title');
                $dataArr['mobile'] = $this->input->post('mobile');
                $dataArr['mobile_code'] = $this->input->post('mobile_code');
                //$dataArr['email'] = $this->input->post('email');
                $dataArr['country'] = $this->input->post('country');
                $dataArr['invite_type'] = $this->input->post('invite_type');
                $dataArr['city'] = $this->input->post('city');
                $dataArr['state'] = $this->input->post('state');

                $q = $this
                ->db
                ->where( 'user_id', $user_id )
                ->where( 'email', $email )
                ->update( 'vip', $dataArr );

                if(!$q)
                {
                    $message = "Error Occured while updating your record!";
                    $status = false;
                    $code = 400;

                }else{
                    $message = "Record Updated Successfully!";
                    $status = true;
                    $code = 200;
                }

            }else{
                $message = "UserId & Email is required for validation!";
                $status = false;
                $code = 400;
            }
            

            

        }else{
            $message = $apiKey." Key is wrong!";
            $status = false;
            $code = 400;

        }

            $response['status'] = $status;
            $response['code'] = $code;
            $response['message'] = $message;

            echo json_encode($response, true);
    }

}
