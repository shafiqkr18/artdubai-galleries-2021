<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogue extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('user');
		}
        $this->load->model('catalogue_model');
        $this->load->model('gallery_model');
	}

    public function index()
    {

        $user = $_SESSION['userData'];

        $data['gp'] = $this->gallery_model->get_gallery_profile( $user->id );

        $this->load->view('templates/header' );
        $this->load->view('static/catalogue', $data);
        $this->load->view('templates/footer');

    }

}
