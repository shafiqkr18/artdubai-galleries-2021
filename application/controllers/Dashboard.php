<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct() {
		parent::__construct();
		
		session_start();
		$this->load->model('dashboard_model');
        $this->load->model('gallery_model');
        $this->load->model('page_model');
        $this->load->model('user_model');
	}

	public function index()
	{

		$data   = array();
		

		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('/');
		}

		$user = $_SESSION['userData'];

		$data['user'] = $user;

        $data['notifications'] = $this->dashboard_model->get_unread_notifications( $user->id );
        $result = $this->page_model->get_page('schedule');
        $data['data'] = json_decode( $result[0]->content );

        $data['gp'] = $this->gallery_model->get_gallery_profile( $user->id );

		$this->load->view('templates/header' );
		$this->load->view('dashboard', $data);
		$this->load->view('templates/footer' );

	}

	public function notifications()
	{

		$data = array();

		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('/');
		}

		$user = $_SESSION['userData'];

		$data['notifications'] = $this->dashboard_model->get_notifications( $user->id );

		$this->load->view('templates/header' );
		$this->load->view('notifications', $data);
		$this->load->view('templates/footer');

	}

	public function notification_update()
	{

		$id = $this->input->post( 'id' );
		$user_id = $this->input->post( 'user_id' );

		$result = $this->dashboard_model->notification_mark_read( $id, $user_id );

		echo $result;

	}

	public function search()
	{

		$data = array();

		$match = $this->input->post('s');

		if( !isset( $match ) )
		{
			$data['keyword'] = '';
			$data['search'] = array();
		}
		else
		{
			$data['keyword'] = $match;
			$data['search'] = $this->dashboard_model->get_search( $match );
		}

		$this->load->view('templates/header' );
		$this->load->view('search', $data);
		$this->load->view('templates/footer');

	}

	public function volunteer_request()
	{

		$posted_data = $this->input->post();
		if( isset( $posted_data ) && !empty( $posted_data ) )
		{
			$already_submitted = $this->dashboard_model->get_volunteer_request( $posted_data['userID'] );
			if( $already_submitted == true ) {
				echo json_encode( array( 'error' => 'You have already submitted a request for a Volunteer.' ) );
			} else {
				$this->dashboard_model->set_volunteer_request( $posted_data['userID'] );
				echo json_encode( array( 'success' => 'Submitted Successfully.' ) );
			}
		}
		else
		{
			show_error( 'Not allowed.', 405 );
		}

	}

    public function gallery_fascia()
    {

        $posted_data = $this->input->post();
        
        if( isset( $posted_data ) && !empty( $posted_data ) )
        {
            $already_added = $this->dashboard_model->get_gallery_fascia( $posted_data['userID'] );
            if( $already_added == true ) {

                $user = $_SESSION['userData'];

                $this->dashboard_model->update_gallery_fascia( $posted_data['userID'], $posted_data );

                // Set email mesaage setting
                $to         = 'galleriesartdubai@gmail.com'; // 'kabir@wewantzoom.com'; // 
                $subject    = 'Gallery Name Board Approved';
                $cc         = 'umair@wewantzoom.com'; // 'kabirahmed.dms@gmail.com'; // 
                $attachment = '';
                $message    = '<p>'.$user->galleryname.' Gallery has approved their gallery name board.</p>';

                $send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

                echo json_encode( array( 'success' => 'Submitted Successfully.' ) );

            } else {

                $this->dashboard_model->set_gallery_fascia( $posted_data['userID'], $posted_data );

                echo json_encode( array( 'success' => 'Submitted Successfully.' ) );

            }
        }
        else
        {
            show_error( 'Sorry! Something went wrong.', 405 );
        }


    }

    public function gallery_communication()
    {

        $posted_data = $this->input->post();

        if( isset( $posted_data ) && !empty( $posted_data ) )
        {
            $already_added = $this->dashboard_model->get_gallery_communication( $posted_data['userID'] );
            if( $already_added == true ) {

                $this->dashboard_model->update_gallery_communication( $posted_data['userID'], $posted_data );

                $user = $_SESSION['userData'];

                // Set email mesaage setting
                $to         = 'galleriesartdubai@gmail.com'; // 'kabir@wewantzoom.com'; // 
                $subject    = 'Gallery Name Confirmation';
                $cc         = 'umair@wewantzoom.com'; // 'kabirahmed.dms@gmail.com'; // 
                $attachment = '';
                $message    = '<p>'.$user->galleryname.' Gallery has confirmed their gallery name information for communications.</p>';

                $send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

                echo json_encode( array( 'success' => 'Submitted Successfully.' ) );

            } else {
                $this->dashboard_model->set_gallery_communication( $posted_data['userID'], $posted_data );
                echo json_encode( array( 'success' => 'Submitted Successfully.' ) );
            }
        }
        else
        {
            show_error( 'Sorry! Something went wrong.', 405 );
        }


    }

	public function accommodation_request()
	{

		$posted_data = $this->input->post();
		if( isset( $posted_data ) && !empty( $posted_data ) )
		{
            $userData = $_SESSION['userData'];
			$already_submitted = $this->dashboard_model->get_accommodation_request( $posted_data['userID'] );
			if( $already_submitted == true ) {
				echo json_encode( array( 'error' => 'You have already applied for Accommodation & Travel.' ) );
			} else {
				$this->dashboard_model->set_accommodation_request( $posted_data['userID'] );

                // Set email mesaage setting
                $to         = 'leo@artdubai.ae'; // 'kabir@wewantzoom.com'; // 
                $subject    = 'Accommodation Request';
                $cc         = 'galleriesartdubai@gmail.com,umair@wewantzoom.com'; // 'kabirahmed.dms@gmail.com'; // 'genna@artdubai.ae, umair@wewantzoom.com'; // 
                $attachment = '';
                $message    = '<p>You have received a travel and accommodation information request.</p>';
                $message   .= '<table><thead><tr><th>Full Name</th><th>Gallery Name</th><th>Email</th></tr></thead><tbody><tr><td>' . $userData->fullname . '</td><td>' . $userData->galleryname . '</td><td>' . $userData->email . '</td></tr></tbody></table>';

                $send_email = send_email_notification($to, $subject, $message, $cc, $attachment );


				echo json_encode( array( 'success' => 'Submitted Successfully.' ) );
			}
		}
		else
		{
			show_error( 'Sorry! Something went wrong.', 405 );
		}

	}

}
