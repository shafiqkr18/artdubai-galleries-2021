<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('library_model');
		$this->load->model('gallery_model');
	}

	public function index()
	{

		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('user');
		}

		$user = $_SESSION['userData'];

		$image_id = $this->uri->segment(2, 0);
		
    	$data['gp'] = $this->gallery_model->get_gallery_profile( $user->id );

        $data['mediums'] = $this->library_model->get_all_mediums();
        
        $data['exhibit_years'] = $this->library_model->get_document_exhibit_years($user->id);

		if( !empty( $image_id ) )
		{
		    
            $files = $this->library_model->get_document_by_id( $user->id, $image_id );
            $mediums = $this->library_model->get_file_medium( $image_id );
            if( $mediums ) {
                foreach ( $mediums as $medium ) {
                    $data['mediums_selected'][] = $medium['medium_id'];
                }
            }

            if( !empty( $files ) )
            {
                $data['files'] = $files;
                $data['gp'] = $this->gallery_model->get_gallery_profile( $user->id );
            }
            else
            {
                $data['files'] = array();
            }
			$view = 'image_library_b';
		}
		else
		{
            $files = $this->library_model->get_documents( $user->id );

            if( !empty( $files ) )
            {
                $data['files'] = $files;
            }
            else
            {
                $data['files'] = array();
            }
			$view = 'image_library';
		}

		$this->load->view('templates/header' );
		$this->load->view($view, $data);
		$this->load->view('templates/footer');

	}

	public function upload()
	{

		$config['upload_path']      = './uploads/';
		$config['allowed_types']    = 'jpg|jpeg';
		$config['max_size']         = 20000;
		$config['min_width']        = 2000;
		$config['encrypt_name']     = true;

		$data = $this->input->post();

		$config_b['image_library']    = 'gd2';
		$config_b['create_thumb']     = TRUE;
		$config_b['maintain_ratio']   = FALSE;
		//$config_b['width']            = 373;
		//$config_b['height']           = 214;
		$config['max_width']            = 6000; 
		$config['max_height']           = 6000; 

		$this->load->library( 'image_lib', $config_b );
		$this->load->library( 'upload', $config );

		if( !empty( $data ) )
		{
			if ( ! $this->upload->do_upload( 'file' ))
			{
				echo json_encode( array( 'error' => "upload file error=".$this->upload->display_errors() ) );
			}
			else
			{

				$user = $_SESSION['userData'];
				$upload_data = $this->upload->data();

				// Set email mesaage setting
				$to			= 'galleriesartdubai@gmail.com'; // 'kabir@wewantzoom.com'; //
				$subject	= 'Artwork Library - Added Information';
				$cc 		= 'umair@wewantzoom.com'; // 'umair@wewantzoom.com, judy@artdubai.ae'; //
				$attachment	= '';
				$message	= $user->galleryname . ' has uploaded an image in their Library.' ;

				$config_b['source_image'] =  $upload_data['full_path'];
				$this->image_lib->initialize( $config_b );
				$this->image_lib->resize();
				$this->image_lib->clear();

				$data['file'] = json_encode( array( 'file_name' => $upload_data['file_name'], 'raw_name' => $upload_data['raw_name'], 'file_ext' => $upload_data['file_ext'], 'file_size' => $upload_data['file_size'], 'image_width' => $upload_data['image_width'], 'image_height' => $upload_data['image_height'] ) );
                
                // get_event_year is defined in helper functions_helper.php
                $data['exhibit_year']   = get_event_year();
				$doc_id = $this->library_model->set_document( $user->id, $data );

      	//$this->library_model->update_file_medium($doc_id, $data['medium']);

        //$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

				echo json_encode( array( 'success' => 'Submitted Successfully.' ) );

			}
		}
		else
		{
			show_error( 'Not allowed.', 405 );
		}

	}

	public function update()
	{

		$data = $this->input->post();
		if( !empty( $data ) )
		{
			$user = $_SESSION['userData'];

			$updated = $this->library_model->update_document( $user->id, $data );

			if( $updated !== false ) {

			    if( isset($data['catalogue']) && $data['catalogue'] !== '0' )
                {

                	$to			= 'galleriesartdubai@gmail.com'; // 'kabir@wewantzoom.com'; //
									$subject	= 'Artwork Library - Updated Information';
									$cc 		= 'umair@wewantzoom.com'; // 'kabirahmed.dms@gmail.com'; //
									$attachment	= '';
									$message	= $user->galleryname . ' has updated information in their Library.';

									$send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

                }

				echo json_encode( array( 'success' => 'Submitted Successfully.' ) );

			} else {

				echo json_encode( array( 'error' => 'Sorry! Something went wrong.' ) );

			}
		}
		else
		{
			show_error( 'Not allowed.', 405 );
		}

	}

	public function delete()
	{

		$id = $this->input->post('id');

		if( isset( $id ) && !empty( $id ) )
		{
			$user = $_SESSION['userData'];
			$file = $this->library_model->get_document_by_id( $user->id, $id );
			$file_data = $file[0]->files;
			$obj = json_decode( $file_data );
			$this->library_model->delete_document( $user->id, $id );
			$file1 = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $obj->raw_name . $obj->file_ext;
			$file2 = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $obj->raw_name . '_thumb' . $obj->file_ext;
			unlink( $file1 );
            if( file_exists( $file2 ) ) {
                unlink( $file2 );
            }

            // Set email mesaage setting
			$to			= 'galleriesartdubai@gmail.com'; // 'kabir@wewantzoom.com'; //
			$subject	= 'Artwork Library - Deleted Information';
			$cc 		= 'umair@wewantzoom.com'; // 'umair@wewantzoom.com'; //
			$attachment	= '';
			$message	= $user->galleryname . ' has deleted an image in their Library.';

            $send_email = send_email_notification($to, $subject, $message, $cc, $attachment );

			echo json_encode( array( 'success' => 'Selected image has been deleted.' ) );

		}
		else
		{
			show_error( 'Access not allowed.', 405 );
		}

	}

}
