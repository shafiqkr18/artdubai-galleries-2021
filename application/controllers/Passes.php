<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passes extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('passes_model');
	}

	public function index()
	{

		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('user');
		}

		$user = $_SESSION['userData'];

		$type = $this->uri->segment(2, 0);

		$data['section'] = $type;
        $passes = $this->passes_model->get_passes( $user->id, $type );
		$data['artists'] = $this->passes_model->get_artists( $user->id );
        $data['staffs'] = $this->passes_model->get_staff( $user->id );

        if( isset( $passes ) && !empty( $passes ) )
        {
            foreach ( $passes as $pass )
            {
                if( $pass->type == 'artist' )
                {
                    $person = $this->passes_model->get_a_artist( $pass->aid );
                }
                else
                {
                    $person = $this->passes_model->get_a_staff( $pass->aid );
                }
                $data['passes'][] = array( 'id' => $pass->id, 'person' => $person, 'thumb' => $pass->thumb );
            }
        }
        else
        {
            $data['passes'] = array();
        }

		$this->load->view('templates/header' );
		$this->load->view('passes_' . $type, $data);
		$this->load->view('templates/footer');

	}

	public function create()
	{

		$posted_data = $this->input->post();

		if( !empty( $posted_data ) )
		{

			$user = $_SESSION['userData'];

			$config['upload_path']      = './uploads/passes/';
			$config['allowed_types']    = 'jpg|jpeg';
			$config['max_size']         = 1020;
			$config['encrypt_name']     = true;
			$config['min_width']        = 354;
			$config['min_height']       = 472;

			$this->load->library( 'upload', $config );

			if ( ! $this->upload->do_upload( 'file' ))
			{
				echo json_encode( array( 'error' => $this->upload->display_errors() ) );
			}
			else
			{

				$upload_data = $this->upload->data();

				$data['aid'] = $posted_data['aid'];
				$data['type'] = $posted_data['section'];
				$data['thumb'] = $upload_data['file_name'];

				$this->passes_model->set_passes( $user->id, $data );

				echo json_encode( array( 'success' => 'Image uploaded successfully!', 'section' => $posted_data['section'] ) );

			}

		}
		else
		{

			show_error( 'Not allowed!' );

		}

	}

	public function delete()
	{

		$id = $this->input->post('id');

		if( isset( $id ) && !empty( $id ) )
		{
			$file = $this->passes_model->get_image( $id );
			$this->passes_model->delete_passes( $id );
			$file_to_delete = $_SERVER['DOCUMENT_ROOT'] . '/uploads/passes/' . $file[0]->thumb;
			unlink( $file_to_delete );
			echo json_encode( array( 'success' => 'Selected information has been deleted.' ) );
		}
		else
		{
			show_error( 'Access not allowed.', 405 );
		}

	}

}