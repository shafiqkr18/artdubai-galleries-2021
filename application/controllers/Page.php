<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		if( !isset( $_SESSION['userData'] ) )
		{
			redirect('user');
		}
        $this->load->model('page_model');
	}

	public function shipping()
	{

		$result = $this->page_model->get_page('shipping');
		$data['data'] = json_decode( $result[0]->content );

		$this->load->view('templates/header' );
		$this->load->view('static/shipping', $data);
		$this->load->view('templates/footer');

	}

	public function booth_design()
	{

        $userData = $_SESSION['userData'];

        $this->load->model('dashboard_model');
        $this->load->model('gallery_model');

		$result = $this->page_model->get_page('booth_design');
		$data['data'] = json_decode( $result[0]->content );

        $gf = $this->dashboard_model->get_gallery_fascia( $userData->id );
        if( isset( $gf ) && !empty( $gf ) ) {
            $data['gallery_fascia'] = $gf;
        } else {
            $data['gallery_fascia'] = array();
        }

        $gp = $this->gallery_model->get_gallery_profile( $userData->id );
        if( isset( $gp ) && !empty( $gp ) ) {
            $b1     = (!empty( $gp[0]->booth1 ) ) ? explode('|', $gp[0]->booth1) : '' ;
            $data['gallery_booth'] = (!empty( $b1[0] ) ) ? $b1[0] : '' ;
            $data['indiviual_booth_plan'] = (!empty( $gp[0]->indiviual_booth_plan ) ) ? $gp[0]->indiviual_booth_plan : '' ;
            $data['gallery_profile'] = $gp;
        } else {
            $data['gallery_booth'] = '';
            $data['gallery_profile'] = array();
        }

		$this->load->view('templates/header' );
		$this->load->view('static/booth_design', $data);
		$this->load->view('templates/footer');

	}

	public function contact()
	{

        $result = $this->page_model->get_page('contact');
        $data['data'] = json_decode( $result[0]->content );

		$this->load->view('templates/header' );
		$this->load->view('static/contact', $data);
		$this->load->view('templates/footer');

	}

	public function travel()
	{

        $result = $this->page_model->get_page('travel');
        $data['data'] = json_decode( $result[0]->content );

		$this->load->view('templates/header' );
		$this->load->view('static/travel', $data);
		$this->load->view('templates/footer');

	}

	public function communications()
	{

        $userData = $_SESSION['userData'];
        $this->load->model('dashboard_model');

        $this->load->model('gallery_model');

		$result = $this->page_model->get_page('communications');
		$data['data'] = json_decode( $result[0]->content );

        $gp = $this->gallery_model->get_gallery_profile( $userData->id );
        if( isset( $gp ) && !empty( $gp ) ) {
            $data['gallery_profile'] = $gp[0];
        }

        $gc = $this->dashboard_model->get_gallery_communication( $userData->id );
        if( isset( $gc ) && !empty( $gc ) ) {
            $data['gallery_communication'] = $gc;
        } else {
            $data['gallery_communication'] = array();
        }

		$this->load->view('templates/header' );
		$this->load->view('static/communications', $data);
		$this->load->view('templates/footer');

	}

	public function volunteer()
	{

		$result = $this->page_model->get_page('volunteer');
		$data['data'] = json_decode( $result[0]->content );

		$this->load->view('templates/header' );
		$this->load->view('static/volunteer', $data);
		$this->load->view('templates/footer');

	}

    public function list_of_works()
    {

        $this->load->view('templates/header' );
        $this->load->view('static/works');
        $this->load->view('templates/footer');

    }

    public function my_exhibition()
    {

        $this->load->model('gallery_model');

        $volunteerPage = $this->page_model->get_page('volunteer');
        $data['volunteer'] = json_decode( $volunteerPage[0]->content );

        $user = $_SESSION['userData'];

        $data['gp'] = $this->gallery_model->get_gallery_profile( $user->id );
        $data['tp'] = $this->gallery_model->get_gallery_types();

        $ae = $this->gallery_model->get_artists_exhibiting( $user->id );

        if(isset($ae)){
            $artists = json_decode( $ae->artists );

            foreach ( $artists as $artist ) {
                $data['ae'][] = $this->gallery_model->get_an_artist( (int) $artist );
            }
        }


        if( !empty( $ae->additional_artists ) ) {
            $data['aae'] = explode( ',', $ae->additional_artists );
        }



        $this->load->view('templates/header' );
        $this->load->view('static/exhibition', $data);
        $this->load->view('templates/footer');

    }

    public function important_documents()
    {

        $data['documents'] = $this->page_model->get_all_documents();

        $this->load->view('templates/header' );
        $this->load->view('static/documents', $data);
        $this->load->view('templates/footer');

    }

}
