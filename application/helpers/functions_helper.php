<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_event_year'))
{
    function get_event_year(){
        $CI=& get_instance();
        
        $CI->load->model('page_model');
	    $content = $CI->page_model->get_page( 'schedule' );
	    
	    if($content){
	        $data = json_decode( $content[0]->content );
	        
	        if($data->event_year){
	           return $data->event_year;
	       }else{
	          return false; 
	       }
	    }
		
    }
}
        

if ( ! function_exists('send_email_notification'))
{
    function send_email_notification($to, $subject, $message, $cc, $attachment ){
        $CI=& get_instance();

        $config = array(
                'mailtype'      => 'html',
                'smtp_host'     => 'smtp-relay.gmail.com',
                'smtp_user'     => 'umair@wewantzoom.com', 
                'smtp_pass'     => 'Umair@Zoom5858!',
                'smtp_port'     => '587',
                'smtp_crypto'   => 'ssl'
            );

        // Load email library an helpers
        $CI->load->library('email');
        $CI->load->helper('url');
        $CI->load->helper('string');

        $CI->email->initialize($config);
        $CI->email->set_mailtype("html");
        $CI->email->set_newline("\r\n"); 
        $CI->email->from('no-reply@artdubai.ae', 'Art Dubai');
        $CI->email->to($to);

        if(!empty($cc)){
            $CI->email->cc($cc); 
        }

        if(!empty($attachment)):
            $CI->email->attach($attachment);
        endif;

        $CI->email->subject($subject);
        $CI->email->message( $message );

        $result = $CI->email->send();

        return $result  ;
    }

}

if ( ! function_exists('get_gravatar'))
{

	function get_gravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}

}

if( ! function_exists('html_to_text') )
{

	function html_to_text( $html ) {

		$array = json_decode( $html, true );
		$text = implode( ' ', $array );

		$remove_tags = strip_tags( $text );

		return substr( $remove_tags, 0, 300 );

	}

}

if ( ! function_exists('upload_dir_admin'))
{

    function upload_dir_admin( $slug = '' ) {
        $url = base_url( 'admin/uploads/' );
        return $url . $slug;
    }

}

if ( ! function_exists('upload_dir'))
{

    function upload_dir( $slug = '' ) {
        $url = base_url( 'uploads/' );
        return $url . $slug;
    }

}

if ( ! function_exists('get_popup_footer'))
{

    function show_popup_footer() {

        $display    = '';
        $data       = array();

        if(!empty( $_SESSION['userData'])):

            $CI = get_instance();

            $CI->load->model('gallery_model');
            
            $user   = $_SESSION['userData'];
            $data['gp']     = $CI->gallery_model->get_gallery_profile( $user->id );
            $data['gl']     = $CI->gallery_model->get_locations( $user->id );

    		if( !empty( $data['gp'][0]->confirmation ) && $data['gp'][0]->confirmation !== 0 ){
    		    $display = 'no';
    		} else {
    		    $display = 'yes';
    		}

        endif;

        return array( 'display' => $display, 'info' => $data );

    }

}

if ( ! function_exists('array_to_string'))
{

    function array_to_string( $array, $separator ) {

        $output = '';

        if( is_array( $array ) ) {

            $counter = 1;

            foreach ( $array as $item ) {
                if( $counter < count( $array ) ) {
                    $output .= $item . $separator;
                } else {
                    $output .= $item;
                }
                $counter++;
            }

        }

        return $output;

    }

}