<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model {

	/* GALLERY - VIEW */

	public function get_gallery_profile( $user_id )
	{

		$q = $this
			->db
			->select('*')
			->from('gallery_profile')
			->where('user_id', $user_id)
			->get();

		if( $q->num_rows() > 0 )
		{
			return $q->result();
		}

		return false;

	}

	public function get_gallery_types()
    {

        $q = $this
            ->db
            ->get('gallery_types');

        if( $q->num_rows() > 0 )
        {
            return $q->result_array();
        }

        return false;

    }

	public function get_social_platforms( $userid )
	{
		$q = $this
			->db
			->where('user_id',$userid)
			->limit(1)
			->get('social_platforms');

		if( $q->num_rows() > 0 )
		{
			return $q->result_array();
		}

		return false;

	}

	public function get_locations( $userid )
	{
		$q = $this
			->db
			->where('user_id',$userid)
			->limit(4)
			->get('locations');

		if( $q->num_rows() > 0 )
		{
			return $q->result_array();
		}

		return false;

	}

	public function get_primary_location( $userid )
	{

		$q = $this
			->db
			->where('user_id',$userid)
			->where('type',1)
			->limit(1)
			->get('locations');

		if( $q->num_rows() > 0 )
		{
			return $q->row();
		}

		return false;

	}

	public function get_gallery_team($userid)
	{
		$q = $this
			->db
			->where('user_id',$userid)
			->limit(4)
			->get('gallery_team');

		if( $q->num_rows() > 0 )
		{
			return $q->result_array();
		}

		return false;

	}

	public function get_participation($userid)
	{
		$q = $this
			->db
			->where('user_id',$userid)
			->get('participation');

		if( $q->num_rows() > 0 )
		{
			return $q->result_array();
		}

		return false;

	}

	public function get_artists($userid)
	{
		$q = $this
			->db
			->where('user_id',$userid)
			->get('artists');

		if( $q->num_rows() > 0 ) {
			return $q->result_array();
		}

		return false;

	}

	public function delete_gallery( $user_id )
	{

		$tables = array('social_platforms','participation','locations','gallery_team','gallery_profile','library','artists','accommodation_request','passes','vip','volunteer_request');
		$this->db->where('user_id',$user_id)->delete($tables);

		return true;

	}

	/* GALLERY - EDIT */

    public function edit_gallery_profile( $userID, $id = NULL )
    {

        $this->db->where('user_id', $userID);

        if( !empty( $id ) )
        {
            $this->db->where('id', $id );
        }

        $q =  $this->db->get('gallery_profile');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function edit_social_platforms( $userID, $id = NULL )
    {

        $this->db->where('user_id', $userID);

        if( !empty( $id ) )
        {
            $this->db->where('id', $id );
        }

        $q =  $this->db->get('social_platforms');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function edit_locations( $userID, $id = NULL )
    {

        $this->db->where('user_id', $userID);

        if( !empty( $id ) )
        {
            $this->db->where('id', $id );
        }

        $q =  $this->db->get('locations');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function edit_gallery_team( $userID, $id = NULL )
    {

        $this->db->where('user_id', $userID);

        if( !empty( $id ) )
        {
            $this->db->where('id', $id );
        }

        $q =  $this->db->get('gallery_team');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function edit_artists( $userID, $id = NULL )
    {

        $this->db->where('user_id', $userID);

        if( !empty( $id ) )
        {
            $this->db->where('id', $id );
        }

        $q =  $this->db->get('artists');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function edit_participation( $userID, $id = NULL )
    {

        $this->db->where('user_id', $userID);

        if( !empty( $id ) )
        {
            $this->db->where('id', $id );
        }

        $q =  $this->db->get('participation');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    /* GALLERY - UPDATE */

    public function update_gallery_profile( $id, $data )
    {

        unset( $data['section'] );
        unset( $data['id'] );
        unset( $data['type1'] );
        unset( $data['type2'] );
        unset( $data['b1_m2'] );
        unset( $data['b2_m2'] );

        $this->db->where('id', $id );
        $this->db->update('gallery_profile', $data);

        return true;

    }

    public function update_social_platforms( $id, $data )
    {

        unset( $data['section'] );
        unset( $data['id'] );

        $this->db->where('id', $id );
        $this->db->update('social_platforms', $data);

        return true;

    }

    public function update_locations( $id, $data )
    {

        unset( $data['section'] );
        unset( $data['id'] );

        $this->db->where('id', $id );
        $this->db->update('locations', $data);

        return true;

    }

    public function update_gallery_team( $id, $data )
    {

        unset( $data['section'] );
        unset( $data['id'] );

        $this->db->where('id', $id );
        $q = $this->db->update('gallery_team', $data);

        return $q;

    }

    public function update_artists( $id, $data )
    {

        unset( $data['section'] );
        unset( $data['id'] );
        unset( $data['exhibit_flag'] );
        unset( $data['userid'] );

        $this->db->where('id', $id );
        $this->db->update('artists', $data);

        return true;

    }

    public function create_artists( $data )
    {

        unset( $data['exhibit_flag'] );

        $this->db->insert('artists', $data);

        return $this->db->insert_id();

    }

    public function get_exhibiting_artists( $userid )
    {

        $q = $this
            ->db
            ->select('artists')
            ->where('user_id',$userid)
            ->get('artists_exhibit');

        if( $q->num_rows() > 0 ) {
            return $q->result_array();
        }

        return false;

    }

    public function update_exhibiting_artists( $id, $data )
    {

        $this->db->set('artists', $data );
        $this->db->where('user_id', $id );
        $this->db->update('artists_exhibit');

        return true;

    }

    public function delete_artist( $id )
    {

        $this->db->where('id',$id)->delete('artists');

        return true;

    }

    public function update_participation( $id, $data )
    {

        unset( $data['section'] );
        unset( $data['id'] );

        $this->db->where('id', $id );
        $this->db->update('participation', $data);

        return true;

    }

	/* VOLUNTEER */

	public function get_volunteer_request( $userid )
	{

		$q = $this
			->db
			->where('user_id', $userid)
			->get('volunteer_request');

		if( $q->num_rows() > 0 )
		{
			return $q->result();
		}

		return false;

	}

	/* TRAVEL */

	public function get_travel_request( $userid )
	{

		$q = $this
			->db
			->where('user_id', $userid)
			->get('accommodation_request');

		if( $q->num_rows() > 0 )
		{
			return $q->result();
		}

		return false;

	}

    /* FASCIA */

    public function get_gallery_communication( $userid )
    {

        $q = $this
            ->db
            ->where('user_id', $userid)
            ->get('gallery_communication');

        if( $q->num_rows() > 0 )
        {
            return $q->row();
        }

        return false;

    }

    /* FASCIA */

    public function get_gallery_fascia( $userid )
    {

        $q = $this
            ->db
            ->where('user_id', $userid)
            ->get('gallery_fascia');

        if( $q->num_rows() > 0 )
        {
            return $q->row();
        }

        return false;

    }

    public function get_gallery_fascia_by_id( $id )
    {

        $q = $this
            ->db
            ->where('id', $id)
            ->get('gallery_fascia');

        if( $q->num_rows() > 0 )
        {
            return $q->row();
        }

        return false;

    }

    public function update_gallery_fascia_booth( $id, $booth )
    {

        $q = $this
            ->db
            ->where('id', $id)
            ->set('booth', $booth)
            ->limit(1)
            ->update('gallery_fascia');


            return $q;


    }

	/* USER */

	public function get_user_by_id($id)
	{

		$users = $this->db->query( "SELECT * FROM users WHERE id = $id" );

		if( $users->num_rows() > 0 ){
			return $users->result();
		}

		return false;

	}

	public function get_all_users()
	{

		$users = $this->db->query( "SELECT * FROM users WHERE role != 'admin' AND archive = 0 AND archive_year = 0"  );

		if( $users->num_rows() > 0 ){
			return $users->result();
		}

		return false;

	}

	public function update_user( $data, $password )
	{

		$id = $data['id'];

		if( $password == TRUE )
		{
			$data = array(
				'fullname' => $data['fullname'],
				'galleryname' => $data['galleryname'],
				'email' => $data['email'],
				'password' => sha1( $data['password'] ),
			);
		}
		else
		{
			$data = array(
				'fullname' => $data['fullname'],
				'galleryname' => $data['galleryname'],
				'email' => $data['email']
			);
		}

		$q = $this
			->db
			->where('id', $id)
			->update('users', $data);

		return $q;

	}

	public function delete_user( $id )
	{

		$this->db->where('id',$id)->delete('users');
		$tables = array('social_platforms','participation','locations','gallery_team','gallery_profile','library','artists','accommodation_request','passes','vip','volunteer_request');
		$this->db->where('user_id',$id)->delete($tables);

		return true;

	}

}
