<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vip_model extends CI_Model {

	public function __construct() {

	}

	public function get_entries()
	{

		$q = $this
			->db
			->select('*')
			->from('vip')
			->get();

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

    public function get_gallery_name( $userID )
    {

        $response = $this
            ->db
            ->select( 'galleryname' )
            ->where( 'id', $userID )
            ->get( 'users' );

        if( $response->num_rows() > 0 ) {
            return $response->result();
        }

        return false;

    }

	public function get_entry( $id )
	{

		$q = $this
			->db
			->select('*')
			->from('vip')
			->where( 'id', $id )
			->get();

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

    public function delete_entry( $id )
    {

        $this->db->where('id', $id);

        $this->db->delete('vip');

        return true;

    }

}