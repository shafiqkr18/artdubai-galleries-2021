<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents_model extends CI_Model {

    public function __construct() {

    }
    
    public function get_all_files()
    {

        $q = $this
            ->db
            ->select('*')
            ->from('documents')
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }


    public function set_document( $userID, $data )
    {

        $data = array(
            'user_id'       => $userID,
            'name'          => $data['file_name'],
            'file'          => $data['file'],
            'date_created'  => date( 'Y-m-d H:i:s' )
        );

        $q = $this
            ->db
            ->insert( 'documents', $data );

        return $q;

    }

    public function get_document_by_id($id){
        $q = $this
            ->db
            ->where('id', $id)
            ->limit(1)
            ->get('documents');

        if( $q->num_rows() > 0 ) {
            return $q->row();
        }
        return false;
    }

    public function delete_document($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('documents');

        return true;

    }



}