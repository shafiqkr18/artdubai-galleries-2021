<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication_model extends CI_Model {

	public function __construct() {

	}

	public function verify_user($email, $password)
	{
		$q = $this
				->db
				->where('email',$email)
				->where('password',sha1($password))
				->where('role="admin" OR role="super-admin"')
				->limit(1)
				->get('users');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

	public function verify_email($email)
	{
		$q = $this
			->db
			->where('email',$email)
			->where('role','admin')
			->limit(1)
			->get('users');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

	public function verify_user_id($id)
	{
		$q = $this
			->db
			->where('id',$id)
			->where('role','')
			->limit(1)
			->get('users');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

	public function create_key($email,$key)
	{

		$this->db->set('password_reset_key',$key);
		$this->db->where('email',$email);
		$this->db->update('users');

	}

	public function verify_key($email,$key)
	{
		$q = $this
			->db
			->where('email',$email)
			->where('password_reset_key',$key)
			->where('role','admin')
			->limit(1)
			->get('users');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

	public function update_password($email,$password,$key)
	{

		$q = $this
				->db
				->set('password',sha1($password))
				->set('password_reset_key',NULL)
				->where('email',$email)
				->where('password_reset_key',$key)
				->update('users');

		return $q;

	}

}