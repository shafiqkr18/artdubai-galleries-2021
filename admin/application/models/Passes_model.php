<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passes_model extends CI_Model {

	public function __construct() {

	}

	public function get_passes()
	{

		$q = $this
			->db
			->select('*')
			->from('passes')
			->get();

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_gallery_name( $userID )
    {

        $response = $this
            ->db
            ->select( 'galleryname' )
            ->where( 'id', $userID )
            ->get( 'users' );

        if( $response->num_rows() > 0 ) {
            return $response->result();
        }

        return false;

    }

    public function get_a_artist( $artistID )
    {

        $response = $this
            ->db
            ->where( 'id', $artistID )
            ->get( 'artists' );

        if( $response->num_rows() > 0 ) {
            return $response->result();
        }

        return false;

    }

    public function get_a_staff( $staffID )
    {

        $response = $this
            ->db
            ->where( 'id', $staffID )
            ->get( 'gallery_team' );

        if( $response->num_rows() > 0 ) {
            return $response->result();
        }

        return false;

    }

}