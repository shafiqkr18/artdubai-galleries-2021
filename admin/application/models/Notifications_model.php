<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications_model extends CI_Model {

	public function __construct() {

	}

	public function get_all_notifications()
	{

        $q = $this->db->select('title,uid,message,date_created,type')
            ->from('notifications')
            ->group_by('title,uid,message,date_created,type')
            ->get();

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

    public function get_user_by_id( $id ){
        $q = $this
            ->db
            ->where('id', $id)
            ->limit(1)
            ->get('users');

        if( $q->num_rows() > 0 ) {
            return $q->row();
        }

        return false;
    }

    public function get_user_contacts_by_id( $id, $contactType ){
        $q = $this
            ->db
            ->where('user_id', $id)
            ->where('type', $contactType)
            ->get('gallery_team');

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;
    }

	public function get_all_users()
	{

		$q = $this
			->db
			->select('*')
			->from('users')
			->where('role !="admin"')
			->where('archive =', 0)
			->get();

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function add_to_db( $notification_id, $user_id, $posted_data )
	{

	    $data = array(
	        'uid' => $notification_id,
	        'user_id' => $posted_data['user_id'],
	        'title' => $posted_data['title'],
            'message' => $posted_data['message'],
            'receiver_id' => $user_id,
            'read' => 0,
            'type' => $posted_data['type']
        );
		$q = $this
			->db
			->insert('notifications', $data);

		return $q;

	}

	public function remove_from_db( $id )
	{

		$this->db->where('id', $id);

		$this->db->delete('notifications');

	}

}