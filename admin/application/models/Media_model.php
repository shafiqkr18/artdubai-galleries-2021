<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media_model extends CI_Model {

    public function __construct() {

    }
    
    public function get_gallery_exhibit_years( $user_id )
    {

        $query = $this->db->query( 'SELECT DISTINCT exhibit_year FROM `library` where user_id='.$user_id.' ORDER BY `library`.`exhibit_year` DESC'); 
                                    
        if( $query->num_rows() > 0 ) {
            return $query->result();
        }

        return false;

		
    }

    public function get_all_users($year)
    {


        $this->db->select('u.id,u.email,g.name');;
        $this->db->from('users u');
        $this->db->where('role','');
        $this->db->join('gallery_profile g','g.user_id=u.id');
        if( $year < date("Y") ){
            //$archive_status = 1;
            $this->db->where('u.archive =', 1);
            $this->db->where('u.archive_year =', $year);
        }else {
            //$archive_status = 0;
            $this->db->where('u.archive =', 0);
        }

        $q = $this->db->get();

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function get_all_files()
    {

        $q = $this
            ->db
            ->select('*')
            ->from('library')
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function get_all_files_by_id( $id, $year )
    {

        $this->db->select('*');
        $this->db->from('library');
        $this->db->where('user_id',$id);
        $this->db->where('exhibit_year',$year);

        $q = $this->db->get();

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function get_image_info( $id )
    {

        $q = $this
            ->db
            ->select('artist,year,title,medium,dimensions,courtesy,catalogue,gallery_type,exhibit_year')
            ->from('library')
            ->where('id',$id)
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;

    }

    public function get_catalogue_status( $id )
    {

        $q = $this
            ->db
            ->select('*')
            ->from('library')
            ->where('user_id',$id)
            ->where('catalogue','1')
            ->limit(1)
            ->get();

        if( $q->num_rows() > 0 ) {
            return '1';
        }

        return '0';

    }

    public function get_all_mediums()
    {

        $q = $this
            ->db
            ->select('id,name')
            ->from('library_medium')
            ->get();

        if( $q->num_rows() > 0 ) {
            return $q->result_array();
        }

        return false;

    }

    public function get_file_mediums( $id )
    {

        $q = $this
            ->db
            ->select('medium_id')
            ->from('library_medium_mapping')
            ->where('file_id',$id)
            ->get();

        if( $q->num_rows() > 0 ) {
            $results = $q->result_array();
            foreach ( $results as $result ) $data[] = $result['medium_id'];
            return $data;
        }

        return false;

    }

    public function clear_file_medium( $id )
    {

        $this->db->where('file_id', $id);
        $this->db->delete('library_medium_mapping');

        return true;

    }

    public function update_file_medium( $id, $medium )
    {

        $this->db->set( array( 'file_id' => $id, 'medium_id' => $medium ) );
        $this->db->insert('library_medium_mapping');

        return true;

    }

    public function update_file_caption( $id, $data )
    {

        $catalogue = (isset( $data['catalogue'] )) ? 1 : 0;

        $this->db->set( array( 'exhibit_year' => $data['exhibit_year'],  'gallery_type' => $data['gallery_type'], 'artist' => $data['artist'], 'title' => $data['title'], 'year' => $data['year'], 'medium' => $data['medium'], 'dimensions' => $data['dimensions'], 'courtesy' => $data['courtesy'], 'catalogue' => $catalogue ) );
        $this->db->where('id', $id);
        $this->db->update('library');

        return true;

    }

}
