<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct() {

	}

	public function get_list_menu(  )
	{

		$q = $this
			->db
			//->where('id', $menuId)
			//->limit(0)
			->get('important_action_menu');

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

	public function get_menu( $menuId )
	{

		$q = $this
			->db
			->where('id', $menuId)
			//->limit(0)
			->get('important_action_menu');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

	public function update_menu( $menuId, $value)
	{
		
		$q = $this
			->db
			->where('id', $menuId)
			->set('status', $value)
			->update('important_action_menu');

		return $q;

	}

}