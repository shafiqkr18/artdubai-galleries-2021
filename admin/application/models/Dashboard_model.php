<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct() {

	}

	public function get_total_users() {

		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('role !=','admin');
        $this->db->where('archive =', 0);
				$this->db->where('archive_year =', 0);
		$num_results = $this->db->count_all_results();

		return $num_results;

	}

    public function get_total_gallery_submissions()
    {

        $this->db->select('*');
        $this->db->from('gallery_profile');
        $this->db->join('users', 'users.id = gallery_profile.user_id', 'inner');
				$this->db->where('archive =', 0);
				$this->db->where('archive_year =', 0);

        $num_results = $this->db->count_all_results();

        return $num_results;

    }

}
