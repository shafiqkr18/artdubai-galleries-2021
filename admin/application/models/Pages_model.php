<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model {

	public function __construct() {

	}

	public function get_content( $page )
	{

		$q = $this
			->db
			->where('name', $page)
			->get('pages');

		if( $q->num_rows() > 0 ) {
			return $q->row();
		}

		return false;

	}

	public function update_content( $data_posted, $page )
	{

		$data = array(
			'content' => $data_posted
		);
		$q = $this
			->db
			->where('name', $page)
			->update('pages', $data);

		return $q;

	}

}