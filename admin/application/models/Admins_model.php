<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins_model extends CI_Model {

	public function __construct() {

	}

	public function get_all_users()
	{

		$q = $this
			->db
			->select('*')
			->from('users')
			->where('role!=""')
			->get();

		if( $q->num_rows() > 0 ) {
			return $q->result();
		}

		return false;

	}

}