<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('pages_model');
		
		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('user') );
		}
	}

	public function contact()
	{

		$content = $this->pages_model->get_content( 'contact' );
		$data['data'] = json_decode( $content->content );

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('pages/contact',$data);
		$this->load->view('templates/footer');

	}

	public function schedule()
	{

		$content = $this->pages_model->get_content( 'schedule' );
		$data['data'] = json_decode( $content->content );

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('pages/schedule',$data);
		$this->load->view('templates/footer');

	}

	public function travel()
	{

		$content = $this->pages_model->get_content( 'travel' );
		$data['data'] = json_decode( $content->content );

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('pages/travel',$data);
		$this->load->view('templates/footer');

	}

    public function communications()
    {

        $content = $this->pages_model->get_content( 'communications' );
        $data['data'] = json_decode( $content->content );

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/communications',$data);
        $this->load->view('templates/footer');

    }

    public function booth_design()
    {

        $content = $this->pages_model->get_content( 'booth_design' );
        $data['data'] = json_decode( $content->content );

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/booth_design',$data);
        $this->load->view('templates/footer');

    }

    public function shipping()
    {

        $content = $this->pages_model->get_content( 'shipping' );
        $data['data'] = json_decode( $content->content );

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/shipping',$data);
        $this->load->view('templates/footer');

    }

    public function volunteer()
    {

        $content = $this->pages_model->get_content( 'volunteer' );
        $data['data'] = json_decode( $content->content );

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/volunteer',$data);
        $this->load->view('templates/footer');

    }

	public function update()
	{

		$data = $this->input->post();
		$page_name = $data['page_name'];

		if( $page_name == 'shipping' ) {

            $existing_content_json = $this->pages_model->get_content( 'shipping' );
            $existing_content = json_decode( $existing_content_json->content );

            $config['upload_path']      = './uploads/';
            $config['allowed_types']    = 'jpg|jpeg';
            $config['encrypt_name']     = true;

            $this->load->library( 'upload', $config );

            if( $_FILES['image1']['name'] ) {
                $this->upload->do_upload( 'image1' );
                $data['image1'] = $this->upload->data();
            } else {
                $data['image1'] = $existing_content->image1;
            }

            if( $_FILES['image2']['name'] ) {
                $this->upload->do_upload( 'image2' );
                $data['image2'] = $this->upload->data();
            } else {
                $data['image2'] = $existing_content->image2;
            }

        }

        if( $page_name == 'booth_design' ) {

            $existing_content_json = $this->pages_model->get_content( 'booth_design' );
            $existing_content = json_decode( $existing_content_json->content );

            $config['upload_path']      = './uploads/';
            $config['allowed_types']    = 'jpg|jpeg';
            $config['encrypt_name']     = true;

            $this->load->library( 'upload', $config );

            if( $_FILES['image']['name'] ) {
                $this->upload->do_upload( 'image' );
                $data['image'] = $this->upload->data();
            } else {
                $data['image'] = $existing_content->image;
            }

        }

        unset( $data['_wysihtml5_mode'] );
        unset( $data['page_name'] );

		$data_to_post = json_encode( $data );

		$res = $this->pages_model->update_content( $data_to_post, $page_name );
		if( $res == TRUE ) {
			redirect( base_url( 'pages/' . $page_name ) );
		} else {
			show_error( 'Something went wrong, Information cannot be saved.' );
		}

	}

}
