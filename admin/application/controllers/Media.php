<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller
{

    function __construct() {
        parent::__construct();
        session_start();
        $this->load->model('media_model');
        $this->load->model('gallery_model');

        if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('user') );
		}

    }

    public function index()
    {

        $data = array();

        $archive_year = $this->input->get('year');

        if($archive_year){
            $year =  $archive_year;
        }else{
            $year = date("Y");
        }

        $users = $this->media_model->get_all_users($year);

        if( $users !== false )
        {
            foreach ( $users as $user )
            {
                $data['users'][] = array( 'info' => $user, 'catalogue' => $this->media_model->get_catalogue_status($user->id) );
            }
        }

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('media',$data);
        $this->load->view('templates/footer');

    }

    public function view()
    {

        $id = $this->input->get('id');
        $year = $this->input->get('year');
        
        $data = array();
        
        $exhibit_year = $this->media_model->get_gallery_exhibit_years( $id);
        
        if ($year) {
            $year = $year;
        } else {
            $year = $exhibit_year[0]->exhibit_year;
        }
        
        $data['exhibit_year'] = $exhibit_year;
        
        

        if( isset( $id ) && !empty( $id ) )
        {

            $files = $this->media_model->get_all_files_by_id( $id, $year);

            $data['doc_view'] = true;

            if ( !empty( $files ) && $files !== false )
            {
                foreach ( $files as $file )
                {
                    $data['files'][] = $file;
                }
            }

            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('media',$data);
            $this->load->view('templates/footer');

        }
        else
        {
            redirect(site_url().'media');
        }

    }

    public function edit()
    {

        $id   = $this->input->get('id');
        $view = $this->input->get('v');
        $user_id   = $this->input->get('gid');

        $data = array();
        
        $exhibit_year = $this->media_model->get_gallery_exhibit_years( $user_id);
        
        $data['exhibit_year'] = $exhibit_year;
        

        if( isset( $id ) && !empty( $id ) )
        {

            if( $view == 'f' ) {

                $data['mediums']          = $this->media_model->get_all_mediums();
                $data['mediums_selected'] = $this->media_model->get_file_mediums( $id );

            } else {
                $data['gallery']  = $this->gallery_model->get_gallery_profile($user_id);
                $data['types']    = $this->gallery_model->get_gallery_types();
                $data['info']     = $this->media_model->get_image_info($id);

            }

            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('media_edit',$data);
            $this->load->view('templates/footer');

        }
        else
        {
            redirect(site_url().'media');
        }

    }

    public function update()
    {

        $id = $this->input->post('id');
        $gid = $this->input->post('gid');
        $view = $this->input->post('view');

        if( $view == 'f' ) {

            $mediums = $this->input->post('medium');

            $this->media_model->clear_file_medium( $id );

            foreach( $mediums as $medium ) {
                $this->media_model->update_file_medium( $id, $medium );
            }

        } elseif( $view == 'c' ) {

            $data = $this->input->post();

            $this->media_model->update_file_caption( $id, $data );

        }

        redirect( site_url().'Media/view?id='.$gid );

    }

}
