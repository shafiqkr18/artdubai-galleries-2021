<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('menu_model');
		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('user') );
		}
	}

	public function index()
	{

		$data['data'] = $this->menu_model->get_list_menu();

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('menu', $data); 
		$this->load->view('templates/footer');

	}

	public function update()
	{

		$data = $this->input->post();
	
		foreach($data['menu'] as $menuId => $value):
			$res = $this->menu_model->update_menu($menuId,  $value );
		endforeach;
		
		if( $res !== false )
		{
			redirect( base_url( 'menu' ) );
		}
		exit();

		return false;

	}

}
