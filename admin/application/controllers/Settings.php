<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('settings_model');
		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('user') );
		}
	}

	public function index()
	{

		$data['data'] = $this->settings_model->read();

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('settings', $data);
		$this->load->view('templates/footer');

	}

	public function update()
	{

		$data = $this->input->post();

		unset($data['_wysihtml5_mode']);

		$res = $this->settings_model->update( $data );

		if( $res !== false )
		{
			redirect( base_url( 'settings' ) );
		}

		return false;

	}

}
