<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entries extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('registers_model');
		$this->load->model('entries_model');
		$this->load->model('authentication_model');
		$this->load->model('settings_model');
		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('admin/user') );
		}
	}

	public function index()
	{   
        $users = $this->registers_model->get_all_registered_users();

        $counter = 1;

        $data = array();

		foreach ( $users as $user )
		{

			$user_id = $user['id'];

			$application = $this->entries_model->get_application_by_user_id( $user_id );

			if( $application !== false )
			{

				$gp = $this->entries_model->get_gallery_profile( $user_id );
				$lc = $this->entries_model->get_locations( $user_id );
				$gt = $this->entries_model->get_gallery_team( $user_id );
				$pt = $this->entries_model->get_participation( $user_id );
				$ar = $this->entries_model->get_artists( $user_id );

				$data['applications'][$counter]['us'] = $user_id;
				$data['applications'][$counter]['us_full_name'] = $user['fullname'];
				$data['applications'][$counter]['us_gallery_name'] = $user['galleryname'];

				if( $gp !== false )
				{
					$data['applications'][$counter]['gp'] = $gp;
				}

				if( $gp !== false )
				{
					$data['applications'][$counter]['lc'] = $lc;
				}

				if( $gt !== false )
				{
					$data['applications'][$counter]['gt'] = $gt;
				}

				if( $pt !== false )
				{
					$data['applications'][$counter]['pt'] = $pt;
				}

				if( $ar !== false )
				{
					$data['applications'][$counter]['ar'] = $ar;
				}

				$data['applications'][$counter]['ap'] = $application;

				$pr = $this->entries_model->get_proposal( $user_id );
				$aia = $this->entries_model->get_artist_information_a( $user_id );
				$aib = $this->entries_model->get_artist_information_b( $user_id );
				$docs = $this->entries_model->get_documents( $user_id );

				if( $pr !== false )
				{
					$data['applications'][$counter]['pr_description'] = $pr[0]['description'];
				}
				else
				{
					$data['applications'][$counter]['pr_description'] = '';
				}

				if( $aia !== false )
				{
					$data['applications'][$counter]['aia'] = $aia[0];
				}

				if( $aib !== false )
				{
					$data['applications'][$counter]['aib'] = $aib[0];
				}

				if( $docs !== false )
				{
					$data['applications'][$counter]['docs'] = $docs;
				}

				$counter++;

			}

		}

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('entries',$data);
		$this->load->view('templates/footer');
     
	}

	public function view_edit()
	{

		$user_id = $this->input->get('id');

		$verification = $this->authentication_model->verify_user_id( $user_id );

		if( $verification !== false )
		{

			$gp = $this->entries_model->get_gallery_profile( $user_id );
			$lc = $this->entries_model->get_locations( $user_id );
			$gt = $this->entries_model->get_gallery_team( $user_id );
			$pt = $this->entries_model->get_participation( $user_id );
			$ar = $this->entries_model->get_artists( $user_id );

			$data['data']['us'] = $user_id;

			if( $gp !== false )
			{
				$data['data']['gp'] = $gp;
			}

			if( $gp !== false )
			{
				$data['data']['lc'] = $lc;
			}

			if( $gt !== false )
			{
				$data['data']['gt'] = $gt;
			}

			if( $pt !== false )
			{
				$data['data']['pt'] = $pt;
			}

			if( $ar !== false )
			{
				$data['data']['ar'] = $ar;
			}

			$application = $this->entries_model->get_application_by_user_id( $user_id );

			if( $application !== false )
			{

				$pr = $this->entries_model->get_proposal( $user_id );
				$aia = $this->entries_model->get_artist_information_a( $user_id );
				$aib = $this->entries_model->get_artist_information_b( $user_id );
				$dc = $this->entries_model->get_documents( $user_id );

				$data['data']['ap'] = $application;

				if( $pr !== false )
				{
					$data['data']['pr_description'] = $pr[0]['description'];
				}
				else
				{
					$data['data']['pr_description'] = '';
				}

				if( $aia !== false )
				{
					$data['data']['aia'] = $aia[0];
				}

				if( $aib !== false )
				{
					$data['data']['aib'] = $aib[0];
				}

				if( $dc !== false )
				{
					$data['data']['docs'] = $dc;
				}

			}

			$this->load->view('templates/header');
			$this->load->view('templates/sidebar');
			$this->load->view('entries',$data);
			$this->load->view('templates/footer');

		}
		else
		{

			echo 'Wrong Application ID';

		}

	}

    public function update()
    {

        $data = $this->input->post();
        if( isset( $data ) && !empty( $data ) )
        {
            $res = $this->entries_model->update_application( $data['app_id'], $data['user_id'], $data['status'] );
            if( $res == TRUE ) {
                redirect( base_url( 'entries/view_edit?id=' . $data['user_id'] ) );
            } else {
                echo 'Application status cannot be updated.';
            }
        }
        else
        {
            show_error( 'No data is posted.' );
        }

    }

	public function delete()
	{

		$id = $this->input->get('id');
		$res = $this->entries_model->delete_application( $id );

		if( $res !== false )
		{
			redirect( base_url( 'entries' ) );
		}
		else
		{
			echo 'Wrong application ID.';
		}

	}

	public function generate_pdf()
	{

		ini_set('memory_limit', '256M');

		$data = array();

		$user_id = $this->input->post( 'user_id' );

		$pdf_name = '_ARTDUBAI_2018.pdf';

		if( !empty( $user_id ) )
		{

			$gl = $this->entries_model->get_gallery_profile($user_id);
			$sp = $this->entries_model->get_social_platforms($user_id);
			$lc = $this->entries_model->get_locations($user_id);
			$lcp = $this->entries_model->get_primary_location($user_id);
			$gt = $this->entries_model->get_gallery_team($user_id);
			$ar = $this->entries_model->get_artists($user_id);
			$pr = $this->entries_model->get_participation($user_id);

			if( $gl !== false ) {
				$data['gallery_info'] = $gl[0];
				$pdf_name = str_replace( ' ', '_', $gl[0]->name ) . $pdf_name;
			}

			if( $sp !== false ) {
				$data['links'] = json_decode( $sp[0]['links'] );
			}

			if( $lc !== false ) {
				$data['locations'] = $lc;
			}

			if( $lcp !== false ) {
				$data['primary_location'] = $lcp;
			}

			if( $gt !== false ) {
				$data['teams'] = $gt;
			}

			if( $ar !== false ) {
				$data['artists'] = $ar;
			}

			if( $pr !== false ) {
				$data['participation'] = $pr;
			}

			$application = $this->entries_model->get_application_by_user_id($user_id);

			if( $application !== false )
			{

				$ep = $this->entries_model->get_proposal($user_id);

				if( $ep !== false )
				{
					$data['app_proposal'] = $ep[0];
				}

				$aia = $this->entries_model->get_artist_information_a($user_id);

				if( $aia !== false )
				{
					$data['artist_a'] = $aia[0];
				}

				$aib = $this->entries_model->get_artist_information_b($user_id);

				if( $aib !== false )
				{
					$data['artist_b'] = $aib[0];
				}

				$dc = $this->entries_model->get_documents($user_id);

				if( $dc !== false )
				{
					$data['app_docs'] = $dc;
				}

			}

//			$this->load->view( 'pdf', $data );

			$this->load->library('html2pdf');

			$this->html2pdf->folder('./uploads/');

			$this->html2pdf->filename( $pdf_name );

			$this->html2pdf->paper('a4', 'landscape');

			$this->html2pdf->html( $this->load->view('pdf', $data, true) );

			$this->html2pdf->create('download');

		}
		else
		{

			echo 'Not allowed to access this page.';

		}

	}

	public function generate_tnc_pdf()
	{

		$data = array();

		$user_id = $this->input->post( 'user_id' );

		if( !empty( $user_id ) )
		{

			$data['user'] = $this->registers_model->get_user_by_id($user_id);

			$data['application'] = $this->entries_model->get_application_by_user_id($user_id);

			$data['settings'] = $this->settings_model->read();

			//$this->load->view( 'pdf_tnc', $data );

			$this->load->library('html2pdf');

			$this->html2pdf->folder('./uploads/');

			$this->html2pdf->filename( 'Resident_2018_TNC.pdf' );

			$this->html2pdf->paper('a4', 'landscape');

			$this->html2pdf->html( $this->load->view('pdf_tnc', $data, true) );

			$this->html2pdf->create('download');

		}
		else
		{

			echo 'Not allowed to access this page.';

		}

	}

}
