<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registers extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('registers_model');
	}

	public function index()
	{

		$users = $this->registers_model->get_all_registered_users();

		$formatted_users = [];

		foreach($users as $user_data)
		{
			$formatted_users[(int)$user_data['id']] = $user_data;

		}

		$data['users'] = $formatted_users;
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('registers',$data);
		$this->load->view('templates/footer');

	}

	public function delete_user()
	{

		$id = $this->input->get('id');

		if( $id == $_SESSION['user_id'] )
		{

			echo 'User cannot be deleted.';

		}
		else
		{

			$this->registers_model->delete_user($id);
			redirect( base_url( 'registers' ) );

		}

	}

}