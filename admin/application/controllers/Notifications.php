<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller
{

    function __construct() {
        parent::__construct();
        session_start();

        $this->load->model('notifications_model');

        if( !isset( $_SESSION['user_id'] ) )
        {
            redirect( base_url('admin') );
        }

    }

    /* Web */

    public function index()
    { 

        $data['user_id'] = $_SESSION['user_id'];
        $data['users'] = $this->notifications_model->get_all_users();
        $data['notifications'] = $this->notifications_model->get_all_notifications();

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('notifications', $data);
        $this->load->view('templates/footer');
     
    }

    public function create()
    {

        $posted_data = $this->input->post();

        $notification_id = rand(3,3);

        if( $posted_data['type'] == '1' )
        {
            $users = $this->notifications_model->get_all_users();
            foreach ( $users as $user )
            {
                $this->notifications_model->add_to_db( $notification_id, $user->id, $posted_data );
            }
        }
        else
        {
            if( isset( $posted_data['users'] ) )
            {
                foreach ( $posted_data['users'] as $user )
                {
                    $this->notifications_model->add_to_db( $notification_id, $user, $posted_data );
                }
            }
            else
            {
                echo 'Sorry no user is selected!';
            }
        }

        redirect( '/notifications' );

    }

    public function delete()
    {

        $id = $this->input->get( 'id' );

        $this->notifications_model->remove_from_db( $id );

        redirect( '/notifications' );

    }

    /* Email */

    public function email()
    {

        $data['user_id'] = $_SESSION['user_id'];
        $data['users'] = $this->notifications_model->get_all_users();

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('notifications-email', $data);
        $this->load->view('templates/footer');

    }

    public function create_email()
    {

        $posted_data = $this->input->post();
        $contactType = $this->input->post('contact_type');
        $attachment  = '';

        $config['upload_path']      = './uploads/notification-email-attachment/';
        $config['allowed_types']    = 'jpg|jpeg|pdf|docx|doc|docs|png|ppt';
        $config['max_size']         = 20000;

        $this->load->library( 'upload', $config );
        $this->load->helper('url');


        if ( ! $this->upload->do_upload( 'file' )) {
            $attachemnt_error =  json_encode( array( 'error' => $this->upload->display_errors() ) );
        }
        else
        {

            $upload_data = $this->upload->data();
            $file_name   = $upload_data['file_name'];
            $attachment  = site_url("/uploads/notification-email-attachment/".$file_name);
        }

        $notification_id = rand(3,3);

        if( $posted_data['type'] == '1' ) {
            $users = $this->notifications_model->get_all_users();
            foreach ( $users as $user )
            {
                $email = $user->email;

                if(!empty($email)):
                    //echo $email . '-------------- <br />';
                    // Send email to actual gallery account
                    $this->send_email($email, $posted_data['title'], $posted_data['message'], $attachment);
                endif;
                if(!empty($contactType)):
                    $contactlist    = $this->notifications_model->get_user_contacts_by_id($user->id, $contactType);
                    if($contactlist):
                        foreach ($contactlist as $contact ):
                            if($email != $contact->email):
                                //echo $contact->email . '<br />';
                                // Send email to contacts (Main Account, CC's , Others) of gallery account
                                $this->send_email($contact->email, $posted_data['title'], $posted_data['message'], $attachment);
                            endif;
                        endforeach;
                    endif;
                endif;
            }
            $response = 1;
        } else if( $posted_data['type'] == '2' )  {

            if( isset( $posted_data['users'] ) ) {
                foreach ( $posted_data['users'] as $userId )
                {

                    $userInfo   = $this->notifications_model->get_user_by_id($userId);
                    $email      = $userInfo->email;

                    if(!empty($email)):
                        //echo $email . '-------------- <br />';
                        // Send email to actual gallery account
                        $this->send_email($email, $posted_data['title'], $posted_data['message'], $attachment);
                    endif;

                    if(!empty($contactType)):
                        $contactlist    = $this->notifications_model->get_user_contacts_by_id($userInfo->id, $contactType);
                        if($contactlist):
                            foreach ($contactlist as $contact ):
                                if($email != $contact->email):
                                    //echo $contact->email . '<br />';
                                    // Send email to contacts (Main Account, CC's , Others) of gallery account
                                    $this->send_email($contact->email, $posted_data['title'], $posted_data['message'], $attachment);
                                endif;
                            endforeach;
                        endif;
                    endif;
                }
                $response =  2;
            } else {
                $response =  3;
            }
        }

        $this->load->helper('url');

        redirect('email-notifications?response='.$response);

    }

    public function send_email( $toEmail, $emailsubject, $message, $attachment ){

        if( !empty( $toEmail ) )
        {

            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'davidsoncolaw@gmail.com',
                'smtp_pass' => 'Umair@Zoom',
                'mailtype'  => 'html',
                'smtp_crypto' => 'ssl'
            );
            $this->load->library('email');
            $this->load->helper('url');
            $this->load->helper('string');

            $key = random_string('alnum', 16);

            $html =  $message;

            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from('umair@wewantzoom.com', 'Art Dubai');
            $this->email->to($toEmail);
            $this->email->subject($emailsubject);
            $this->email->set_mailtype('html');
            $this->email->message( $html );
            if(!empty($attachment)):
                $this->email->attach($attachment);
            endif;
            if($this->email->send()) {
                $res = json_encode( array( 'ok' => 'Email has been sent to all selected users' ) );
            } else {
                $res =  json_encode( array( 'error' => 'There is an issue while sending email.' ) );
            }

        } else {
            $res =  json_encode( array( 'error' => 'To email is required!' ) );
        }

        return $res;

    }



}