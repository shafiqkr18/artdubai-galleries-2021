<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vip extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('vip_model');
		
		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('user') );
		}
	}

	public function index()
	{

		$data = array();

        $entries = $this->vip_model->get_entries();

		foreach ( $entries as $entry )
        {

            $gallery_name = $this->vip_model->get_gallery_name( $entry->user_id );
            $data['entries'][] = array( 'gallery_name' => $gallery_name, 'entry' => $entry );

        }

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('vip_designations', $data);
		$this->load->view('templates/footer');

	}

	public function view()
	{

		$data = array();

		$id = $this->input->get('id');

		$data['view'] = 'single';
		$entry = $this->vip_model->get_entry( $id );
		$data['entry'] = $entry[0];

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('vip_designations', $data);
		$this->load->view('templates/footer');

	}

    public function delete()
    {

        $id = $this->input->get('id');

        $res = $this->vip_model->delete_entry( $id );

        if( $res == TRUE ) {
            redirect( base_url( 'vip' ) );
        } else {
            show_error( 'Something went wrong, Entry cannot be deleted.' );
        }

    }

}