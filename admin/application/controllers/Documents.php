<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();

		$this->load->model('documents_model');

		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('admin') );
		}

	}

	public function index()
	{

		$data = array();
		$data['documents'] = $this->documents_model->get_all_files();

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('documents', $data);
		$this->load->view('templates/footer');
     
	}

    public function delete_document()
    {
       

        $id = $this->input->get('id');
        $document = $this->documents_model->get_document_by_id( $id );

        $file = json_decode( $document->file );

        $path_to_file = $_SERVER["DOCUMENT_ROOT"] . '/admin/uploads/documents/' . $file->file_name;
        unlink($path_to_file);

        $delete = $this->documents_model->delete_document( $id );

        if( $delete !== false )
        {
            redirect( base_url( 'documents' ) );
        }
        else
        {
            echo 'Wrong application ID.';
        }
       
     
    }

    public function upload()
    {

        $config['upload_path']      = './uploads/documents/';
        $config['allowed_types']    = 'jpg|jpeg|pdf|docx|xlsx|docs|png|ppt';
        $config['max_size']         = 20000;

        $data = $this->input->post();

        $this->load->library( 'upload', $config );

        if( !empty( $data ) )
        {
            if ( ! $this->upload->do_upload( 'file' ))
            {
                echo json_encode( array( 'error' => $this->upload->display_errors() ) );
            }
            else
            {

                $user_id = $_SESSION['user_id'];

                $upload_data = $this->upload->data();

                $data['file'] = json_encode( $upload_data );

                $this->documents_model->set_document( $user_id, $data );

                redirect( 'documents' );

            }
        }
        else
        {
            show_error( 'Not allowed.', 405 );
        }

    }

}
