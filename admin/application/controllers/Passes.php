<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passes extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('passes_model');
	}

	public function index()
	{

		$data = array();

		$passes = $this->passes_model->get_passes();

        if( isset( $passes ) && !empty( $passes ) )
        {
            foreach ( $passes as $pass )
            {
                $gallery_name = $this->passes_model->get_gallery_name( $pass->user_id );
                if( $pass->type == 'artist' )
                {
                    $person = $this->passes_model->get_a_artist( $pass->aid );
                }
                else
                {
                    $person = $this->passes_model->get_a_staff( $pass->aid );
                }
                $data['passes'][] = array( 'id' => $pass->id, 'gallery_name' => $gallery_name, 'person' => $person, 'type' => $pass->type, 'thumb' => $pass->thumb );
            }
        }
        else
        {
            $data['passes'] = array();
        }

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('passes', $data);
		$this->load->view('templates/footer');

	}

}