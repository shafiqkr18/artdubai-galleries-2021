<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('dashboard_model');
		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('user') );
		}
	}

	public function index()
	{

		$data = array();
		$total_users = $this->dashboard_model->get_total_users();
		$total_gallery_submissions = $this->dashboard_model->get_total_gallery_submissions();

		$data['total_users'] = $total_users;
		$data['total_gallery'] = $total_gallery_submissions;
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('dashboard', $data);
		$this->load->view('templates/footer');

	}

}
