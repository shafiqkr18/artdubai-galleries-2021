<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();

		$this->load->model('gallery_model');

		if( !isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('admin') );
		}

	}

    
	public function index()
	{

		$data = array();
		$counter = 1;
		$users = $this->gallery_model->get_all_users();
		$data['title'] = 'Gallery Profiles';
		foreach ( $users as $user )
		{
			$info = $this->gallery_model->get_gallery_profile( $user->id );
			if( !empty( $info ) )
			{
				$data['galleries'][$counter]['user'] = $user;
				$data['galleries'][$counter]['info'] = $info[0];
				$counter++;
			}
		}

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('gallery', $data);
		$this->load->view('templates/footer');

	}

	public function view()
	{

		$user_id = $this->input->get('id');

		$gp = $this->gallery_model->get_gallery_profile( $user_id );
		$lc = $this->gallery_model->get_locations( $user_id );
		$gt = $this->gallery_model->get_gallery_team( $user_id );
		$pt = $this->gallery_model->get_participation( $user_id );
		$ar = $this->gallery_model->get_artists( $user_id );
		$ae = $this->gallery_model->get_exhibiting_artists( $user_id );

		$data['data']['us'] = $user_id;

		if( $gp !== false )
		{
			$data['data']['gp'] = $gp;
		}

		if( $gp !== false )
		{
			$data['data']['lc'] = $lc;
		}

		if( $gt !== false )
		{
			$data['data']['gt'] = $gt;
		}

		if( $pt !== false )
		{
			$data['data']['pt'] = $pt;
		}

		if( $ar !== false )
		{
			$data['data']['ar'] = $ar;
		}

        if( $ae !== false )
        {
            $data['data']['ae'] = json_decode( $ae[0]['artists'] );
        }

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('gallery_single',$data);
		$this->load->view('templates/footer');

	}

    public function edit()
    {

        $all_modules = array( 'info', 'social', 'location', 'staff', 'artist', 'participation' );

        $section = $this->input->get('s');
        $id = $this->input->get('i');
        $user_id = $this->input->get('u');

        if( in_array( $section, $all_modules ) )
        {

            $data['section'] = $section;
            $data['gp'] = $this->gallery_model->edit_gallery_profile( $user_id, $id );
            $data['tp'] = $this->gallery_model->get_gallery_types();
            $data['sp'] = $this->gallery_model->edit_social_platforms( $user_id, $id );
            $data['lc'] = $this->gallery_model->edit_locations( $user_id, $id );
            $data['gt'] = $this->gallery_model->edit_gallery_team( $user_id, $id );
            $data['ar'] = $this->gallery_model->edit_artists( $user_id, $id );
            $ae = $this->gallery_model->get_exhibiting_artists( $user_id );
            $data['ae'] = json_decode( $ae[0]['artists'] );
            $data['pr'] = $this->gallery_model->edit_participation( $user_id, $id );

            $this->load->view('templates/header' );
            $this->load->view('templates/sidebar' );
            $this->load->view('gallery_edit', $data);
            $this->load->view('templates/footer');

        }
        else
        {

            show_error( 'Something went wrong!' );

        }

    }

    public function update()
    {

        $posted_data = $this->input->post();

        if( !empty( $posted_data ) )
        {

            $section = $posted_data['section'];

            if( $section == 'info' ) {

                $posted_data['booth1'] = implode("|", $posted_data['type1']);
                $posted_data['booth2'] = (!empty( $posted_data['type2'] )|| $posted_data['type2'] != '|') ? implode("|", $posted_data['type2']) : '' ;

                // $posted_data['booth1_m2'] = $posted_data['b1_m2'];
                // $posted_data['booth2_m2'] = $posted_data['b2_m2'];

                /**/
                $config['upload_path']          = '../uploads/booth-location/';
                $config['allowed_types']        = 'gif|jpg|png|pdf';
                $config['max_size']             = 4000;



                if($_FILES['b1_allocation']['error'] == 0){
                    $this->load->library('upload', $config);
                    $b1_image = $this->upload->do_upload('b1_allocation');
                    $b1_image = $this->upload->data();
                    $posted_data['booth1_allocation'] = json_encode($b1_image);
                }else{
                    unset( $_FILES['b1_allocation'] );
                }

                if($_FILES['b2_allocation']['error'] == 0){
                    $this->load->library('upload', $config);
                    $b2_image = $this->upload->do_upload('b2_allocation');
                    $b2_image = $this->upload->data();
                    $posted_data['booth2_allocation'] = json_encode($b2_image);
                }else if ( $posted_data['booth2'] == '' || $posted_data['booth2'] == '|'){
                     $posted_data['booth2_allocation'] = '';
                }else{
                    unset( $_FILES['b2_allocation'] );
                }


                // print_r($posted_data);
                //print_r($_FILES);
                $user_id = $posted_data['userid'];
                unset( $posted_data['userid'] );

                $res = $this->gallery_model->update_gallery_profile( $posted_data['id'], $posted_data );
                redirect( 'gallery/view?id='.$user_id );
            } elseif( $section == 'social' ) {
                $res = $this->gallery_model->update_social_platforms( $posted_data['id'], $posted_data );
                redirect( 'gallery' );
            } elseif( $section == 'location' ) {
                $res = $this->gallery_model->update_locations( $posted_data['id'], $posted_data );
                redirect( 'gallery' );
            } elseif( $section == 'staff' ) {
                $res = $this->gallery_model->update_gallery_team( $posted_data['id'], $posted_data );
                redirect( 'gallery' );
            } elseif( $section == 'artist' ) {
                $res = $this->gallery_model->update_artists( $posted_data['id'], $posted_data );
                $ae_json = $this->gallery_model->get_exhibiting_artists( $posted_data['userid'] );
                $ae_array = json_decode( $ae_json[0]['artists'], true );
                if( isset( $posted_data['exhibit_flag'] ) )
                {
                    if( !in_array( $posted_data['id'], $ae_array ) )
                    {
                        array_push( $ae_array, $posted_data['id'] );
                        $ae_new_json = json_encode( array_values($ae_array) );
                        $res3 = $this->gallery_model->update_exhibiting_artists( $posted_data['userid'], $ae_new_json );
                    }
                }
                else
                {
                    if( in_array( $posted_data['id'], $ae_array ) )
                    {
                        $ae_new_array = array_diff( $ae_array, array( $posted_data['id'] ) );
                        $ae_new_json = json_encode( array_values($ae_new_array) );
                        $res3 = $this->gallery_model->update_exhibiting_artists( $posted_data['userid'], $ae_new_json );
                    }
                }
                redirect( 'gallery/view?id='.$posted_data['userid'] );
            } elseif( $section == 'participation' ) {
                $res = $this->gallery_model->update_participation( $posted_data['id'], $posted_data );
                redirect( 'gallery' );
            } else {
                show_error( 'Something went wrong!' );
            }

        }
        else
        {

            show_error( 'Something went wrong!' );

        }

    }

    public function add()
    {

        $all_modules = array( 'info', 'social', 'location', 'staff', 'artist', 'participation' );

        $section = $this->input->get('s');
        $user_id = $this->input->get('u');

        if( in_array( $section, $all_modules ) )
        {

            $data['section'] = $section;
            $data['user_id'] = $user_id;

            $this->load->view('templates/header' );
            $this->load->view('templates/sidebar' );
            $this->load->view('gallery_create', $data);
            $this->load->view('templates/footer');

        }
        else
        {

            show_error( 'Something went wrong!' );

        }

    }

    public function create()
    {

        $posted_data = $this->input->post();

        if( !empty( $posted_data ) )
        {

            $section = $posted_data['section'];

            if( $section == 'artist' ) {
                unset( $posted_data['section'] );
                $res = $this->gallery_model->create_artists( $posted_data );
                if( !empty( $res ) && isset( $posted_data['exhibit_flag'] ) ) {
                    $ae_json = $this->gallery_model->get_exhibiting_artists( $posted_data['user_id'] );
                    $ae_array = json_decode( $ae_json[0]['artists'], true );
                    array_push( $ae_array, $res );
                    $ae_new_json = json_encode( array_values($ae_array) );
                    $res3 = $this->gallery_model->update_exhibiting_artists( $posted_data['user_id'], $ae_new_json );
                }
                redirect( 'gallery/view?id='.$posted_data['user_id'] );
            } else {
                show_error( 'Something went wrong!' );
            }

        }
        else
        {

            show_error( 'Something went wrong!' );

        }

    }

	public function delete_gallery()
	{

		$id = $this->input->get('id');
		$res = $this->gallery_model->delete_gallery( $id );
		if( $res !== false )
		{
			redirect( base_url( 'gallery' ) );
		}
		else
		{
			echo 'Wrong application ID.';
		}

	}

    public function delete_artist()
    {

        $id = $this->input->get('i');
        $user_id = $this->input->get('u');
        $res = $this->gallery_model->delete_artist( $id );
        if( $res !== false )
        {
            $ae_json = $this->gallery_model->get_exhibiting_artists( $user_id );
            $ae_array = json_decode( $ae_json[0]['artists'], true );
            if( in_array( $id, $ae_array ) )
            {
                $ae_new_array = array_diff( $ae_array, array( $id ) );
                $ae_new_json = json_encode( array_values($ae_new_array) );
                $res3 = $this->gallery_model->update_exhibiting_artists( $user_id, $ae_new_json );
            }
            redirect( base_url( 'gallery/view?id='.$user_id ) );
        }
        else
        {
            echo 'Wrong application ID.';
        }

    }

	public function volunteer_requests()
	{

		$data['view'] = 'volunteer';
		$data['title'] = 'Volunteer Requests';
		$users = $this->gallery_model->get_all_users();
		foreach ( $users as $user )
		{
			$status = $this->gallery_model->get_volunteer_request( $user->id );
			if( $status == true )
			{
				$data['requests'][] = $user;
			}
		}

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('gallery', $data);
		$this->load->view('templates/footer');

	}

	public function travel_requests()
	{

		$data['view'] = 'travel';
		$data['title'] = 'Travel/Accommodation Requests';
		$users = $this->gallery_model->get_all_users();
		foreach ( $users as $user )
		{
			$status = $this->gallery_model->get_travel_request( $user->id );
			if( $status == true )
			{
				$data['requests'][] = $user;
			}
		}

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('gallery', $data);
		$this->load->view('templates/footer');

	}

    public function gallery_fascia()
    {

        $data['view'] = 'fascia';
        $data['title'] = 'Gallery Fascia';

        $users = $this->gallery_model->get_all_users();
        foreach ( $users as $user )
        {
            $fascia = $this->gallery_model->get_gallery_fascia( $user->id );
            if( $fascia == true )
            {
                $data['fascias'][] = array( 'data' => $fascia, 'gallery' => $user->galleryname );
            }
        }

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('gallery', $data);
        $this->load->view('templates/footer');

    }

    public function gallery_communication()
    {

        $data['view'] = 'gallery-communication';
        $data['title'] = 'Gallery Communication';

        $users = $this->gallery_model->get_all_users();
        foreach ( $users as $user )
        {
            $gallerycommunication = $this->gallery_model->get_gallery_communication( $user->id );
            if( $gallerycommunication == true )
            {
                $data['gallerycommunication'][] = array( 'data' => $gallerycommunication, 'gallery' => $user->galleryname );
            }
        }

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('gallery', $data);
        $this->load->view('templates/footer');

    }

	public function users()
	{

		$data['users'] = $this->gallery_model->get_all_users();

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('gallery_users', $data);
		$this->load->view('templates/footer');

	}

	public function view_user()
	{

		$user_id = $this->input->get('id');

		$data['user'] = $this->gallery_model->get_user_by_id( $user_id );

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('gallery_users_single',$data);
		$this->load->view('templates/footer');

	}

	public function update_user()
	{

		$data = $this->input->post();

		if( !empty( $data ) )
		{

			if( empty( $data['password'] ) )
			{

				$this->gallery_model->update_user( $data, FALSE );
				redirect( base_url( 'gallery/users' ) );
			}
			elseif( $data['password'] == $data['confirm_password'] )
			{
				$this->gallery_model->update_user( $data, TRUE );
				redirect( base_url( 'gallery/users' ) );
			}
			else
			{
				show_error( 'Sorry! passwords do not match.' );
			}

		}

	}

	public function delete_user()
	{

		$id = $this->input->get('id');

		if( $id == $_SESSION['user_id'] )
		{

			echo 'User cannot be deleted.';

		}
		else
		{

			$this->gallery_model->delete_user($id);
			redirect( base_url( 'gallery/users' ) );

		}

	}

	public function update_booth()
	{

		$id = $this->input->post('id');
		$value = $this->input->post('value');

		if(!empty($id) && !empty($value)){

			$this->gallery_model->update_gallery_fascia_booth($id, $value);
			$result = array('ok' => 'Successfully updated booth name for selected gallery.');

		}else if(!empty($id) && empty($value)){
		    $this->gallery_model->update_gallery_fascia_booth($id, 'Booth');
		    $result = array('ok' => 'Empty booth name is saved to selected gallery.');
		}else{
		    $result = array('error' => 'Error!');
		}
		echo json_encode($result);

	}

}
