<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('admins_model');
	}

	public function index()
	{

		$data = array();

		$users = $this->admins_model->get_all_users();

		if( $users !== false )
		{
			$data['users'] = $users;
		}

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('admins',$data);
		$this->load->view('templates/footer');

	}

}