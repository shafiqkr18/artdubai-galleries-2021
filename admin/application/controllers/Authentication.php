<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('authentication_model');
	}

	public function index()
	{

		if( isset( $_SESSION['user_id'] ) )
		{
			redirect( base_url('dashboard') );
		}

		$data = array();

		if( !empty( $_GET['key'] ) && isset( $_GET['key'] ) ) {
			$res = $this->authentication_model->verify_key($_GET['id'],$_GET['key']);
			if( $res !== false ) {
				$data['form'] = 'reset';
				$data['email'] = $_GET['id'];
				$data['key'] = $_GET['key'];
			}
		}

		$this->load->view('templates/header');
		$this->load->view('login',$data);
		$this->load->view('templates/footer');

	}

	public function login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$res = $this->authentication_model->verify_user($email,$password);
		if( $res !== false )
		{
			$_SESSION['user_id'] = $res->id;
			echo json_encode( array( 'ok' => 'logged in' ) );
		}
		else
		{
			echo json_encode( array( 'error' => 'Check your email and password.'.$res ) );
		}
	}

	public function forget_password()
	{
		$email = $this->input->post('email');
		$res = $this->authentication_model->verify_email($email);
		if( $res !== false )
		{
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'davidsoncolaw@gmail.com',
				'smtp_pass' => 'Umair@Zoom',
				'mailtype'  => 'html',
				'smtp_crypto' => 'ssl'
			);
			$this->load->library('email');
			$this->load->helper('url');
			$this->load->helper('string');

			$key = random_string('alnum', 16);

			$this->authentication_model->create_key($email,$key);

			$html = 'Click on the link below to reset your password. '.anchor( site_url('?key='.$key.'&id='.$email), 'Reset Password', array( 'target' => '_blank' ) );

			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from('umair@wewantzoom.com', 'Art Dubai');
			$this->email->to($email);
			$this->email->subject('Password Reset Request');
			$this->email->message( $html );

			$this->email->send();

			echo json_encode( array( 'ok' => 'An email has been sent with a link to reset your password.' ) );

		}
		else
		{
			echo json_encode( array( 'error' => 'No user found with this email address.' ) );
		}
	}

	public function logout()
	{
		session_destroy();
		redirect( base_url() );
	}

	public function reset_password(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$key = $this->input->post('key');
		$res1 = $this->authentication_model->verify_key($email,$key);
		if( $res1 !== false ) {
			$res2 = $this->authentication_model->update_password($email,$password,$key);
			if( $res2 !== false ) {
				echo json_encode( array( 'ok' => 'Password changed successfully.' ) );
			}
			else
			{
				echo json_encode( array( 'error' => 'Sorry! Password cannot be changed.' ) );
			}
		} else {
			echo 'Access not allowed!';
		}
	}

}