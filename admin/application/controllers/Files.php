<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends CI_Controller
{

	function __construct() {
		parent::__construct();
		session_start();
		$this->load->model('files_model');
	}

	public function index()
	{

		ini_set("upload_max_filesize","10M");
		ini_set("post_max_size","10M");

		$data = array();

		$data['files'] = $this->files_model->get_files();

		if( !empty( $_FILES ) )
		{

			$config['upload_path']      = './uploads/';
			$config['allowed_types']    = 'pdf';
			$config['max_size']         = 100000;

			$type = $this->input->post('filetype');

			$this->load->library('upload');

			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data['error'] = $this->upload->display_errors();
			}
			else
			{
				$uploaded_file = $this->upload->data();
				$this->files_model->set_file( $uploaded_file['orig_name'], $type );
				$data['ok'] = 'Image uploaded successfully!';
				redirect( 'files' );
			}

		}

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('files',$data);
		$this->load->view('templates/footer');

	}

	public function delete()
	{

		if( isset( $_GET['id'] ) && !empty( $_GET['id'] ) )
		{
			$file = $this->files_model->get_file_by_id( $_GET['id'] );
			if( $file !== false )
			{
				$path = FCPATH.'uploads/';
				$this->files_model->delete_file( $_GET['id'] );
				unlink( $path.$file->name );
				redirect( 'files' );
			}
			else
			{
				echo 'No such file exist.';
			}
		}
		else
		{
			echo 'Not allowed to access the site.';
		}

	}

}