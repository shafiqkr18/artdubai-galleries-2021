<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_event_year'))
{
    function get_event_year(){
        $CI=& get_instance();
        
        $CI->load->model('pages_model');
	    $content = $CI->pages_model->get_content( 'schedule' );
	    
	    if($content){
	        $data = json_decode( $content[0]->content );
	        
	        if($data->event_year){
	           return $data->event_year;
	       }else{
	          return false; 
	       }
	    }
		
    }
}

if ( ! function_exists('upload_dir') )
{

	function upload_dir( $slug = '' ) {
        $url = 'https://galleries.artdubai.ae/uploads/';
		return $url . $slug;
	}

}

if ( ! function_exists('upload_dir_admin') )
{

    function upload_dir_admin( $slug = '' ) {
        $url = 'https://galleries.artdubai.ae/admin/uploads/';
        return $url . $slug;
    }

}

if ( ! function_exists('local_date') )
{

    function local_date( $timestamp, $format = NULL ) {
        $tz = 'Asia/Dubai';
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp($timestamp);
        if( $format == NULL )
        {
            $format = 'd/m/y H:i:s';
        }
        return $dt->format( $format );
    }

}