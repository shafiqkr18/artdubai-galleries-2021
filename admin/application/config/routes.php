<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Authentication';
$route['forget-password'] = 'Authentication/forget_password';
$route['reset-password'] = 'Authentication/reset_password';
$route['logout'] = 'Authentication/logout';
$route['login'] = 'Authentication/login';
$route['email-notifications'] = 'Notifications/email';
$route['admins'] = 'Admins';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;