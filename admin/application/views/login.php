<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="login-content card">
                    <div class="login-form">

                        <h4><img height="20" src="<?php echo base_url() ?>assets/images/logo-text.png" alt="Login"></h4>

                        <?php if( isset( $form ) && ( $form == 'reset' ) ) : ?>

                            <form id="admin-reset-password" method="post" action="<?php base_url() ?>reset-password">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" name="cpassword" class="form-control" placeholder="Confirm Password" required>
                                </div>
                                <input type="hidden" name="email" value="<?php echo $email; ?>">
                                <input type="hidden" name="key" value="<?php echo $key; ?>">
                                <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Update</button>
                            </form>

                        <?php else: ?>

                            <form id="admin-login" method="post" action="<?php base_url() ?>login">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                                </div>
                                <div class="checkbox">
                                    <label class="pull-right">
                                        <a class="forget-password-link" href="#">Forgotten Password?</a>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Sign in</button>
                            </form>
                            <form id="admin-forget-password" class="hidden" method="post" action="<?php base_url() ?>forget-password">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="checkbox">
                                    <label class="pull-right">
                                        <a href="#" class="signin-link">Back to Sign In</a>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Submit</button>
                            </form>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>