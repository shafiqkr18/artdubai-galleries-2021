<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Notifications</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <form id="settings-form" action="<?php echo base_url() ?>notifications/create" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select a User</label><br>
                                        <input class="radio-notification" type="radio" name="type" value="1" checked><span style="display: inline-block;margin: 0 10px;">All gallery users</span>
                                        <input class="radio-notification" type="radio" name="type" value="2"><span style="display: inline-block;margin: 0 10px;">Selected gallery users</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="users[]" id="user_selection" class="multipleSelect" multiple style="display: none;" >
                                        <!-- <select name="users[]" id="user_selection" style="height: 100px;" required class="form-control" disabled multiple> -->
                                            <?php
                                            $i = 0;
                                            foreach ( $users as $user )
                                            {
                                                if( !empty( $user->galleryname ) )
                                                {
                                                    echo '<option data-archive-user="'. $i . ' - ' . $user->archive .'" value="' . $user->id . '">' . $user->galleryname . '</option>';
                                                }
                                                else
                                                {
                                                    echo '<option data-archive-user="'. $i . ' - ' . $user->archive .'" value="' . $user->id . '">' . $user->id . '</option>';
                                                }
                                            
                                            $i++;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Add Message</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title" value="" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="message" class="form-control" placeholder="Message" style="height: 150px;" rows="20"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                    <input type="submit" class="btn btn-primary" value="Send">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Message</th>
                                <th>Receivers</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( !empty( $notifications ) )
                            {
                                foreach ( $notifications as $notification )
                                {
                                    $count = 1;
                                    $receivers = ( $notification->type == 2 ) ? 'Custom Selection' : 'All Users';
                                    echo '<tr>
                                        <td>' . $count . '</td>
                                        <td>' . $notification->title . '</td>
                                        <td>' . $notification->message . '</td>
                                        <td>' . $receivers . '</td>
                                        <td>' . $notification->date_created . '</td>
                                    </tr>';
                                    $count++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>