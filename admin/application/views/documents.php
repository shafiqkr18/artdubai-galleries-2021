<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Documents Library</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-title">
                        <h4>File Upload</h4>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo base_url( 'documents/upload' ); ?>" method="post" enctype='multipart/form-data'>
                            <div class="row">
                                <div class="col-md-5">
                                    <fieldset>
                                        <input type="text" class="form-control" name="file_name" placeholder="Enter file name" required>
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <input type="file" id="file" name="file" class="form-control">
                                    <p><small>* Max file size can be 20MB</small></p>
                                </div>
                                <div class="col-md-2">
                                    <input type="submit" class="btn btn-primary" value="Upload">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card">
                    <div class="card-title">
                        <h4>All uploaded documents</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>File Name</th>
                                    <th>File Path</th>
                                    <th>Download</th>
                                    <th>Date</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if( !empty( $documents ) ) :
                                    $counter = 1;
                                    foreach ( $documents as $document ) :
                                        $file = json_decode( $document->file );
                                        ?>
                                        <tr onclick="copyToClipboard('<?php echo 'document-'. $document->id; ?>')">
                                            <td><?php echo $counter; ?></td>
                                            <td><?php echo $document->name; ?></td>
                                            <td>
                                                <input style="width: 100%" readonly type="text" id="document-<?php echo $document->id; ?>" value="<?php echo upload_dir_admin( 'documents/' . $file->file_name ); ?>" />  
                                            </td>
                                            <td><a target="_blank" href="<?php echo upload_dir_admin( 'documents/' . $file->file_name ); ?>">View</a></td>
                                            <td><?php echo $document->date_created; ?></td>
                                            <td><a href="<?php echo base_url('documents/delete_document?id='. $document->id); ?>">Delete</a></td>
                                        </tr>
                                        <?php
                                        $counter++;
                                    endforeach;
                                else :
                                    ?>
                                    <tr>
                                        <td colspan="4" class="text-center">No document is uploaded yet.</td>
                                    </tr>
                                    <?php
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

    <script>
    function copyToClipboard(filePath) {

      var copyText = document.getElementById(filePath);
      copyText.select();
      copyText.setSelectionRange(0, 99999)
      document.execCommand("copy");
    }
    </script>


</div>