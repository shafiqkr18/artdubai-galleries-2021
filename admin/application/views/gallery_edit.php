<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Update Gallery Information</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">

                        <?php if( $section == 'info' ) : ?>

                            <form action="<?php echo base_url( 'gallery/update' ); ?>"  id="gp-update-form" class="gp-update-form row" method="post"  enctype="multipart/form-data">
                                <div class="col-md-6">
                                    <figure class="form-group">
                                        <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $gp[0]->name; ?>" required>
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <figure class="form-group">
                                        <input type="text" class="form-control" placeholder="Established" name="established" value="<?php echo $gp[0]->established; ?>" required>
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <div class="div form-group">
                                        <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $gp[0]->email; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Website" name="website" value="<?php echo $gp[0]->website; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="bio" class="form-control" rows="10" style="height: 300px;" placeholder="Biography" required><?php echo $gp[0]->bio; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Indiviual Booth Plan Link</label>
                                        <input type="text" class="form-control" placeholder="Indiviual Booth Plan" name="indiviual_booth_plan" value="<?php echo $gp[0]->indiviual_booth_plan; ?>" >
                                    </div>
                                </div>

                                <?php $booth1 = explode( '|', $gp[0]->booth1 ); ?>
                                <div class="col-md-12"><label> Booth Info 1</label></div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Booth No." name="type1[]" value="<?php echo $booth1[0]; ?>" required>
                                </div>
                                <?php
                                $b1_m2          = (!empty(  $gp[0]->booth1_m2 ) ) ?  $gp[0]->booth1_m2 : '';
                                $b1_img_link    = (!empty(  $gp[0]->booth1_img_link ) ) ?  $gp[0]->booth1_img_link : '';
                                $b1_allocation  = json_decode($gp[0]->booth1_allocation);
                                $b1_allocation  = (!empty(  $b1_allocation->file_name ) ) ?  $b1_allocation->file_name : '';
                                ?>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="M2" name="booth1_m2" value="<?php echo $b1_m2; ?>" required>
                                </div>
                                <?php /**/ ?>
                                <div class="col-md-3">
                                    <input type="file" class="form-control" placeholder="M2" name="b1_allocation" value="" >
                                    <small><?php echo $b1_allocation; ?></small>
                                </div>
                                
                                <div class="col-md-3">
                                    <select name="type1[]" id="type" class="form-control" required>
                                        <option value="">Select Gallery Type</option>
                                        <?php
                                        foreach ( $tp as $type ) :
                                            $selected = ( $type['id'] == $booth1[1] ) ? 'selected' : '';
                                            echo '<option value="'.$type['id'].'" '.$selected.'>'.$type['name'].'</option>';
                                        endforeach;
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-12 p-t-20">
                                    <input type="text" class="form-control" placeholder="Booth 1 allocation image link" name="booth1_img_link" value="<?php echo $b1_img_link; ?>" required>
                                </div>

                                <?php $booth2 = explode( '|', $gp[0]->booth2 ); ?>
                                <div class="col-md-12"><label class="p-t-20"> Booth Info 2</label></div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Booth No." name="type2[]" value="<?php echo $booth2[0]; ?>">
                                </div>
                                <?php
                                $b2_m2          = (!empty(  $gp[0]->booth2_m2 ) ) ?  $gp[0]->booth2_m2 : '';
                                $b2_img_link    = (!empty(  $gp[0]->booth2_img_link ) ) ?  $gp[0]->booth2_img_link : '';
                                $b2_allocation  = json_decode($gp[0]->booth2_allocation);
                                $b2_allocation  = (!empty(  $b2_allocation->file_name ) ) ?  $b2_allocation->file_name : '';
                                ?>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="M2" name="booth2_m2" value="<?php echo $b2_m2; ?>" >
                                </div>
                                <?php /**/ ?>
                                <div class="col-md-3">
                                    <input type="file" class="form-control" placeholder="M2" name="b2_allocation" value="" >
                                    <?php if (!empty($booth2[0])) : ?>
                                        <small><?php echo $b2_allocation; ?></small>
                                    <?php endif;?>
                                </div>
                                
                                <div class="col-md-3">
                                    <select name="type2[]" id="type" class="form-control">
                                        <option value="">Select Gallery Type</option>
                                        <?php
                                        foreach ( $tp as $type ) :
                                            $selected = ( $type['id'] == $booth2[1] ) ? 'selected' : '';
                                            echo '<option value="'.$type['id'].'" '.$selected.'>'.$type['name'].'</option>';
                                        endforeach;
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-12 p-t-20">
                                    <input type="text" class="form-control" placeholder="Booth 2 allocation image link" name="booth2_img_link" value="<?php echo $b2_img_link; ?>" >
                                </div>

                                <div class="col-md-12">
                                    <div class="text-right">
                                        <input type="hidden" name="id" value="<?php echo $gp[0]->id; ?>">
                                        <input type="hidden" name="userid" value="<?php echo $gp[0]->user_id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                            </form>

                        <?php elseif( $section == 'location' ) : ?>

                            <form action="<?php echo base_url( 'gallery/update' ); ?>" id="gp-update-form" class="gp-update-form row" method="post">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address 1" name="address1" value="<?php echo $lc[0]->address1; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address 2" name="address2" value="<?php echo $lc[0]->address2; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="City" name="city" value="<?php echo $lc[0]->city; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Country" name="country" value="<?php echo $lc[0]->country; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="PO Box" name="pobox" value="<?php echo $lc[0]->pobox; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip Code" name="zip" value="<?php echo $lc[0]->zip; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select name="type" class="form-control" required>
                                            <option value="">Address type</option>
                                            <option value="1" <?php echo ( $lc[0]->type == 1 ) ? 'selected' : ''; ?>>Primary</option>
                                            <option value="2" <?php echo ( $lc[0]->type == 2 ) ? 'selected' : ''; ?>>Billing</option>
                                            <option value="3" <?php echo ( $lc[0]->type == 3 ) ? 'selected' : ''; ?>>Gallery</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <figure class="text-right">
                                        <input type="hidden" name="id" value="<?php echo $lc[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn btn-primary" value="Update">
                                    </figure>
                                </div>
                            </form>

                        <?php elseif( $section == 'staff' ) : ?>

                            <form action="<?php echo base_url( 'gallery/update' ); ?>" id="gp-update-form" class="gp-update-form row" method="post">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Salutation" name="salutation" value="<?php echo $gt[0]->salutation; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First Name" name="first_name" value="<?php echo $gt[0]->first_name; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="<?php echo $gt[0]->last_name; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Country" name="phone_country" value="<?php echo $gt[0]->phone_country; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone" name="phone" value="<?php echo $gt[0]->phone; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Country" name="mobile_country" value="<?php echo $gt[0]->mobile_country; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Mobile" name="mobile" value="<?php echo $gt[0]->mobile; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $gt[0]->email; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="City" name="city" value="<?php echo $gt[0]->city; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Position" name="position" value="<?php echo $gt[0]->position; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select name="type" class="form-control" required>
                                            <option value="">Contact type</option>
                                            <option value="1" <?php echo ( $gt[0]->type == 1 ) ? 'selected' : ''; ?>>Main contact</option>
                                            <option value="2" <?php echo ( $gt[0]->type == 2 ) ? 'selected' : ''; ?>>CC in emails only</option>
                                            <option value="3" <?php echo ( $gt[0]->type == 3 ) ? 'selected' : ''; ?>>Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <input type="hidden" name="id" value="<?php echo $gt[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                            </form>

                        <?php elseif( $section == 'artist' ) : ?>

                            <form action="<?php echo base_url( 'gallery/update' ); ?>" id="gp-update-form" class="gp-update-form row" method="post">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First Name" name="first_name" value="<?php echo $ar[0]->first_name; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="<?php echo $ar[0]->last_name; ?>" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                      <select name="nationality" class="form-control" placeholder="Nationality" required>
                                          <option value="">Select Country *</option>
                                          <?php
                                          $countries = config_item('country_list');
                                          foreach ( $countries as $key => $value )
                                          {
                                              $selected = ( $key === $ar[0]->nationality ) ? 'selected=selected' : '';
                                              echo '<option value="' . $key . '" '.$selected.'>' . $value . '</option>';
                                          }
                                          ?>
                                      </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="gender" required style="width: 100%;">
                                            <option value="">Select Gender *</option>
                                            <?php
                                                $m_selected = (!empty( $ar[0]->gender ) && $ar[0]->gender == 'm' ) ? 'selected' : '';
                                                $f_selected = (!empty( $ar[0]->gender ) && $ar[0]->gender == 'f' ) ? 'selected' : '';
                                                echo '<option value="m" '. $m_selected  .'>Male</option>';
                                                echo '<option value="f" '. $f_selected  .'>Female</option>';

                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="dob" placeholder="Date of Birth" value="<?php echo (!empty( $ar[0]->dob )) ? $ar[0]->dob : ''; ?>" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="check-box">
                                        <label for="exhibit_flag">
                                            <?php $ae = (!empty( $ae ) ) ? $ae : array(); ?>
                                            <input type="checkbox" id="exhibit_flag" <?php echo ( in_array( $ar[0]->id, $ae ) ) ? 'checked' : ''; ?> name="exhibit_flag"> Add to Exhibiting Artists
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">
                                        <input type="hidden" name="id" value="<?php echo $ar[0]->id; ?>">
                                        <input type="hidden" name="userid" value="<?php echo $ar[0]->user_id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                            </form>

                        <?php elseif( $section == 'participation' ) : ?>

                            <form action="<?php echo base_url( 'gallery/update' ); ?>" id="gp-update-form" class="gp-update-form row" method="post">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Fair Name" name="name" value="<?php echo $pr[0]->name; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Year" name="year" value="<?php echo $pr[0]->year; ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Section" name="section" value="<?php echo $pr[0]->section; ?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <input type="hidden" name="id" value="<?php echo $pr[0]->id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                            </form>

                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>
