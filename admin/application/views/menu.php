<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Important Menu Setting</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                

                <div class="card">
                    <div class="card-title">
                        <h4>Important icon to display</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Menu Name</th>
                                    <th  style="text-align: left; ">Important Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <form action="<?php echo base_url( 'menu/update' ); ?>" method="post">
                                    <?php
                                    if( !empty( $data ) ) :
                                        $counter = 1;
                                        foreach ( $data as $menu ) :
                                            //$file = json_decode( $document->file );
                                            $checked = (!empty( $menu->status ) && $menu->status == 1 ) ? 'checked' : '';
                                            ?>
                                            <tr>
                                                <td><?php echo $menu->id; ?></td>
                                                <td><?php echo $menu->name; ?></td>
                                                <td>
                                                    <div class="row" style="text-align: left; margin: 0; ">
                                                        <div class="col-md-6">
                                                            <input type="radio" name="menu[<?php echo $menu->id; ?>]" value="1" <?php echo (!empty( $menu->status ) && $menu->status == 1 ) ? 'checked' : ''; ?>> Yes</div>
                                                        <div class="col-md-6">
                                                            <input type="radio" name="menu[<?php echo $menu->id; ?>]" value="0"  <?php echo ($menu->status == 0 ) ? 'checked' : ''; ?>> No </div>
                                                    </div>
                                                </td>
                                            </tr> 
                                            <?php
                                            $counter++;
                                        endforeach;
                                    endif;
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><input type="submit" class="btn btn-primary m-t-20" value="Update"></td>
                                    </tr>
                                    
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

    <script>
    function copyToClipboard(filePath) {

      var copyText = document.getElementById(filePath);
      copyText.select();
      copyText.setSelectionRange(0, 99999)
      document.execCommand("copy");
    }
    </script>


</div>