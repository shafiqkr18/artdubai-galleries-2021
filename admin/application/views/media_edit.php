<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Media Gallery Edit</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-title">
                        <h4>&nbsp;</h4>
                    </div>
                    <div class="card-body">
                        <?php
                        if( isset( $_GET['v'] ) && ( $_GET['v'] == 'f' ) ) {
                            $options = '';
                            foreach ( $mediums as $medium ) {
                                $selected = ( isset( $mediums_selected ) && (!empty( $mediums_selected )) && in_array( $medium['id'], $mediums_selected ) ) ? 'selected=selected' : '';
                                $options .= '<option value="'.$medium['id'].'" '.$selected.'>'.$medium['name'].'</option>';
                            }
                            $form = '<form action="'.base_url('media/update').'" method="post">
                                    <label for="">Update Medium Filter</label>
                                    <select style="height: 210px;" name="medium[]" multiple class="form-control" required>
                                        <option value="">Select a Medium Filter</option>
                                        '.$options.'
                                    </select><br>
                                    <p><small>CTRL/COMMAND + CLICK for multiple selection</small></p>
                                    <input type="hidden" name="view" value="'.$_GET['v'].'">
                                    <input type="hidden" name="id" value="'.$_GET['id'].'">
                                    <input type="hidden" name="gid" value="'.$_GET['gid'].'">
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </form>';
                            echo $form;
                        } elseif( isset( $_GET['v'] ) && ( $_GET['v'] == 'c' ) ) {
                            $catalogue = ($info[0]->catalogue == 1) ? 'checked' : '';

                            $booth        = (!empty( $gallery[0]->booth1 ) ) ? explode('|', $gallery[0]->booth1) : 0 ;
                            $get_gallery  = (!empty( $booth[1] ) ) ? $booth[1] : 0 ;

                            if($info[0]->gallery_type != 0){
                              $gallery_type = $info[0]->gallery_type;
                            } else{
                              $gallery_type = '';
                            }


                            $type_options = '';
                            foreach ( $types as $type ) {
                                // && (!empty( $gallery_type )) && ($type['id'] ==  $gallery_type)
                                $selected       = ( isset( $type ) && (!empty( $gallery_type )) && ($type['id'] ==  $gallery_type)) ? 'selected=selected' : '';
                                $type_options  .= '<option value="'.$type['id'].'" '.$selected.'>'.$type['name'].'</option>';
                            }
                            
                            
                            // $currentyear = date('Y');
                            // $yearParam   = $info[0]->exhibit_year;
                            // $option = '';
                            // $option .= '<option value="">Select year</option>';
                            // $option .= '<option value="'. $currentyear .'">'. $currentyear .'</option>';
                            // for($i=1; $i < 2; $i++){
            
                            //     $previous = $currentyear - $i  ;
                            //     $selected = (!empty($yearParam) && $yearParam == $previous) ? 'selected' : '';
            
                            //     $option .= '<option value="'. $previous .'" '. $selected .' >'. $previous .'</option>';
                            // }
                            
                            $currentyear = date('Y');
                            $yearParam   = $info[0]->exhibit_year;
                            $option = '';
                            $option .= '<option value="">Select catalogue in year</option>';
                            foreach ($exhibit_year as $e_year){
                                $year = $e_year->exhibit_year;
                              if($year == $currentyear){
                                  
                                $option .= '<option value="'. $year .'"  selected>'. $year .'</option>';
                              }else{
                                $selected = (!empty($yearParam) && $yearParam == $year) ? 'selected' : '';
            
                                $option .= '<option value="'. $year .'" '. $selected .' >'. $year .'</option>';
                              }
            
                            }
                            
                            //print_r($info[0]);
                            


                            $form = '<form action="'.base_url('media/update').'" method="post">
                                    <div class="row">
                                    <div class="col-md-3">
                                        <label>Exhibiting Year</label>
                                        <select  class="form-control" name="exhibit_year" required>'. $option .'</select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Artist</label>
                                        <input type="text" class="form-control" name="artist" value="'.$info[0]->artist.'" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Title</label>
                                        <input type="text" class="form-control" name="title" value="'.$info[0]->title.'" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Year</label>
                                        <input type="text" class="form-control" name="year" value="'.$info[0]->year.'" required>
                                    </div>
                                    </div>
                                    <div class="mt-2"></div>
                                    <div class="row">
                                      <div class="col-md-3">
                                          <label>Medium</label>
                                          <input type="text" class="form-control" name="medium" value="'.$info[0]->medium.'" required>
                                      </div>
                                      <div class="col-md-3">
                                          <label>Dimensions</label>
                                          <input type="text" class="form-control" name="dimensions" value="'.$info[0]->dimensions.'" required>
                                      </div>
                                      <div class="col-md-3">
                                          <label>Courtesy</label>
                                          <input type="text" class="form-control" name="courtesy" value="'.$info[0]->courtesy.'" required>
                                      </div>
                                      <div class="col-md-3">
                                          <label>Gallery Type</label>
                                          <select name="gallery_type" class="form-control" required>
                                              <option value="">Select Gallery Type</option>
                                              '.$type_options.'
                                          </select>
                                      </div>
                                    </div>
                                    <div class="mt-2"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="checkbox" id="catalogue" name="catalogue" '.$catalogue.'>
                                            <label for="catalogue">Add to e-Catalogue</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="view" value="'.$_GET['v'].'">
                                    <input type="hidden" name="id" value="'.$_GET['id'].'">
                                    <input type="hidden" name="gid" value="'.$_GET['gid'].'">
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </form>';
                            echo $form;
                        } else {
                            echo '<p>Something went wrong!</p>';
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>
