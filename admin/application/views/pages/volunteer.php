<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-primary">Volunteer Page</h3>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">

				<div class="card">
					<div class="card-body">
						<form id="settings-form" action="<?php echo base_url() ?>pages/update" method="post">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<textarea name="box1" id="box1" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box1 ) ) ? $data->box1 : ''; ?></textarea>
									</div>
								</div>
								<div class="col-md-12 text-right">
									<input type="hidden" name="page_name" value="volunteer">
									<input type="submit" class="btn btn-lg btn-primary" value="Update">
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>

	<footer class="footer"> © 2018 All rights reserved.</footer>

</div>
