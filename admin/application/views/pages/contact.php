<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-primary">Contact Page</h3>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">

				<div class="card">
					<div class="card-body">
						<form id="settings-form" action="<?php echo base_url() ?>pages/update" method="post">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Text Box 1</label>
										<textarea name="box1" id="box1" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo $data->box1; ?></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Text Box 2</label>
										<textarea name="box2" id="box2" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo $data->box2; ?></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Text Box 3</label>
										<textarea name="box3" id="box3" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo $data->box3; ?></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Text Box 4</label>
										<textarea name="box4" id="box4" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo $data->box4; ?></textarea>
									</div>
								</div>
								<div class="col-md-12 text-right">
									<input type="hidden" name="page_name" value="contact">
									<input type="submit" class="btn btn-lg btn-primary" value="Update">
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>

	<footer class="footer"> © 2018 All rights reserved.</footer>

</div>