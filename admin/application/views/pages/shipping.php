<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-primary">Shipping Page</h3>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">

				<div class="card">
					<div class="card-body">
						<form id="settings-form" action="<?php echo base_url() ?>pages/update" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Partner 1</label>
                                        <input type="text" name="title1" class="form-control" value="<?php echo ( !empty( $data->title1 ) ) ? $data->title1 : ''; ?>" placeholder="Title">
										<textarea name="box1" id="box1" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box1 ) ) ? $data->box1 : ''; ?></textarea>
									</div>
                                    <div class="form-group">
                                        <?php if( $data->image1 ) : ?>
                                            <img style="margin: 0 0 10px;width: 100px;" src="<?php echo upload_dir_admin( $data->image1->file_name ); ?>" alt="">
                                            <br>
                                        <?php endif; ?>
                                        <input type="file" name="image1" accept="image/jpg,image/jpeg" class="form-control-file">
                                        <p><small>* Ideal image size for this space is 100x100</small></p>
                                    </div>
									<div class="form-group">
										<label>Partner 2</label>
                                        <input type="text" name="title2" class="form-control" value="<?php echo ( !empty( $data->title2 ) ) ? $data->title2 : ''; ?>" placeholder="Title">
										<textarea name="box2" id="box2" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box2 ) ) ? $data->box2 : ''; ?></textarea>
									</div>
                                    <div class="form-group">
                                        <?php if( $data->image2 ) : ?>
                                            <img style="margin: 0 0 10px;width: 100px;" src="<?php echo upload_dir_admin( $data->image2->file_name ); ?>" alt="">
                                        <?php endif; ?>
                                        <input type="file" name="image2" accept="image/jpg,image/jpeg" class="form-control-file">
                                        <p><small>* Ideal image size for this space is 100x100</small></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Others</label>
                                        <textarea name="box3" id="box3" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box3 ) ) ? $data->box3 : ''; ?></textarea>
                                        <textarea name="box4" id="box4" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box4 ) ) ? $data->box4 : ''; ?></textarea>
                                        <textarea name="box5" id="box5" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box5 ) ) ? $data->box5 : ''; ?></textarea>
                                    </div>
								</div>
								<div class="col-md-12 text-right">
									<input type="hidden" name="page_name" value="shipping">
									<input type="submit" class="btn btn-lg btn-primary" value="Update">
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>

	<footer class="footer"> © 2018 All rights reserved.</footer>

</div>
