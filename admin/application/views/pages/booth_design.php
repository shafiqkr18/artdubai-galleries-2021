<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-primary">Booth Design Page</h3>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">

				<div class="card">
					<div class="card-body">
						<form id="settings-form" action="<?php echo base_url() ?>pages/update" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Intro</label>
										<textarea name="box1" id="box1" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box1 ) ) ? $data->box1 : ''; ?></textarea>
									</div>
									<div class="form-group">
										<label>Contact Person</label>
										<textarea name="box2" id="box2" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box2 ) ) ? $data->box2 : ''; ?></textarea>
									</div>
                                    <div class="form-group">
                                        <?php if( $data->image ) : ?>
                                            <img style="margin: 0 0 10px;width: 100px;" src="<?php echo upload_dir_admin( $data->image->file_name ); ?>" alt="">
                                        <?php endif; ?>
                                        <input type="file" name="image" accept="image/jpg,image/jpeg" class="form-control-file">
                                        <p><small>* Ideal image size for this space is 100x100</small></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Intro 2</label>
                                        <textarea name="box3" id="box3" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box3 ) ) ? $data->box3 : ''; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Gallery Fascia</label>
                                        <textarea name="box4" id="box4" class="textarea_editor form-control" style="height: 250px;" cols="30" rows="10"><?php echo ( !empty( $data->box4 ) ) ? $data->box4 : ''; ?></textarea>
                                    </div>
								</div>
								<div class="col-md-12 text-right">
									<input type="hidden" name="page_name" value="booth_design">
									<input type="submit" class="btn btn-lg btn-primary" value="Update">
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>

	<footer class="footer"> © 2018 All rights reserved.</footer>

</div>
