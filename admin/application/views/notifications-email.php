<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Email Notifications</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        
                       <?php $response = $this->input->get('response'); 
                       
                       if($response == 1 || $response == 2):
                           echo '<div class="alert alert-success" style="color: #2f7b78;">
                              <strong>Success!</strong> Email has been sent to all selected users.
                            </div>';
                       elseif($response == 3):
                           echo '<div class="alert alert-warning">
                                    <strong>Warning!</strong> Sorry no user is selected.
                                </div>';
                        endif;
                       
                       ?> 
                       
                        <form id="settings-form" action="<?php echo base_url() ?>notifications/create_email" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select users to send email notificaitons</label><br>
                                        <input class="radio-notification" type="radio" name="type" value="1" checked><span style="display: inline-block;margin: 0 10px;">All gallery users</span>
                                        <input class="radio-notification" type="radio" name="type" value="2"><span style="display: inline-block;margin: 0 10px;">Selected gallery users</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <select name="users[]" id="user_selection" class="multipleSelect" multiple style="display: none;" >
                                        <!-- <select name="users[]" id="user_selection" style="height: 100px;" required class="form-control" disabled multiple> -->
                                            <?php
                                            $i = 0;
                                            foreach ( $users as $user )
                                            {
                                                if( !empty( $user->galleryname ) )
                                                {
                                                    echo '<option data-archive-user="'. $i . ' - ' . $user->archive .'" value="' . $user->id . '">' . $user->galleryname . '</option>';
                                                }
                                                else
                                                {
                                                    echo '<option data-archive-user="'. $i . ' - ' . $user->archive .'" value="' . $user->id . '">' . $user->id . '</option>';
                                                }
                                            $i++;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select gallery contact type to send email notificaitons</label><br>
                                        <select name="contact_type">
                                            <option value="">Select Contact Type</option>
                                            <option value="1">Main Contact</option>
                                            <option value="2">CC Contact</option>
                                            <option value="3">Others Contact</option>
                                        </select>
                                    </div>
                                    <div class="alert alert-warning" role="alert" style="color: #856404;">
                                      Note: Email notifcation will send to registered account and selected contact type.
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label>Email attachment</label><br>
                                    <input type="file" id="file" name="file" class="form-control"> 
                                    <p><small>* Max file size can be 20MB</small></p> 
                                    <p><small>* jpg|jpeg|pdf|docx|doc|docs|png|ppt</small></p>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Email Subject</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title" value="" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Email Message</label>
                                        <textarea name="message" id="box1" class="textarea_editor form-control" style="height: 250px; margin-top:0px" cols="30" rows="10" required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                    <input type="submit" class="btn btn-primary" value="Send">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>