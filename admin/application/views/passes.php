<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Passes</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">

                            <form action="" class="mb-5" method="post">
                                <p>Select filter options:</p>
                                <fieldset>
                                    <input id="artists" type="radio" name="filter_b" value="artist" <?php echo ( !empty( $_POST['filter_b'] ) && ( $_POST['filter_b'] == 'artists' ) ) ? 'checked' : ''; ?>>
                                    <label for="artists">Artist</label>
                                    <input id="exhibitor" type="radio" name="filter_b" value="exhibitor" <?php echo ( !empty( $_POST['filter_b'] ) && ( $_POST['filter_b'] == 'exhibitor' ) ) ? 'checked' : ''; ?>>
                                    <label for="exhibitor">Exhibitor</label>
                                    <input type="submit" value="Update" class="btn btn-outline-warning float-right">
                                    <a href="" class="btn btn-outline-info float-right mr-1">Reset</a>
                                </fieldset>
                            </form>

                            <table class="table export_table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Gallery</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th class="tableexport-ignore">Thumb</th>
                                    <th class="hidden">Thumb</th>
                                </tr>
                                </thead>
                                <tbody>

		                        <?php
		                        if( isset( $passes ) && !empty( $passes ) ){
			                        $count = 1;
			                        foreach( $passes as $pass )
			                        {

                                        if( !empty( $_POST['filter_b'] ) && !empty( $_POST['filter_b'] ) ) {
                                            $filter = ( $pass['type'] == $_POST['filter_b'] ) ? true : false;
                                        } else {
                                            $filter = true;
                                        }

                                        if( $filter ) :

                                            echo "<tr>";
                                            echo "<td>".$count."</td>";
                                            echo "<td>".$pass['gallery_name'][0]->galleryname."</td>";
                                            echo "<td>".(!empty( $pass['person'][0]->first_name ) ? $pass['person'][0]->first_name : '').(!empty( $pass['person'][0]->last_name ) ? ' ' . $pass['person'][0]->last_name : '')."</td>";
                                            echo "<td>".$pass['type']."</td>";
                                            echo "<td class='tableexport-ignore'><img width='100' src='". upload_dir( 'passes/' . $pass['thumb'] ) ."' alt=''></td>";
                                            echo "<td class='hidden'>". upload_dir( 'passes/' . $pass['thumb'] ) ."</td>";
                                            echo "</tr>";
                                            $count++;

                                        endif;

			                        }
		                        }
		                        else{
			                        echo '<tr><td colspan="4" style="text-align: center;">No data found!</td></tr>';
		                        }
		                        ?>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>