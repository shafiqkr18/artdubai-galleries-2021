<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Gallery Users</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table export_table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Gallery Name</th>
                                    <th class="tableexport-ignore" style="min-width: 150px;"></th>
                                </tr>
                                </thead>
                                <tbody>

					            <?php
					            if( isset( $users ) && !empty( $users ) ){
					                $count = 1;
						            foreach( $users as $user )
						            {
							            echo "<tr>";
							            echo "<td>".$count."</td>";
							            echo "<td>".$user->fullname."</td>";
							            echo "<td>".$user->email."</td>";
							            echo "<td>".$user->galleryname."</td>";
							            echo "<td class='tableexport-ignore'><a href=".base_url( 'gallery/view_user' )."?id=".$user->id.">View</a> | <a class='delete-action' href=".base_url( 'gallery/delete_user' )."?id=".$user->id.">Delete</a></td>";
							            echo "</tr>";
                                        $count++;
						            }
					            }
					            else{
						            echo '<tr><td colspan="14" style="text-align: center;">No data found!</td></tr>';
					            }
					            ?>

                                </tbody>
                            </table>
                            <p class="mt-5"><small>* Deleting a user will delete all of its gallery related information as well.</small></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>
