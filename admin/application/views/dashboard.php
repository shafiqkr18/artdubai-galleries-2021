<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Dashboard</h3>
        </div>
    </div>

	<div class="container-fluid">

        <div class="row">

            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?php echo $total_users; ?></h2>
                            <p class="m-b-0">Registered Users</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-file f-s-40 color-primary"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2><?php echo $total_gallery; ?></h2>
                            <p class="m-b-0">Gallery Submissions</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

	</div>

	<footer class="footer"> © 2018 All rights reserved.</footer>

</div>