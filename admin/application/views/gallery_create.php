<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Add Gallery Information</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">

                        <?php if( $section == 'artist' ) : ?>

                            <h4 class="mb-3"><strong>Add New Artist</strong></h4>

                            <form action="<?php echo base_url( 'gallery/create' ); ?>" id="gp-update-form" class="gp-update-form row" method="post">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First Name" name="first_name" value="" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="check-box">
                                        <label for="exhibit_flag">
                                            <input type="checkbox" id="exhibit_flag" name="exhibit_flag"> Add to Exhibiting Artists
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">
                                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                        <input type="hidden" name="section" value="<?php echo $section; ?>">
                                        <input type="submit" class="btn btn-primary" value="Add">
                                    </div>
                                </div>
                            </form>

                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>