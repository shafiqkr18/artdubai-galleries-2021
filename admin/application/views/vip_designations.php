<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">VIP Designations</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">

                        <?php if( isset( $view ) ) : ?>

                            <form id="settings-form" action="<?php echo base_url() ?>vip/update" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Salutation</label>
                                            <input type="text" readonly name="salutation" class="form-control" value="<?php echo $entry->salutation; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>First name</label>
                                            <input type="text" readonly name="first_name" class="form-control" value="<?php echo $entry->first_name; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Sur name</label>
                                            <input type="text" readonly name="sur_name" class="form-control" value="<?php echo $entry->sur_name; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Company</label>
                                            <input type="text" readonly name="company" class="form-control" value="<?php echo $entry->company; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Job title</label>
                                            <input type="text" readonly name="job_title" class="form-control" value="<?php echo $entry->job_title; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">
                                      <div class="form-group">
                                          <label>Mobile Code</label>
                                          <input type="text" readonly name="mobile_code" class="form-control" value="<?php echo $entry->mobile_code; ?>">
                                      </div>
                                  </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <input type="text" readonly name="mobile" class="form-control" value="<?php echo $entry->mobile; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" readonly name="email" class="form-control" value="<?php echo $entry->email; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Apartment / Flat</label>
                                            <input type="text" readonly name="apt_flat" class="form-control" value="<?php echo $entry->apt_flat; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Street / Block</label>
                                            <input type="text" readonly name="street_block" class="form-control" value="<?php echo $entry->street_block; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Postal Code</label>
                                            <input type="text" readonly name="apt_flat" class="form-control" value="<?php echo $entry->postal_zip_code; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>PO Box</label>
                                            <input type="text" readonly name="street_block" class="form-control" value="<?php echo $entry->po_box; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" readonly name="city" class="form-control" value="<?php echo $entry->city; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>State</label>
                                            <input type="text" readonly name="state" class="form-control" value="<?php echo $entry->state; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input type="text" readonly name="country" class="form-control" value="<?php echo $entry->country; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Invitation Type</label>
                                            <select readonly name="invite_type" class="form-control">
                                                <option value="patrons" <?php echo ( $entry->invite_type == 'patrons' ) ? 'SELECTED' : '' ?>>Patrons</option>
                                                <option value="collectors" <?php echo ( $entry->invite_type == 'collectors' ) ? 'SELECTED' : '' ?>>Collectors</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        <?php else: ?>

                            <div class="table-responsive">
                                <table class="table export_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Gallery</th>
                                        <th class="hidden">Salutation</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Company</th>
                                        <th class="hidden">Job Title</th>
                                        <th class="hidden">Mobile Code</th>
                                        <th class="hidden">Mobile</th>
                                        <th class="hidden">Email</th>
                                        <th class="hidden">Apartment / Flat</th>
                                        <th class="hidden">Street / Block</th>
                                        <th class="hidden">ZIP / Postal Code</th>
                                        <th class="hidden">PO Box</th>
                                        <th class="hidden">City</th>
                                        <th class="hidden">State</th>
                                        <th class="hidden">Country</th>
                                        <th>Invite Type</th>
                                        <th class="tableexport-ignore"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    if( isset( $entries ) && !empty( $entries ) ){
                                        $count = 1;
                                        foreach( $entries as $entry )
                                        {
                                            echo "<tr>";
                                            echo "<td>".$count."</td>";
                                            echo "<td>".$entry['gallery_name'][0]->galleryname."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->salutation."</td>";
                                            echo "<td>".$entry['entry']->first_name."</td>";
                                            echo "<td>".$entry['entry']->sur_name."</td>";
                                            echo "<td>".$entry['entry']->company."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->job_title."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->mobile_code."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->mobile."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->email."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->apt_flat."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->street_block."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->postal_zip_code."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->po_box."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->city."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->state."</td>";
                                            echo "<td class='hidden'>".$entry['entry']->country."</td>";
                                            echo "<td>".$entry['entry']->invite_type."</td>";
                                            echo "<td class='tableexport-ignore'><a href=".base_url( 'vip/delete' )."?id=".$entry['entry']->id.">Delete</a>&nbsp;|&nbsp;<a href=".base_url( 'vip/view' )."?id=".$entry['entry']->id.">View</a></td>";
                                            echo "</tr>";
                                            $count++;
                                        }
                                    }
                                    else{
                                        echo '<tr><td colspan="4" style="text-align: center;">No data found!</td></tr>';
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>

                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>
