<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Gallery Submissions</h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <form id="settings-form" action="<?php echo base_url() ?>entries/update_application" method="post">
                            <h4>Gallery Profile</h4>
                            <hr>
                            <div class="row">
					            <?php if( isset( $data['gp'] ) ) : ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Gallery Name</label>
                                            <input type="text" readonly name="gallery_name" class="form-control" value="<?php echo $data['gp'][0]->name; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Established Date</label>
                                            <input type="text" readonly name="gallery_established" class="form-control" value="<?php echo $data['gp'][0]->established; ?>" style="text-transform: capitalize;">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Gallery Email</label>
                                            <input type="text" readonly name="gallery_email" class="form-control" value="<?php echo $data['gp'][0]->email; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Gallery Website</label>
                                            <input type="text" readonly name="gallery_website" class="form-control" value="<?php echo $data['gp'][0]->website; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Gallery Biography</label>
                                            <textarea name="gallery_bio" readonly class="form-control" style="height: 250px;" rows="20"><?php echo $data['gp'][0]->bio; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Indiviual Booth Plan Link</label>
                                            <input type="text" readonly name="indiviual_booth_plan" class="form-control" value="<?php echo $data['gp'][0]->indiviual_booth_plan; ?>">
                                        </div>
                                    </div>
                                    <?php
                                    if( !empty( $data['gp'][0]->booth1 ) ) {
                                        $bt1 = explode( '|', $data['gp'][0]->booth1 );
                                    } else {
                                        $bt1[0] = $bt1[1] = '';
                                    }
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Booth Info 1</label>
                                            <input type="text" readonly name="gallery_booth" class="form-control" value="<?php echo $bt1[0] . ' ' . $bt1[1]; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>M2</label>
                                            <input type="text" readonly name="gallery_m2" class="form-control" value="<?php echo $data['gp'][0]->booth1_m2; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Allocation Image Link</label>
                                            <p><?php echo (!empty( $data['gp'][0]->booth1_img_link ) ) ? '<a target="_blank" href="'. $data['gp'][0]->booth1_img_link .'">'.$data['gp'][0]->booth1_img_link.'</a>' : '' ;?></p>
                                        </div>
                                        <?php /**/ ?>
                                        <div class="form-group">
                                            <label>Allocation Image</label>
                                            <?php $booth1_allocation = json_decode($data['gp'][0]->booth1_allocation); ?>
                                            <div class="">
                                                <?php echo (!empty( $booth1_allocation->file_name) ) ? '<a target="_blank" href="'.base_url('../uploads/booth-location/'.$booth1_allocation->file_name).'">'.$booth1_allocation->file_name.'</a>' : '' ;?>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <?php
                                    if( !empty( $data['gp'][0]->booth2 ) ) {
                                        $bt2 = explode( '|', $data['gp'][0]->booth2 );
                                    } else {
                                        $bt2[0] = $bt2[1] = '';
                                    }
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Booth Info 2</label>
                                            <input type="text" readonly name="gallery_booth" class="form-control" value="<?php echo $bt2[0] . ' ' . $bt2[1]; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>M2</label>
                                            <input type="text" readonly name="gallery_m2" class="form-control" value="<?php echo $data['gp'][0]->booth2_m2; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Allocation Image Link</label>
                                            <p><?php echo (!empty($data['gp'][0]->booth2_img_link) ) ? '<a target="_blank" href="'. $data['gp'][0]->booth2_img_link .'">'.$data['gp'][0]->booth2_img_link.'</a>' : '' ;?></p>
                                        </div>
                                        <?php /**/ ?>
                                        <?php if (!empty($bt2[0]) || $bt2[1]) : ?>
                                            <div class="form-group">
                                                <label>Allocation Image</label>

                                                <?php $booth2_allocation = json_decode($data['gp'][0]->booth2_allocation); ?>
                                                 <div class="">
                                                    <?php echo (!empty( $booth2_allocation->file_name) ) ? '<a target="_blank" href="'.base_url('../uploads/booth-location/'.$booth2_allocation->file_name).'">'.$booth2_allocation->file_name.'</a>' : '' ;?>
                                                </div>
                                            </div>
                                        <?php endif; ?> 
                                    </div>
                                    <div class="col-md-12">
                                        <div class="text-right"><a href="<?php echo base_url( 'gallery/edit/?s=info&i='.$data['gp'][0]->id.'&u='.$data['us'] ); ?>" class="btn btn-primary">Edit</a></div>
                                    </div>
					            <?php else: ?>
                                    <div class="col-md-12">
                                        <p>No information is available yet.</p>
                                    </div>
					            <?php endif; ?>
                            </div>
                            <h4>Locations</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped" style="margin-bottom: 30px;">
                                        <thead>
                                        <tr>
                                            <th style="border-top:0;">Address 1</th>
                                            <th style="border-top:0;">Address 2</th>
                                            <th style="border-top:0;">City</th>
                                            <th style="border-top:0;">Country</th>
                                            <th style="border-top:0;">PO Box</th>
                                            <th style="border-top:0;">Zip Code</th>
                                            <th style="border-top:0;">Type</th>
                                            <th style="border-top:0;"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
							            <?php
							            if( isset( $data['lc'] ) && !empty( $data['lc'] ) )
							            {
								            foreach ( $data['lc'] as $location )
								            {
									            echo "<tr>";
									            echo "<td>".$location['address1']."</td>";
									            echo "<td>".$location['address2']."</td>";
									            echo "<td>".$location['city']."</td>";
									            echo "<td>".$location['country']."</td>";
									            echo "<td>".$location['pobox']."</td>";
									            echo "<td>".$location['zip']."</td>";
									            if( $location['type'] == 1 )
									            {
										            echo "<td>Primary</td>";
									            }
                                                elseif( $location['type'] == 2 )
									            {
										            echo "<td>Billing</td>";
									            }
									            else
									            {
										            echo "<td>Others</td>";
									            }
									            echo '<td><a href="' . base_url( 'gallery/edit/?s=location&i='.$location['id'].'&u='.$data['us'] ) . '" class="color-primary">Edit</a></td>';
									            echo "</tr>";
								            }
							            }
							            else
							            {
								            echo "<tr><td colspan='7' style='text-align: left;'>No location is added yet.</td></tr>";
							            }
							            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <h4>Gallery Staff</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped" style="margin-bottom: 30px;">
                                        <thead>
                                        <tr>
                                            <th style="border-top:0;">Name</th>
                                            <th style="border-top:0;">City</th>
                                            <th style="border-top:0;">Phone</th>
                                            <th style="border-top:0;">Mobile</th>
                                            <th style="border-top:0;">Email</th>
                                            <th style="border-top:0;">Position</th>
                                            <th style="border-top:0;">Type</th>
                                            <th style="border-top:0;"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
							            <?php
							            if( isset( $data['gt'] ) && !empty( $data['gt'] ) )
							            {
								            foreach ( $data['gt'] as $member )
								            {
									            echo "<tr>";
									            echo "<td>".$member['salutation']." ".$member['first_name']." ".$member['last_name']."</td>";
									            echo "<td>".$member['city']."</td>";
									            echo "<td>".$member['phone']."</td>";
									            echo "<td>".$member['mobile']."</td>";
									            echo "<td>".$member['email']."</td>";
									            echo "<td>".$member['position']."</td>";
									            if( $member['type'] == 1 )
									            {
										            echo "<td>Main Contact</td>";
									            }
                                                elseif( $member['type'] == 2 )
									            {
										            echo "<td>CC in Emails</td>";
									            }
									            else
									            {
										            echo "<td>Others</td>";
									            }
                                                echo '<td><a href="' . base_url( 'gallery/edit/?s=staff&i='.$member['id'].'&u='.$data['us'] ) . '" class="color-primary">Edit</a></td>';
									            echo "</tr>";
								            }
							            }
							            else
							            {
								            echo "<tr><td colspan='7' style='text-align: left;'>No team member is added yet.</td></tr>";
							            }
							            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <h4>Art Fair Participation</h4>
                            <hr>
                            <table class="table table-striped" style="margin-bottom: 30px;">
                                <thead>
                                <tr>
                                    <th style="border-top:0;">Name</th>
                                    <th style="border-top:0;">Year</th>
                                    <th style="border-top:0;">Section</th>
                                    <th style="border-top:0;"></th>
                                </tr>
                                </thead>
                                <tbody>
					            <?php
					            if( isset( $data['pt'] ) )
					            {
						            foreach ( $data['pt'] as $participation )
						            {
							            echo "<tr>";
							            echo "<td>".$participation['name']."</td>";
							            echo "<td>".$participation['year']."</td>";
							            echo "<td>".$participation['section']."</td>";
                                        echo '<td><a href="' . base_url( 'gallery/edit/?s=participation&i='.$participation['id'].'&u='.$data['us'] ) . '" class="color-primary">Edit</a></td>';
							            echo "</tr>";
						            }
					            }
					            else
					            {
						            echo "<tr><td colspan='3' style='text-align: left;'>No detail is added yet.</td></tr>";
					            }
					            ?>
                                </tbody>
                            </table>
                            <h4>Artists to Represent <a href="<?php echo base_url( 'gallery/add/?s=artist&u='.$data['us'] ); ?>" class="float-right btn btn-primary">Add New</a></h4>
                            <hr>
                            <table class="table table-striped" style="margin-bottom: 30px;">
                                <thead>
                                <tr>
                                    <th style="border-top:0;">First Name</th>
                                    <th style="border-top:0;text-align: left;">Last Name</th>
                                    <th style="border-top:0;text-align: center;">Exhibiting</th>
                                    <th style="border-top:0;text-align: center;">Nationality</th>
                                    <th style="border-top:0;text-align: center;">Date of Birth</th>
                                    <th style="border-top:0;text-align: center;">Gender</th>
                                    <th style="border-top:0;text-align: center;">Approved</th>
                                    <th style="border-top:0;"></th>
                                </tr>
                                </thead>
                                <tbody>
					            <?php
					            if( isset( $data['ar'] ) )
					            {
						            foreach ( $data['ar'] as $artist )
						            {
                                        //Array ( [id] => 3188 [user_id] => 34 [first_name] => Ana [last_name] => Mazzei [nationality] => [dob] => [approved] => 0 )
                                        $gender     = '';
                                        if($artist['gender'] == 'm'){ $gender = 'Male'; }else if($artist['gender'] == 'f'){ $gender = 'Female'; }

                                        // foreach ($data['ae'] as $key => $value):
                                        //     $artists = json_decode($value['artists']);
                                        //     $exhibiting[] = ( !empty($artists) && in_array( $artist['id'], $artists ) ) ? $artist['id'] : '';
                                        // endforeach;
                                        // $exhibiting_artist = ( !empty($exhibiting) && in_array( $artist['id'], $exhibiting ) ) ?'Yes' : '';


						                $exhibiting = ( !empty($data['ae']) && in_array( $artist['id'], $data['ae'] ) ) ? 'Yes' : '';
                            $approve_status     = ( $artist['approved']  == 1 ) ? 'Yes' : 'No';

                            $countries = config_item('country_list');
                            $country = $artist['nationality'];
                            $selected_country = '';
                            if (array_key_exists($country, $countries)) {
                                $selected_country = ( !empty( $countries[$country] ) ) ? $countries[$country] : $artist['nationality'];
                            }


							            echo "<tr>";
							            echo "<td>".$artist['first_name']."</td>";
							            echo "<td style='text-align: left;'>".$artist['last_name']."</td>";
                                        echo "<td style='text-align: center;'>". $exhibiting ."</td>";
                                        echo "<td style='text-align: center;'>". $selected_country ."</td>";
                                        echo "<td style='text-align: center;'>". $artist['dob'] ."</td>";
                                        echo "<td style='text-align: center;'>". $gender."</td>";
                                        echo "<td style='text-align: center;'>". $approve_status."</td>";
                                        echo '<td><a href="' . base_url( 'gallery/delete_artist/?i='.$artist['id'].'&u='.$data['us'] ) . '" class="color-danger">Delete</a> | <a href="' . base_url( 'gallery/edit/?s=artist&i='.$artist['id'].'&u='.$data['us'] ) . '" class="color-primary">Edit</a></td>';
							            echo "</tr>";
						            }
					            }
					            else
					            {
						            echo "<tr><td colspan='2' style='text-align: left;'>No information is added yet.</td></tr>";
					            }
					            ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>
