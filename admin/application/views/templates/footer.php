<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    </div>
    <!-- End Wrapper -->

    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sticky-kit.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/Blob.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/xlsx.core.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/FileSaver.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tableexport.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.form.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wysihtml5-parser.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-wysihtml5.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-te-1.4.0.min.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fastselect.standalone.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>

</body>

</html>