<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="header">
	<nav class="navbar top-navbar navbar-expand-md navbar-light">

		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url() ?>">
				<b><img src="<?php echo base_url() ?>assets/images/logo.png" width="30" alt="homepage" class="dark-logo" /></b>
				<span><img src="<?php echo base_url() ?>assets/images/logo-text.png" height="15" alt="homepage" class="dark-logo" /></span>
			</a>
		</div>

		<div class="navbar-collapse">

			<ul class="navbar-nav mr-auto mt-md-0">
				<li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
				<li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
			</ul>

			<ul class="navbar-nav my-lg-0">

				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle text-muted" name="Umair Ishtiaq" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url() ?>assets/images/avatar.png" alt="user" class="profile-pic" /></a>
					<div class="dropdown-menu dropdown-menu-right animated zoomIn">
						<ul class="dropdown-user">
							<li><a href="<?php echo base_url() ?>logout"><i class="fa fa-power-off"></i> Logout</a></li>
						</ul>
					</div>
				</li>

			</ul>
		</div>

	</nav>
</div>

<div class="left-sidebar">

	<div class="scroll-sidebar">
		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<li class="nav-devider"></li>
				<li class="nav-label"><a href="<?php echo base_url() ?>">Home</a></li>
                <li class="nav-label">
                    <a class="has-arrow" href="#">Notifications</a>
                    <ul aria-expanded="false" class="collapse">
                        <li class="nav-label"><a href="<?php echo base_url( 'notifications' ) ?>">Web</a></li>
                        <li class="nav-label"><a href="<?php echo base_url( 'email-notifications' ) ?>">Email</a></li>
                    </ul>
                </li>
                <li class="nav-label">
                    <a class="has-arrow" href="#">Gallery</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url( 'gallery' ) ?>">Profiles</a></li>
                        <li><a href="<?php echo base_url( 'gallery/volunteer_requests' ) ?>">Volunteer Requests</a></li>
                        <li><a href="<?php echo base_url( 'gallery/travel_requests' ) ?>">Travel Requests</a></li>
                        <li><a href="<?php echo base_url( 'gallery/gallery_fascia' ) ?>">Gallery Fascia</a></li>
                        <li><a href="<?php echo base_url( 'gallery/gallery_communication' ) ?>">Gallery Communication</a></li>
                        <li><a href="<?php echo base_url( 'passes' ) ?>">Passes</a></li>
                        <li><a href="<?php echo base_url( 'gallery/users' ) ?>">Users</a></li>
                        <li><a href="<?php echo base_url( 'vip' ) ?>">VIP Designations</a></li>
                        <li><a href="<?php echo base_url( 'media' ) ?>">Media</a></li>
                    </ul>
                </li>
                <li class="nav-label">
                    <a class="has-arrow" href="#">Pages</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url( 'pages/schedule' ) ?>">Dashboard</a></li>
						<li><a href="<?php echo base_url( 'pages/contact' ) ?>">Contact</a></li>
                        <li><a href="<?php echo base_url( 'pages/travel' ) ?>">Travel</a></li>
                        <li><a href="<?php echo base_url( 'pages/communications' ) ?>">Communications</a></li>
                        <li><a href="<?php echo base_url( 'pages/booth_design' ) ?>">Booth Design</a></li>
                        <li><a href="<?php echo base_url( 'pages/shipping' ) ?>">Shipping</a></li>
                        <li><a href="<?php echo base_url( 'pages/volunteer' ) ?>">Volunteer</a></li>
                    </ul>
                </li>
                <li class="nav-label"><a href="<?php echo base_url('menu') ?>">Important Menu</a></li>
                <li class="nav-label"><a href="<?php echo base_url('documents') ?>">Documents</a></li>
                <li class="nav-label"><a href="<?php echo base_url( 'admins' ) ?>">Administrators</a></li>
                <?php 
                $event_year = config_item('event_year');
                
                echo $event_year;
                ?>
                
			</ul>
		</nav>
	</div>

</div>
