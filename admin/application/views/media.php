<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Media Gallery</h3>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">

                    <?php if( !empty( $doc_view ) && isset( $doc_view ) ) : ?>



                        <div class="row">
                            <div class="col-md-12 text-right">
                              <?php $url = "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
                              <?php
                              $currentyear = date('Y');
                                $media_id    = $this->input->get('id');
                                $yearParam   = $this->input->get('year');
                                $option = '';
                                $option = '<select style="font-size: 12px; min-width: 200px;" class="filter-year-catalogue float-right" name="year">';
                                $option .= '<option value="">Select catalogue in year</option>';
                                foreach ($exhibit_year as $e_year){
                                    $year = $e_year->exhibit_year;
                                  if($year == $currentyear){
                                      
                                    $option .= '<option value="'. site_url('Media/view?id='.$media_id) .'">'. $year .'</option>';
                                  }else{
                                    $selected = (!empty($yearParam) && $yearParam == $year) ? 'selected' : '';
                
                                    $option .= '<option value="'. site_url('Media/view?id='.$media_id.'&year='.$year) .'" '. $selected .' >'. $year .'</option>';
                                  }
                
                                }
                              
                              
                              //print_r($exhibit_year);
                                  //$currentyear = date('Y');
                                //   $media_id    = $this->input->get('id');
                                //   $yearParam   = $this->input->get('year');
                                //   $option = '<select style="font-size: 12px; min-width: 200px;" class="filter-year-catalogue float-right" name="year">';
                                //   $option .= '<option value="'. site_url('Media/view?id='.$media_id) .'">Select catalogue in year</option>';
                                //   for($i=1; $i < 2; $i++){

                                //       $previous = $currentyear - $i  ;
                                //       $selected = (!empty($yearParam) && $yearParam == $previous) ? 'selected' : '';

                                //       $option .= '<option value="'. site_url('Media/view?id='.$media_id.'&year='.$previous) .'" '. $selected .' >'. $previous .'</option>';
                                //   }
                                  $option .= '</select>';
                                ?>
                            </div>
                        </div>

                        <div class="card-title">
                            <h4 style="display: block;">&nbsp; <?php echo $option; ?></h4>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Thumbnail</th>
                                        <th>Detail</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if( !empty( $files ) && isset( $files ) ) {
                                        foreach ( $files as $file )
                                        {
                                            $document = json_decode( $file->files );
                                            if(isset($document->file_path)) {
                                                if (strpos($document->file_path, 'residentsapplica') !== false) {
                                                    $src = upload_dir( 'residents/' . $document->file_name );
                                                } else {
                                                    $src = upload_dir( $document->file_name );
                                                }
                                            } else {
                                                $src = upload_dir( $document->file_name );
                                            }
                                            $catalogue = ( $file->catalogue == 1 ) ? 'Yes' : 'No';
                                            echo '<tr>
                                                    <td><img width="150" src="'. $src .'" alt=""><span style="display: block;">Size: ' .  number_format((float)($document->file_size/1024), 2, '.', '') .'MB</span><span style="display: block;">Date: ' . local_date( strtotime( $file->date_created ), 'd/m/Y' ) .'</span></td>
                                                    <td>Artist: '.$file->artist.'<br>
                                                    Title: '.$file->title.'<br>
                                                    Year: '.$file->year.'<br>
                                                    Medium: '.$file->medium.'<br>
                                                    Dimensions: '.$file->dimensions.'<br>
                                                    Courtesy: '.$file->courtesy.'<br>
                                                    Catalogue: '.$catalogue.'<br>
                                                    </td>
                                                    <td><a href="'. $src .'" class="btn btn-secondary" target="_blank">View Original Image</a>&nbsp;<a href="'.site_url('Media/edit?v=c&gid='.$_GET['id'].'&id='.$file->id).'" class="btn btn-secondary">Update Caption</a>&nbsp;<a href="'.site_url('Media/edit?v=f&gid='.$_GET['id'].'&id='.$file->id).'" class="btn btn-secondary">Update Filters</a></td>
                                                </tr>';
                                        }
                                    } else {
                                        echo '<tr><td style="text-align: center" colspan="3">No Data Found!</td></tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    <?php else: ?>

                        <div class="card-title">
                            <h4>All Uploaded Files</h4>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Email</th>
                                        <th>Gallery Name</th>
                                        <th>Catalogue</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if( isset( $users ) )
                                    {
                                        $count = 1;
                                        foreach ( $users as $user )
                                        {
                                            $catalogue = ( $user['catalogue'] == '1' ) ? "Yes" : "No";
                                            echo '<tr>
                                                <td>'.$count.'</td>
                                                <td>'.$user['info']->email.'</td>
                                                <td>'.$user['info']->name.'</td>
                                                <td>'.$catalogue.'</td>
                                                <td>
                                                    <a href="'.base_url().'Media/view?id='.$user['info']->id.'">View Media</a>
                                                </td>
                                                </tr>';
                                            $count++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>
