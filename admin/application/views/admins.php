<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Administrators</h3>
        </div>
    </div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
								<tr>
									<th>#</th>
									<th>Email</th>
									<th>Role</th>
								</tr>
								</thead>
								<tbody>
                                <?php
                                if( isset( $users ) )
                                {
                                    foreach ( $users as $user )
                                    {
                                        echo '<tr>
                                                <td>'.$user->id.'</td>
                                                <td>'.$user->email.'</td>
                                                <td>'.$user->role.'</td>';
                                        echo '</tr>';
                                    }
                                }
                                else
                                {
                                    echo '';
                                }
                                ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<footer class="footer"> © 2018 All rights reserved.</footer>

</div>