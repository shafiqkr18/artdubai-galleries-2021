<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?php echo $title; ?></h3>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">

                            <?php if( isset( $view ) && ( $view == 'volunteer' ) ) : ?>

                                <table class="table export_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Full Name</th>
                                        <th>Gallery Name</th>
                                        <th >Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

		                            <?php
		                            if( isset( $requests ) && !empty( $requests ) ){
			                            $count = 1;
			                            foreach( $requests as $request )
			                            {
				                            echo "<tr>";
				                            echo "<td>".$count."</td>";
				                            echo "<td>".$request->fullname."</td>";
				                            echo "<td>".$request->galleryname."</td>";
				                            echo "<td><span class='badge badge-info'>Applied</span></td>";
				                            echo "</tr>";
				                            $count++;
			                            }
		                            }
		                            else{
			                            echo '<tr><td colspan="14" style="text-align: center;">No data found!</td></tr>';
		                            }
		                            ?>

                                    </tbody>
                                </table>

	                        <?php elseif( isset( $view ) && ( $view == 'travel' ) ) : ?>

                                <table class="table export_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Full Name</th>
                                        <th>Gallery Name</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

			                        <?php
			                        if( isset( $requests ) && !empty( $requests ) ){
				                        $count = 1;
				                        foreach( $requests as $request )
				                        {
					                        echo "<tr>";
					                        echo "<td>".$count."</td>";
					                        echo "<td>".$request->fullname."</td>";
					                        echo "<td>".$request->galleryname."</td>";
					                        echo "<td><span class='badge badge-info'>Applied</span></td>";
					                        echo "</tr>";
					                        $count++;
				                        }
			                        }
			                        else{
				                        echo '<tr><td colspan="14" style="text-align: center;">No data found!</td></tr>';
			                        }
			                        ?>

                                    </tbody>
                                </table>

                            <?php elseif( isset( $view ) && ( $view == 'fascia' ) ) : ?>

                                <table class="table export_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Gallery Name</th>
                                        <th>Name</th>
                                        <th>City, Country</th>
                                        <th>Approved</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    if( isset( $fascias ) && !empty( $fascias ) ){
                                        $count = 1;
                                        foreach( $fascias as $fascia )
                                        {
                                            $arpproved = (!empty( $fascia['data']->approved ) && $fascia['data']->approved == 1 ) ? 'Yes' : 'No';
                                            echo "<tr>";
                                            echo "<td>".$count."</td>";
                                            echo "<td>".$fascia['gallery']."</td>";
                                            echo "<td>".$fascia['data']->name."</td>";
                                            echo "<td>".$fascia['data']->address1."</td>";
                                            echo "<td>".$arpproved."</td>";
                                            echo "</tr>";
                                            $count++;
                                        }
                                    }
                                    else{
                                        echo '<tr><td colspan="14" style="text-align: center;">No data found!</td></tr>';
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            <?php elseif( isset( $view ) && ( $view == 'gallery-communication' ) ) : ?>

                                <table class="table export_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Gallery Name</th>
                                        <th>Name</th>
                                        <th>City, Country</th>
                                        <th>Approved</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    if( isset( $gallerycommunication ) && !empty( $gallerycommunication ) ){
                                        $count = 1;
                                        foreach( $gallerycommunication as $communication )
                                        {
                                            $arpproved = (!empty( $communication['data']->approved ) && $communication['data']->approved == 1 ) ? 'Yes' : 'No';
                                            echo "<tr>";
                                            echo "<td>".$count."</td>";
                                            echo "<td>".$communication['gallery']."</td>";
                                            echo "<td>".$communication['data']->name."</td>";
                                            echo "<td>".$communication['data']->address1."</td>";
                                            echo "<td>".$arpproved."</td>";
                                            echo "</tr>";
                                            $count++;
                                        }
                                    }
                                    else{
                                        echo '<tr><td colspan="14" style="text-align: center;">No data found!</td></tr>';
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            <?php else: ?>

                                <table class="table export_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Gallery Name</th>
                                        <th>Confirmed Cities</th>
                                        <th style="min-width: 150px;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

		                            <?php
		                            if( isset( $galleries ) && !empty( $galleries ) ){
			                            $count = 1;
			                            foreach( $galleries as $gallery )
			                            {
                                            $lc_selected = json_decode( $gallery['info']->cities );
                                            $confirmed_cities = (!empty($lc_selected) ) ? implode(' / ', $lc_selected )  : '';

				                            echo "<tr>";
				                            echo "<td>".$count."</td>";
				                            echo "<td>".$gallery['user']->fullname."</td>";
				                            echo "<td>".$gallery['user']->email."</td>";
				                            echo "<td>".$gallery['info']->name."</td>";
                                            echo "<td>".  $confirmed_cities ."</td>";
				                            echo "<td><a href=".base_url( 'gallery/view' )."?id=".$gallery['user']->id.">View</a> | <a class='delete-action' href=".base_url( 'gallery/delete_gallery' )."?id=".$gallery['user']->id.">Delete</a></td>";
				                            echo "</tr>";
				                            $count++;
			                            }
		                            }
		                            else{
			                            echo '<tr><td colspan="14" style="text-align: center;">No data found!</td></tr>';
		                            }
		                            ?>

                                    </tbody>
                                </table>

                            <?php endif; ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer"> © 2018 All rights reserved.</footer>

</div>
