$(function() {

    "use strict";

    var body = $('body');

    body.on( 'click', '.signin-link', function(e){
        e.preventDefault();
        $( '#admin-forget-password' ).hide( 'slow' );
        $( '#admin-login' ).show( 'slow' );
    });
    body.on( 'click', '.forget-password-link', function(e){
        e.preventDefault();
        $( '#admin-login' ).hide( 'slow' );
        $( '#admin-forget-password' ).show( 'slow' );
    } );

    $.fn.tableExport.defaultButton = "button-default2";

    $(".export_table").tableExport({
        bootstrap: false,
        formats: ["xlsx"]
    });

    $('input.radio-notification').change(function() {
        $('.multipleSelect').fastselect();
        if (this.value == '2') {
            $('.multipleSelect').show();
            $('.fstElement').show();

        }else{
            $('.multipleSelect').hide();
            $('.fstElement').hide();
        }
    });

    $("#admin-login").validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        toastr.error(responseText.error,'',{
                            "positionClass": "toast-top-full-width",
                            timeOut: 5000,
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut",
                            "tapToDismiss": false

                        });
                    } else {
                        window.location = site_url;
                    }
                }
            });
        }
    });

    $("#admin-forget-password").validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        toastr.error(responseText.error,'',{
                            "positionClass": "toast-top-full-width",
                            timeOut: 5000,
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut",
                            "tapToDismiss": false

                        });
                    } else {
                        toastr.success(responseText.ok,'',{
                            "positionClass": "toast-top-full-width",
                            timeOut: 5000,
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut",
                            "tapToDismiss": false

                        });
                    }
                }
            });
        }
    });

    $("#admin-reset-password").validate({
        rules: {
            password: {
                required: true,
                minlength: 8
            },
            cpassword: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                minlength: "Minimum password length should be 8 characters."
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        toastr.error(responseText.error,'',{
                            "positionClass": "toast-top-full-width",
                            timeOut: 5000,
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut",
                            "tapToDismiss": false

                        });
                    } else {
                        toastr.success(responseText.ok,'',{
                            "positionClass": "toast-top-full-width",
                            timeOut: 5000,
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut",
                            "tapToDismiss": false

                        });
                        window.location = site_url;
                    }
                }
            });
        }
    });

    $(function() {
        $(".preloader").fadeOut();
    }),

        jQuery(document).on("click", ".mega-dropdown", function(i) {
            i.stopPropagation();
        });


    var i = function() {
        (window.innerWidth > 0 ? window.innerWidth : this.screen.width) < 1170 ? ($("body").addClass("mini-sidebar"),
            $(".navbar-brand span").hide(), $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible"),
            $(".sidebartoggler i").addClass("ti-menu")) : ($("body").removeClass("mini-sidebar"),
            $(".navbar-brand span").show());
        var i = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 1;
        (i -= 70) < 1 && (i = 1), i > 70 && $(".page-wrapper").css("min-height", i + "px");
    };


    $(window).ready(i), $(window).on("resize", i), $(".sidebartoggler").on("click", function() {
        $("body").hasClass("mini-sidebar") ? ($("body").trigger("resize"), $(".scroll-sidebar, .slimScrollDiv").css("overflow", "hidden").parent().css("overflow", "visible"),
            $("body").removeClass("mini-sidebar"), $(".navbar-brand span").show()) : ($("body").trigger("resize"),
            $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible"),
            $("body").addClass("mini-sidebar"), $(".navbar-brand span").hide());
    }),



        $(".fix-header .header").stick_in_parent({}), $(".nav-toggler").click(function() {
        $("body").toggleClass("show-sidebar"), $(".nav-toggler i").toggleClass("mdi mdi-menu"),
            $(".nav-toggler i").addClass("mdi mdi-close");
    }),



        $(".search-box a, .search-box .app-search .srh-btn").on("click", function() {
            $(".app-search").slideToggle(200);
        }),



        $(".floating-labels .form-control").on("focus blur", function(i) {
            $(this).parents(".form-group").toggleClass("focused", "focus" === i.type || this.value.length > 0);
        }).trigger("blur"), $(function() {
        for (var i = window.location, o = $("ul#sidebarnav a").filter(function() {
            return this.href == i;
        }).addClass("active").parent().addClass("active");;) {
            if (!o.is("li")) break;
            o = o.parent().addClass("in").parent().addClass("active");
        }
    }),

    $(function() {
        $("#sidebarnav").metisMenu();
    }),

    $(".scroll-sidebar").slimScroll({
        position: "left",
        size: "5px",
        height: "100%",
        color: "#dcdcdc"
    }),

    $(".message-center").slimScroll({
        position: "right",
        size: "5px",
        color: "#dcdcdc"
    }),

    $(".aboutscroll").slimScroll({
        position: "right",
        size: "5px",
        height: "80",
        color: "#dcdcdc"
    }),

    $(".message-scroll").slimScroll({
        position: "right",
        size: "5px",
        height: "570",
        color: "#dcdcdc"
    }),

    $(".chat-box").slimScroll({
        position: "right",
        size: "5px",
        height: "470",
        color: "#dcdcdc"
    }),

    $(".slimscrollright").slimScroll({
        height: "100%",
        position: "right",
        size: "5px",
        color: "#dcdcdc"
    }),

    $("body").trigger("resize"), $(".list-task li label").click(function() {
        $(this).toggleClass("task-done");
    }),

    $("#to-recover").on("click", function() {
        $("#loginform").slideUp(), $("#recoverform").fadeIn();
    }),

    $('a[data-action="collapse"]').on("click", function(i) {
        i.preventDefault(), $(this).closest(".card").find('[data-action="collapse"] i').toggleClass("ti-minus ti-plus"),
            $(this).closest(".card").children(".card-body").collapse("toggle");
    }),

    $('a[data-action="expand"]').on("click", function(i) {
        i.preventDefault(), $(this).closest(".card").find('[data-action="expand"] i').toggleClass("mdi-arrow-expand mdi-arrow-compress"),
            $(this).closest(".card").toggleClass("card-fullscreen");
    }),

    $('a[data-action="close"]').on("click", function() {
        $(this).closest(".card").removeClass().slideUp("fast");
    });

    // $(".textarea_editor").each(function(){
    //     $(this).wysihtml5({
    //         "font-styles": true,
    //         "emphasis": true,
    //         "lists": true,
    //         "html": true,
    //         "link": false,
    //         "image": false
    //     });
    // });

    $(".textarea_editor").jqte({
        rule: false,
        remove: false,
        indent: false,
        sub: false,
        sup: false,
        color: false,
        fsize: false
    });

    $('input[name=type]').on( 'click', function(){
        var value = $(this).val();
        if( value === '2' )
        {
            $('#user_selection').removeAttr( 'disabled' );
        }
        else
        {
            $('#user_selection').attr( 'disabled', 'disabled' );
        }
    } );
    
    $('#media-library').DataTable({
        "initComplete": function(settings, json) {
            //library_tool_tip();
        },
        "drawCallback": function( settings ) {
            library_tool_tip();
        }
          
    });
    function library_tool_tip(){
        $('.gallery-tooltip').hover(function () {
             // Hover over code
             var title = $(this).attr('data-alt');
             var img = $(this).attr('data-src');
             //$(this).data('tipText', title);
             $('<div class="tooltip-image"><img src="'+ img +'" /><div>'+ title +'</div></div>').appendTo('body').fadeIn('slow');
         }, function () {
             // Hover out code
             //$(this).attr('alt', $(this).data('tipText'));
             $('.tooltip-image').hide();
         }).mousemove(function (e) {
             var mousex = e.pageX + 20;
             //Get X coordinates
             var mousey = e.pageY + 10;
             //Get Y coordinates
             $('.tooltip-image').css({
                 top: mousey,
                 left: mousex
             })
         });
    }
    
    $('.edit').click(function(){
      $(this).addClass('editMode');
    });
    
    // Save Booth Name data

    $('button.editable').on('click', function() {
        var id = this.id;
        var split_id = id.split("_");
        var field_name = split_id[0];
        var edit_id = split_id[1];
        var value = $(this).text();
        
        // console.log(    'id: ' + id, '\n' ,
        //             ' split_id: ' + split_id, '\n',
        //             ' field_name: ' + field_name, '\n',
        //             ' edit_id: ' + edit_id, '\n',
        //             ' value: ' + value, '\n'
        //         );
        
        if ($(this).hasClass('save')) {
    
            $(this).text("Edit").removeClass('save');
            $('.gallery-'+edit_id).attr('contenteditable', 'false').removeClass('active-field');
            var booth = $('.gallery-'+edit_id).text();
            
            $.ajax({
                    url: site_url + 'gallery/update_booth',
                    type: 'post',
                    dataType: 'json',
                    data: { id:edit_id, value:booth },
                    success:function(response){
                        
                        if(response.error){
                             $('.gallery-'+edit_id).closest('tr').append('<small class="booth-ajax-info label label-warning">'+ response.error +'</small>');
                        }else{
                             $('.gallery-'+edit_id).closest('tr').append('<small class="booth-ajax-info label label-success">'+ response.ok +'</small>');
                        }
                        window.setTimeout(function(){
                            $('tr').find('.booth-ajax-info').fadeOut("normal", function() {
                                $('.booth-ajax-info').fadeOut();
                                
                            });
                            
                            $('.booth-ajax-info').remove();
                        },3000);
                    }
                });
        } else {
            $(this).text("Save").addClass('save');
            $('.gallery-'+edit_id).attr('contenteditable', 'true').addClass('active-field').focus();
      }
    });

    // Delete profiles 
    $(".delete-action").on("click", function(){

        event.preventDefault(); 

        var result = confirm("Are you sure you want to delete this item?");
        if(result){
            alert(result);
            //window.location.href
        }
    });
    
    $('.filter-year-catalogue').on('change', function() {

        var url = window.location.href;
        var val = $(this).val();
    
        if (val) {
            window.location = val;
        }
        return false;
    });


});