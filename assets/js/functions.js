jQuery(document).ready(function( $ ){

    $('[data-toggle="tooltip"]').tooltip();

    $('.show-notification').on( 'click', function( e ){
        e.preventDefault();
        $('#notifications-box').fadeToggle();
    } );

    $("#volunteer-request").validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.magnificPopup.open({
                            items: {
                                src: '#volunteer-request-popup',
                                type: 'inline'
                            }
                        });
                    }
                }
            });
        }
    });

    $("#accommodation-request").validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.magnificPopup.open({
                            items: {
                                src: '#accommodation-request-popup',
                                type: 'inline'
                            }
                        });
                    }
                }
            });
        }
    });

    $("#gallery-fascia-form").validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                beforeSubmit: function() {
                    $(form).find('.btn').prop('disabled',true);
                    $(form).find('.btn').after('<p style="margin: 15px 0 0;"><i class="fas fa-spinner fa-spin fa-2x"></i></p>');
                },
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                        setTimeout(function(){
                            window.location.reload();
                        }, 500);
                    }
                }
            });
        }
    });

    $("#gallery-communication-form").validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                beforeSubmit: function() {
                    $(form).find('.btn').prop('disabled',true);
                    $(form).find('.btn').after('<p style="margin: 15px 0 0;"><i class="fas fa-spinner fa-spin fa-2x"></i></p>');
                },
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                        setTimeout(function(){
                            window.location.reload();
                        }, 500);
                    }
                }
            });
        }
    });

    $("#upload-zone").dropzone({
        url: baseURL + "library/upload",
        autoProcessQueue: false,
        uploadMultiple: false,
        clickable: true,
        maxFilesize: 20,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: '.jpg,.jpeg',
        createImageThumbnails: true,
        thumbnailWidth: 330,
        thumbnailHeight: 160,
        thumbnailMethod: 'crop',
        parallelUploads: 1,
        timeout: 300000,
        init: function () {
            var form = $('#upload-form'),
                dropZone = this;
            form.validate({
                errorPlacement: function(error, element) {}
            });
            form.submit(function(e){
                e.preventDefault();
                if( form.valid() ) {
                    dropZone.processQueue();
                }
            });
            dropZone.on('uploadprogress', function(file, progress){
                $(document.body).css({'cursor' : 'wait'});
            });
            dropZone.on('sending', function(file, xhr, formData) {
                var data = form.serializeArray();
                $.each(data, function(key, el) {
                    formData.append(el.name, el.value);
                });
            });
            dropZone.on("error", function(file,errorMessage) {
                $.notify({
                    message: errorMessage
                },{
                    type: 'danger'
                });
            });
            dropZone.on("success", function(file,responseText) {
                var data = JSON.parse( responseText );
                if( data.error ) {
                    $.notify({
                        message: data.error
                    },{
                        type: 'danger'
                    });
                } else {
                    $.notify({
                        message: data.success
                    },{
                        type: 'success'
                    });
                    setTimeout(function(){
                        window.location = baseURL + 'image-library';
                    }, 2000);
                }
            });
        }
    });

    $('.delete-image').on('click', function (e) {
        e.preventDefault();
        var returnVal = confirm("Are you sure you want to delete?");
        if(returnVal == true){
            var d = $(this),
                id = d.data('id');
            $.ajax({
                method: 'post',
                url: baseURL + 'library/delete',
                data: {
                    id: id
                },
                success: function( response ){
                    var data = JSON.parse( response );
                    $.notify({
                        message: data.success
                    },{
                        type: 'success'
                    });
                    setTimeout(function(){
                        window.location = baseURL + 'image-library';
                    }, 2000);
                },
                error: function( error ){
                    $.notify({
                        message: error
                    },{
                        type: 'danger'
                    });
                }
            });
        }
    });

    $("#upload-edit-form").validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                        setTimeout(function(){
                            window.location = baseURL + 'image-library';
                        }, 1000);
                    }
                }
            });
        }
    });

    $("#vip-entry-form").validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $('#vip-entry-form').ajaxSubmit({
                dataType: 'json',
                beforeSubmit: function() {
                    $(form).find('.btn').prop('disabled',true);
                    $(form).find('.btn').after('<p id="id_sp" style="margin: 15px 0 0;"><i class="fas fa-spinner fa-spin fa-2x"></i></p>');
                },
                success: function( responseText ) {
                    if( responseText.error ) {
                       // console.log("errorMsg ="+responseText.errorMsg);
                       // console.log("error = "+responseText.error);
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else if( responseText.success ) {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                        setTimeout(function(){
                            window.location = baseURL + 'vip-designations';
                        }, 1000);
                    } else {
                        $.magnificPopup.open({
                            items: {
                                src: '<div class="vip-submission-popup white-popup alt">\n' +
                                    '                <p>'+responseText.info+'</p>\n' +
                                    '                <a href="'+baseURL+'vip-designations#guestnames" class="btn filled small alt mfp-close">SELECT ANOTHER GUEST</a>\n' +
                                    '            </div>',
                                type: 'inline'
                            }
                        });
                        $(form).find('.btn').prop('disabled', false);
                        $(form).find('#id_sp').html('');
                        setTimeout(function(){
                           // window.location = baseURL + 'vip-designations';
                        }, 4000);
                        
                    }
                }
            });
        }
    });

    $('.delete-vip').on('click', function (e) {
        e.preventDefault();
        var returnVal = confirm("Are you sure you want to delete?");
        if(returnVal == true){
            var d = $(this),
                id = d.data('id');
            $.ajax({
                method: 'post',
                url: baseURL + 'vip/delete',
                data: {
                    id: id
                },
                success: function( response ){
                    var data = JSON.parse( response );
                    $.notify({
                        message: data.success
                    },{
                        type: 'success'
                    });
                    setTimeout(function(){
                        window.location = baseURL + 'vip-designations';
                    }, 2000);
                },
                error: function( error ){
                    $.notify({
                        message: error
                    },{
                        type: 'danger'
                    });
                }
            });
        }
    });

    $('#gp-update-form').validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else if( responseText.success ) {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                        setTimeout(function(){
                            if(responseText.redirectTo){
                                window.location = baseURL + responseText.redirectTo;
                            }else{
                                window.location = baseURL + 'gallery-profile';
                            }
                            
                        }, 1000);
                    } 
                },
                error: function( error ){
                    $.notify({
                        message: error
                    },{
                        type: 'danger'
                    });
                }
            });
        }
    });

    $('#me-update-artist').validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    window.location = baseURL + 'my-exhibition';
                }
            });
        }
    });

    $('#gp-create-form').validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    window.location = baseURL + 'gallery-profile';
                },
                error: function( error ){
                    $.notify({
                        message: error
                    },{
                        type: 'danger'
                    });
                }
            });
        }
    });

    $('.delete-gp').on('click', function (e) {
        e.preventDefault();
        var returnVal = confirm("Are you sure you want to delete?");
        if(returnVal == true){
        
            var d = $(this),
                id = d.data('id'),
                section = d.data('section');
            $.ajax({
                method: 'post',
                url: baseURL + 'gallery-profile/delete',
                data: {
                    id: id,
                    section: section
                },
                success: function( response ){
                    var data = JSON.parse( response );
                    $.notify({
                        message: data.success
                    },{
                        type: 'success'
                    });
                    setTimeout(function(){
                        window.location = baseURL + 'gallery-profile';
                    }, 1000);
                },
                error: function( error ){
                    $.notify({
                        message: error
                    },{
                        type: 'danger'
                    });
                }
            });
            }
    });

    $('#passes-form').validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                beforeSubmit: function() {
                    $('#passes-submit').attr('disabled', true).val('Submitting ...');
                },
                success: function( responseText ) {
                    $('#passes-submit').attr('disabled', false).val('Submit');
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                        setTimeout(function(){
                            window.location = baseURL + 'passes/' + responseText.section;
                        }, 2000);
                    }
                }
            });
        }
    });

    $('.delete-pass').on('click', function (e) {
        e.preventDefault();
        var returnVal = confirm("Are you sure you want to delete?");
        if(returnVal == true){
            var d = $(this),
                id = d.data('id'),
                section = d.data('section');
            $.ajax({
                method: 'post',
                url: baseURL + 'passes/delete',
                data: {
                    id: id
                },
                success: function( response ){
                    var data = JSON.parse( response );
                    $.notify({
                        message: data.success
                    },{
                        type: 'success'
                    });
                    setTimeout(function(){
                        window.location = baseURL + 'passes/' + section;
                    }, 500);
                },
                error: function( error ){
                    $.notify({
                        message: error
                    },{
                        type: 'danger'
                    });
                }
            });
        }
    });

    $('.file-trigger').on( 'click', 'button', function(e){
        e.preventDefault();
        $(this).siblings('input').trigger('click');
    } );

    $('input[type="file"]').change( function(e){
        var fileName = e.target.files[0].name;
        $('.file-trigger button').html( fileName );
    });

    $("#mobile").intlTelInput({
        separateDialCode: true,
        initialCountry: 'ae',
        utilsScript: baseURL+"/assets/js/utils.js"
    });

    $("#mobile").on("countrychange", function(e, countryData) {
        var code = $("#mobile").intlTelInput("getSelectedCountryData").dialCode;
        $('#mobile_code').val( '+('+code+')' );
    });

    var code = $("#mobile").intlTelInput("getSelectedCountryData").dialCode;
    $('#mobile_code').val( '+('+code+')' );

    $("#phone").intlTelInput({
        separateDialCode: true,
        initialCountry: 'ae',
        utilsScript: baseURL+"/assets/js/utils.js"
    });

    $("#phone").on("countrychange", function(e, countryData) {
        var code = $("#phone").intlTelInput("getSelectedCountryData").dialCode;
        $('#phone_code').val( '+'+code );
    });

    var code = $("#phone").intlTelInput("getSelectedCountryData").dialCode;
    $('#phone_code').val( '+'+code );

    $('.update_notification').on('click',function(e){
        e.preventDefault();
        var id = $(this).data('id'),
            user = $(this).data('user'),
            item = $(this).parent('.item');
        $.ajax({
            method: 'post',
            url: baseURL + 'dashboard/notification_update',
            data: {
                id: id,
                user_id: user
            },
            success: function( response ){
                item.addClass('read');
            },
            error: function( error ){

            }
        });
    });

    $('.image-popup').magnificPopup({
        type: 'image'
    });

    $('#country').on('change',function(){
        var country = $(this).val(),
            state = $('#state');
        if( country === 'canada' ) {
            $(state).html('<option value="">State / Province *</option><option value="alberta">ALBERTA</option><option value="british columbia">BRITISH COLUMBIA</option><option value="manitoba">MANITOBA</option><option value="new brunswick">NEW BRUNSWICK</option><option value="newfoundland">NEWFOUNDLAND</option><option value="northwest territories">NORTHWEST TERRITORIES</option><option value="nova scotia">NOVA SCOTIA</option><option value="nunavut">NUNAVUT</option><option value="ontario">ONTARIO</option><option value="prince edward island">PRINCE EDWARD ISLAND</option><option value="quebec">QUEBEC</option><option value="saskatchewan">SASKATCHEWAN</option><option value="yukon territory">YUKON TERRITORY</option>').attr('disabled',false);
        } else if( country === 'australia' ) {
            $(state).html('<option value="">State / Province *</option><option value="ac">AC</option><option value="australian capital territory">AUSTRALIAN CAPITAL TERRITORY</option><option value="hk">HK</option><option value="ind">IND</option><option value="new south wales">NEW SOUTH WALES</option><option value="northern territory">NORTHERN TERRITORY</option><option value="ns">NS</option><option value="nz">NZ</option><option value="ql">QL</option><option value="queensland">QUEENSLAND</option><option value="sin">SIN</option><option value="south australia">SOUTH AUSTRALIA</option><option value="sus">SUS</option><option value="ta">TA</option><option value="tasmania">TASMANIA</option><option value="tha">THA</option><option value="vi">VI</option><option value="victoria">VICTORIA</option><option value="western australia">WESTERN AUSTRALIA</option>').attr('disabled',false);
        } else if( country === 'brazil' ) {
            $(state).html('<option value="">State / Province *</option><option value="acre">ACRE</option><option value="alagoas">ALAGOAS</option><option value="amapa">AMAPA</option><option value="amazonas">AMAZONAS</option><option value="bahia">BAHIA</option><option value="ceara">CEARA</option><option value="distrito federal">DISTRITO FEDERAL</option><option value="espirito santo">ESPIRITO SANTO</option><option value="goias">GOIAS</option><option value="maranhao">MARANHAO</option><option value="mato grosso">MATO GROSSO</option><option value="mato grosso do sul">MATO GROSSO DO SUL</option><option value="minas gerais">MINAS GERAIS</option><option value="north rio grande">NORTH RIO GRANDE</option><option value="para">PARA</option><option value="paraiba">PARAIBA</option><option value="parana">PARANA</option><option value="pernambuco">PERNAMBUCO</option><option value="piaui">PIAUI</option><option value="rio de janeiro">RIO DE JANEIRO</option><option value="rio grande do norte">RIO GRANDE DO NORTE</option><option value="rio grande do sul">RIO GRANDE DO SUL</option><option value="rondonia">RONDONIA</option><option value="roraima">RORAIMA</option><option value="santa catarina">SANTA CATARINA</option><option value="sao paulo">SAO PAULO</option><option value="sergipe">SERGIPE</option><option value="south mato grosso">SOUTH MATO GROSSO</option><option value="south rio grande">SOUTH RIO GRANDE</option><option value="tocantins">TOCANTINS</option>').attr('disabled',false);
        } else if( country === 'china' ) {
            $(state).html('<option value="">State / Province *</option><option value="anhui">Anhui</option><option value="beijing">Beijing</option><option value="chongqing">Chongqing</option><option value="fujian">Fujian</option><option value="gansu">Gansu</option><option value="guangdong">Guangdong</option><option value="guangxi">Guangxi</option><option value="guizhou">Guizhou</option><option value="hainan">Hainan</option><option value="hebei">Hebei</option><option value="heilongjiang">Heilongjiang</option><option value="henan">Henan</option><option value="hubei">Hubei</option><option value="hunan">Hunan</option><option value="inner mongolia">Inner Mongolia</option><option value="jiangsu">Jiangsu</option><option value="jiangxi">Jiangxi</option><option value="jilin">Jilin</option><option value="liaoning">Liaoning</option><option value="ningxia">Ningxia</option><option value="qinghai">Qinghai</option><option value="shaanxi">Shaanxi</option><option value="shandong">Shandong</option><option value="shanghai">Shanghai</option><option value="shanxi">Shanxi</option><option value="sichuan">Sichuan</option><option value="tianjin">Tianjin</option><option value="xinjiang">Xinjiang</option><option value="xizang">Xizang</option><option value="yunnan">Yunnan</option><option value="zhejiang">Zhejiang</option>').attr('disabled',false);
        } else if( country === 'india' ) {
            $(state).html('<option value="">State / Province *</option><option value="anda &amp; nicubor">Anda &amp; Nicubor</option><option value="andaman &amp; nicobar">Andaman &amp; Nicobar</option><option value="andaman &amp; nicobar islands">Andaman &amp; Nicobar Islands</option><option value="andhra pradesh">Andhra Pradesh</option><option value="arunachal pradesh">Arunachal Pradesh</option><option value="assam">Assam</option><option value="bihar">Bihar</option><option value="chandigarh">Chandigarh</option><option value="chattisgarh">Chattisgarh</option><option value="chhattisgarh">Chhattisgarh</option><option value="dadra &amp; nagar haveli">Dadra &amp; Nagar Haveli</option><option value="dadra &amp; nagar havelli">Dadra &amp; Nagar Havelli</option><option value="daman &amp; diu">Daman &amp; Diu</option><option value="delhi">Delhi</option><option value="goa">Goa</option><option value="gujarat">Gujarat</option><option value="haryana">Haryana</option><option value="himachal pradesh">Himachal Pradesh</option><option value="hyderabad">Hyderabad</option><option value="jammu &amp; kashmir">Jammu &amp; Kashmir</option><option value="jharkhand">Jharkhand</option><option value="kanchanaburi">Kanchanaburi</option><option value="karnataka">Karnataka</option><option value="kerala">Kerala</option><option value="lakshadweep">Lakshadweep</option><option value="madhya pradesh">Madhya Pradesh</option><option value="maharashtra">Maharashtra</option><option value="manipur">Manipur</option><option value="meghalaya">Meghalaya</option><option value="mindoro oriental">Mindoro Oriental</option><option value="mizoram">Mizoram</option><option value="nagaland">Nagaland</option><option value="new delhi">New Delhi</option><option value="odisha">Odisha</option><option value="orissa">Orissa</option><option value="pondicherry">Pondicherry</option><option value="puducherry">Puducherry</option><option value="punjab">Punjab</option><option value="rajasthan">Rajasthan</option><option value="rajshahi">Rajshahi</option><option value="sikkim">Sikkim</option><option value="tamil nadu">Tamil Nadu</option><option value="tamilnadu">Tamilnadu</option><option value="telangana">Telangana</option><option value="tripura">Tripura</option><option value="uttar pradesh">Uttar Pradesh</option><option value="uttarakhand">Uttarakhand</option><option value="uttaranchal">Uttaranchal</option><option value="west bengal">West Bengal</option>').attr('disabled',false);
        } else if( country === 'ireland' ) {
            $(state).html('<option value="">State / Province *</option><option value="co carlow">CO CARLOW</option><option value="co cavan">CO CAVAN</option><option value="co clare">CO CLARE</option><option value="co cork">CO CORK</option><option value="co donegal">CO DONEGAL</option><option value="co dublin">CO DUBLIN</option><option value="co galway">CO GALWAY</option><option value="co kerry">CO KERRY</option><option value="co kildare">CO KILDARE</option><option value="co kilkenny">CO KILKENNY</option><option value="co laois">CO LAOIS</option><option value="co leitrim">CO LEITRIM</option><option value="co limerick">CO LIMERICK</option><option value="co longford">CO LONGFORD</option><option value="co louth">CO LOUTH</option><option value="co mayo">CO MAYO</option><option value="co meath">CO MEATH</option><option value="co monaghan">CO MONAGHAN</option><option value="co offaly">CO OFFALY</option><option value="co roscommon">CO ROSCOMMON</option><option value="co sligo">CO SLIGO</option><option value="co tipperary">CO TIPPERARY</option><option value="co waterford">CO WATERFORD</option><option value="co westmeath">CO WESTMEATH</option><option value="co wexford">CO WEXFORD</option><option value="co wicklow">CO WICKLOW</option>').attr('disabled',false);
        } else if( country === 'italy' ) {
            $(state).html('<option value="">State / Province *</option><option value="agrigento">AGRIGENTO</option><option value="alessandria">ALESSANDRIA</option><option value="ancona">ANCONA</option><option value="aosta">AOSTA</option><option value="arezzo">AREZZO</option><option value="ascoli piceno">ASCOLI PICENO</option><option value="asti">ASTI</option><option value="avellino">AVELLINO</option><option value="bari">BARI</option><option value="belluno">BELLUNO</option><option value="benevento">BENEVENTO</option><option value="bergamo">BERGAMO</option><option value="biella">BIELLA</option><option value="bologna">BOLOGNA</option><option value="bolzano">BOLZANO</option><option value="brescia">BRESCIA</option><option value="brindisi">BRINDISI</option><option value="cagliari">CAGLIARI</option><option value="caltanissetta">CALTANISSETTA</option><option value="campobasso">CAMPOBASSO</option><option value="carbonia-iglesias">CARBONIA-IGLESIAS</option><option value="caserta">CASERTA</option><option value="catania">CATANIA</option><option value="catanzaro">CATANZARO</option><option value="chieti">CHIETI</option><option value="como">COMO</option><option value="cosenza">COSENZA</option><option value="cremona">CREMONA</option><option value="crotone">CROTONE</option><option value="cuneo">CUNEO</option><option value="enna">ENNA</option><option value="fermo">FERMO</option><option value="ferrara">FERRARA</option><option value="firenze">FIRENZE</option><option value="foggia">FOGGIA</option><option value="forli" -cesena\'="">FORLI\'-CESENA</option><option value="frosinone">FROSINONE</option><option value="genova">GENOVA</option><option value="gorizia">GORIZIA</option><option value="grosseto">GROSSETO</option><option value="imperia">IMPERIA</option><option value="isernia">ISERNIA</option><option value="l" aquila\'="">L\' AQUILA</option><option value="la spezia">LA SPEZIA</option><option value="latina">LATINA</option><option value="lecce">LECCE</option><option value="lecco">LECCO</option><option value="livorno">LIVORNO</option><option value="lodi">LODI</option><option value="lucca">LUCCA</option><option value="macerata">MACERATA</option><option value="mantova">MANTOVA</option><option value="massa">MASSA</option><option value="matera">MATERA</option><option value="medio campidano">MEDIO CAMPIDANO</option><option value="messina">MESSINA</option><option value="milano">MILANO</option><option value="modena">MODENA</option><option value="monza e brianza">MONZA E BRIANZA</option><option value="napoli">NAPOLI</option><option value="novara">NOVARA</option><option value="nuoro">NUORO</option><option value="ogliastra">OGLIASTRA</option><option value="olbia-tempio">OLBIA-TEMPIO</option><option value="oristano">ORISTANO</option><option value="padova">PADOVA</option><option value="palermo">PALERMO</option><option value="parma">PARMA</option><option value="pavia">PAVIA</option><option value="perugia">PERUGIA</option><option value="pesaro e urbino">PESARO E URBINO</option><option value="pescara">PESCARA</option><option value="piacenza">PIACENZA</option><option value="pisa">PISA</option><option value="pistoia">PISTOIA</option><option value="pordenone">PORDENONE</option><option value="potenza">POTENZA</option><option value="prato">PRATO</option><option value="ragusa">RAGUSA</option><option value="ravenna">RAVENNA</option><option value="reggio calabria">REGGIO CALABRIA</option><option value="reggio emilia">REGGIO EMILIA</option><option value="rieti">RIETI</option><option value="rimini">RIMINI</option><option value="roma">ROMA</option><option value="rovigo">ROVIGO</option><option value="salerno">SALERNO</option><option value="sassari">SASSARI</option><option value="savona">SAVONA</option><option value="siena">SIENA</option><option value="siracusa">SIRACUSA</option><option value="sondrio">SONDRIO</option><option value="taranto">TARANTO</option><option value="teramo">TERAMO</option><option value="terni">TERNI</option><option value="torino">TORINO</option><option value="trapani">TRAPANI</option><option value="trento">TRENTO</option><option value="treviso">TREVISO</option><option value="trieste">TRIESTE</option><option value="udine">UDINE</option><option value="varese">VARESE</option><option value="venezia">VENEZIA</option><option value="verbano-cusio-ossola">VERBANO-CUSIO-OSSOLA</option><option value="vercelli">VERCELLI</option><option value="verona">VERONA</option><option value="vibo valentia">VIBO VALENTIA</option><option value="vicenza">VICENZA</option><option value="viterbo">VITERBO</option>').attr('disabled',false);
        } else if( country === 'mexico' ) {
            $(state).html('<option value="">State / Province *</option><option value="aguascalientes">AGUASCALIENTES</option><option value="baja california">BAJA CALIFORNIA</option><option value="baja california norte">BAJA CALIFORNIA NORTE</option><option value="baja california sur">BAJA CALIFORNIA SUR</option><option value="campeche">CAMPECHE</option><option value="chiapas">CHIAPAS</option><option value="chihuahua">CHIHUAHUA</option><option value="ciudad de mexico">CIUDAD DE MEXICO</option><option value="coahuila">COAHUILA</option><option value="colima">COLIMA</option><option value="distrito federal">DISTRITO FEDERAL</option><option value="durango">DURANGO</option><option value="estado de mexico">ESTADO DE MEXICO</option><option value="guanajuato">GUANAJUATO</option><option value="guerrero">GUERRERO</option><option value="hidalgo">HIDALGO</option><option value="jalisco">JALISCO</option><option value="michoacan">MICHOACAN</option><option value="michoac&nbsp;n">Michoac&nbsp;n</option><option value="morelos">MORELOS</option><option value="nayarit">NAYARIT</option><option value="nuevo leon">NUEVO LEON</option><option value="nuevo le¢n">Nuevo Le¢n</option><option value="oaxaca">OAXACA</option><option value="puebla">PUEBLA</option><option value="queretaro">QUERETARO</option><option value="quintana roo">QUINTANA ROO</option><option value="san luis potosi">SAN LUIS POTOSI</option><option value="san luis potos¡">San Luis Potos¡</option><option value="sinaloa">SINALOA</option><option value="sonora">SONORA</option><option value="tabasco">TABASCO</option><option value="tamaulipas">TAMAULIPAS</option><option value="tlaxcala">TLAXCALA</option><option value="veracruz">VERACRUZ</option><option value="yucatan">YUCATAN</option><option value="yucat&nbsp;n">Yucat&nbsp;n</option><option value="zacatecas">ZACATECAS</option>').attr('disabled',false);
        } else if( country === 'united states' ) {
            $(state).html('<option value="">State / Province *</option><option value="alabama">Alabama</option><option value="alaska">Alaska</option><option value="arizona">Arizona</option><option value="arkansas">Arkansas</option><option value="california">California</option><option value="colorado">Colorado</option><option value="connecticut">Connecticut</option><option value="delaware">Delaware</option><option value="district of columbia">District of Columbia</option><option value="florida">Florida</option><option value="georgia">Georgia</option><option value="hawaii">Hawaii</option><option value="idaho">Idaho</option><option value="illinois">Illinois</option><option value="indiana">Indiana</option><option value="iowa">Iowa</option><option value="kansas">Kansas</option><option value="kentucky">Kentucky</option><option value="louisiana">Louisiana</option><option value="maine">Maine</option><option value="maryland">Maryland</option><option value="massachusetts">Massachusetts</option><option value="michigan">Michigan</option><option value="minnesota">Minnesota</option><option value="mississippi">Mississippi</option><option value="missouri">Missouri</option><option value="montana">Montana</option><option value="nebraska">Nebraska</option><option value="nevada">Nevada</option><option value="new hampshire">New Hampshire</option><option value="new jersey">New Jersey</option><option value="new mexico">New Mexico</option><option value="new york">New York</option><option value="north carolina">North Carolina</option><option value="north dakota">North Dakota</option><option value="ohio">Ohio</option><option value="oklahoma">Oklahoma</option><option value="oregon">Oregon</option><option value="pennsylvania">Pennsylvania</option><option value="puerto rico">Puerto Rico</option><option value="rhode island">Rhode Island</option><option value="south carolina">South Carolina</option><option value="south dakota">South Dakota</option><option value="tennessee">Tennessee</option><option value="texas">Texas</option><option value="united states virgin islands">United States Virgin Islands</option><option value="utah">Utah</option><option value="vermont">Vermont</option><option value="virgin islands">Virgin Islands</option><option value="virginia">Virginia</option><option value="washington">Washington</option><option value="west virginia">West Virginia</option><option value="wisconsin">Wisconsin</option><option value="wyoming">Wyoming</option>').attr('disabled',false);
        } else {
            $(state).html('<option value="">State / Province *</option>').attr('disabled',true);
        }
    });

    if( $(window).width() >= 769 ) {

        $('.overline').hoverSlippery({
            bgColor: '',
            speed: 300,
            radius: '',
            border: true,
            borderColor: '#5b91ae',
            borderTop: 0,
            borderStyle: 'solid',
            borderWidth: '3px',
            borderTopLine: false,
            twiceBorder: false,
            overline: true
        });

    }  

    if( $(window).width() <= 768 ) {

        $('.navbar-toggle').on( 'click', function( e ){
            e.preventDefault();
            $('#nav').slideToggle( 'fast' );
        } );

        $('#nav a').on( 'click', function( e ){
            if( $(this).parent('li').hasClass('has_sub_menu') ) {
                console.log('here');
                e.preventDefault();
                $(this).next('.sub-menu').slideToggle( 'fast' );
            }
        } ); 

    }

    $(".only-numbers").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('#glocation').selectpicker();

    $('#gallery-communication-name').bind("keyup change", function(){
        $('#gallery-communication-preview .name').text( $(this).val() );
    } );

    $('#gallery-communication-city').bind("keyup change", function(){
        $('#gallery-communication-preview .city').text( ', ' + $(this).val() );
    } );

    $('#gallery-booth-name').bind("keyup change", function(){
        $('.booth-info .name').text( $(this).val() );
    } );

    $('#gallery-booth-city').bind("keyup change", function(){
        $('.booth-info .city').text( $(this).val() );
    } ); 
    
    $('#confirmation-popup-form').validate({
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    
                    $('#confirmation-popup').modal('hide');

                    setTimeout(function(){
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                    }, 500);

                    setTimeout(function(){ window.location.reload(); }, 1000);
                },
                error: function( error ){
                    $.notify({
                        message: error
                    },{
                        type: 'danger'
                    });
                }
            });
        }
    });

    setTimeout(function(){
        $('#confirmation-popup').modal('show');
    }, 4000);

});
