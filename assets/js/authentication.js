jQuery(document).ready(function( $ ){

    $("#login").validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        window.location = baseURL;
                    }
                }
            });
        }
    });

    $("#forget-password").validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                clearForm: true,
                resetForm: true,
                beforeSubmit: function() {
                    $(form).find('.submit').prop('disabled',true);
                },
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success'
                        });
                        window.location = baseURL;
                    }
                    $(form).find('.submit').removeAttr('disabled');
                }
            });
        }
    });

    $("#reset-password").validate({
        rules: {
            password: {
                required: true,
                minlength: 8,
                pwCheck: true
            },
            password_confirmation: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                pwCheck: "Must have one capital letter and one digit.",
                minlength: "Minimum password length should be 8 characters."
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                clearForm: true,
                resetForm: true,
                beforeSubmit: function() {
                    $(form).find('.submit').prop('disabled',true);
                },
                success: function( responseText ) {
                    if( responseText.error ) {
                        $.notify({
                            message: responseText.error
                        },{
                            type: 'danger'
                        });
                    } else {
                        $.notify({
                            message: responseText.success
                        },{
                            type: 'success',
                            delay: 2000
                        });
                    }
                    window.location = baseURL;
                }
            });
        }
    });

});

$.validator.addMethod("pwCheck", function(value) {
    return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
        && /[A-Z]/.test(value) // has a uppercase letter
        && /\d/.test(value) // has a digit
});